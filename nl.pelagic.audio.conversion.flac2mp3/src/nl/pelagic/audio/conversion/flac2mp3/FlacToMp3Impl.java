package nl.pelagic.audio.conversion.flac2mp3;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import nl.pelagic.audio.conversion.flac2mp3.api.Flac2Mp3Configuration;
import nl.pelagic.audio.conversion.flac2mp3.api.FlacToMp3;
import nl.pelagic.audio.conversion.flac2mp3.i18n.Messages;
import nl.pelagic.jaudiotagger.util.TagUtils;
import nl.pelagic.shell.script.listener.api.ShellScriptListener;
import nl.pelagic.shutdownhook.api.ShutdownHookParticipant;
import nl.pelagic.util.file.DirUtils;
import nl.pelagic.util.string.StringUtils;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldDataInvalidException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.KeyNotFoundException;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.flac.FlacTag;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.id3.ID3v11Tag;
import org.jaudiotagger.tag.id3.ID3v1Tag;
import org.jaudiotagger.tag.id3.ID3v23Tag;
import org.jaudiotagger.tag.id3.ID3v24Tag;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.FieldOption;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;

/**
 * Convert a flac file into an mp3 file
 */
@Component
public class FlacToMp3Impl implements FlacToMp3, ShutdownHookParticipant {
  /** the logger */
  private Logger logger = Logger.getLogger(this.getClass().getName());

  /** the shell script listener (optional) */
  @Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL,
      policyOption = ReferencePolicyOption.GREEDY, fieldOption = FieldOption.REPLACE)
  volatile ShellScriptListener shellScriptListener = null;

  /**
   * Activator
   */
  @Activate
  void activate() {
    state.set(STATE_IDLE);
  }

  /**
   * Deactivator
   */
  @Deactivate
  void deactivate() {
    shutdownHook();
  }

  /** the pipeContainer between flac and lame */
  private AtomicReference<Pipe> pipeContainer = new AtomicReference<>();

  /**
   * Wait for completion of a process
   *
   * @param process
   *          The process for which to wait for completion
   * @return true when the process completed successfully
   */
  static boolean waitForProcessCompletion(Process process) {
    boolean complete = false;
    while (!complete //
        && (process != null)) {
      try {
        process.waitFor();
        complete = true;
      }
      catch (InterruptedException e) {
        return false;
      }
    }

    return complete;
  }

  /**
   * Do the flac to mp3 conversion by executing flac and lame with a pipeContainer in between the 2 processes.
   *
   * @param flac
   *          the flac (source) file
   * @param mp3
   *          the mp3 (destination) file
   * @param flacCommandList
   *          the command list used to execute flac
   * @param lameCommandList
   *          the command list used to execute lame
   * @return true when successful, false otherwise
   */
  boolean runConversionProcesses(File flac, File mp3, List<String> flacCommandList, List<String> lameCommandList) {
    Process flacProcess = null;
    Process lameProcess = null;
    int     pipeRetval  = -1;
    try {
      ProcessBuilder flacProcessBuilder = new ProcessBuilder(flacCommandList.toArray(new String[0]));
      ProcessBuilder lameProcessBuilder = new ProcessBuilder(lameCommandList.toArray(new String[0]));

      flacProcess = flacProcessBuilder.start();
      lameProcess = lameProcessBuilder.start();

      BufferedInputStream  flacOutputStream = new BufferedInputStream(flacProcess.getInputStream());
      BufferedOutputStream lameInputStream  = new BufferedOutputStream(lameProcess.getOutputStream());

      Pipe pipe = new Pipe(flacOutputStream, lameInputStream);
      pipeContainer.set(pipe);
      pipe.run();
      pipeRetval = pipe.getExitValue();
    }
    catch (Exception e) {
      logger.log(Level.WARNING, String.format(Messages.getString("FlacToMp3Impl.0"), flac.getPath(), mp3.getPath()), e); //$NON-NLS-1$
      pipeRetval = -1;
    }
    finally {
      pipeContainer.set(null);
    }

    waitForProcessCompletion(flacProcess);
    waitForProcessCompletion(lameProcess);

    int flacRetval = (flacProcess != null) ? flacProcess.exitValue() : -1;
    int lameRetval = (lameProcess != null) ? lameProcess.exitValue() : -1;

    return ((flacRetval == 0) //
        && (lameRetval == 0) //
        && (pipeRetval == 0));
  }

  /**
   * Set a tag field in an mp3 file. If fieldValue is null or empty then the tag field fieldName will be removed.
   *
   * @param mp3
   *          the mp3 file
   * @param mp3tag
   *          the tag of the mp3 file
   * @param fieldName
   *          the field name
   * @param fieldValue
   *          the field value
   * @param overRide
   *          true to override an existing value
   * @return true upon success, false otherwise
   */
  boolean setMp3TagField(File mp3, AbstractID3v2Tag mp3tag, FieldKey fieldName, String fieldValue, boolean overRide) {
    if (!overRide) {
      try {
        if (TagUtils.concatenateTagFields(mp3tag.getFields(fieldName), null) != null) {
          /* the tag field already has a value */
          return true;
        }
      }
      catch (Exception e) {
        logger.log(Level.WARNING, String.format(Messages.getString("FlacToMp3Impl.1"), fieldName, mp3.getPath()), e); //$NON-NLS-1$
        return false;
      }
    }

    try {
      if ((fieldValue == null) //
          || fieldValue.isEmpty()) {
        mp3tag.deleteField(fieldName);
      } else {
        mp3tag.setField(fieldName, fieldValue);
      }
    }
    catch (Exception e) {
      logger.log(Level.WARNING, String.format(Messages.getString("FlacToMp3Impl.2"), mp3.getPath()), e); //$NON-NLS-1$
      return false;
    }

    return true;
  }

  /**
   * Updates an mp3 tag from a map, used by {@link #convertFromId3V1}
   */
  static class Mp3TagUpdaterAction implements BiConsumer<FieldKey, String> {
    /** The logger to use */
    Logger logger;

    /** The mp3 tag to update */
    Tag mp3tag;

    /**
     * Constructor
     *
     * @param logger
     *          The logger to use
     * @param mp3tag
     *          The mp3 tag to update
     */
    Mp3TagUpdaterAction(Logger logger, Tag mp3tag) {
      this.logger = logger;
      this.mp3tag = mp3tag;
    }

    @Override
    public void accept(FieldKey key, String value) {
      if (value == null) {
        mp3tag.deleteField(key);
        return;
      }

      try {
        mp3tag.setField(key, value);
      }
      catch (KeyNotFoundException e) {
        logger.log(Level.WARNING,
            String.format(Messages.getString("FlacToMp3Impl.14"), key.toString(), mp3tag.getClass().getSimpleName()), //$NON-NLS-1$
            e);
      }
      catch (FieldDataInvalidException e) {
        logger.log(Level.WARNING,
            String.format(Messages.getString("FlacToMp3Impl.15"), value, mp3tag.getClass().getSimpleName()), //$NON-NLS-1$
            e);
      }
    }
  }

  /**
   * Convert an ID3v1 tag into an ID3v24 tag
   *
   * @param id3v1tag
   *          the ID3v1 tag
   * @return the ID3v24 tag
   */
  ID3v24Tag convertFromId3V1(ID3v1Tag id3v1tag) {
    Map<FieldKey, String> fieldValueMap = new LinkedHashMap<>();

    fieldValueMap.put(FieldKey.ALBUM, TagUtils.concatenateTagFields(id3v1tag.getAlbum(), null));
    /* no FieldKey.ALBUM_ARTIST, */
    fieldValueMap.put(FieldKey.ARTIST, TagUtils.concatenateTagFields(id3v1tag.getArtist(), null));
    fieldValueMap.put(FieldKey.YEAR, TagUtils.concatenateTagFields(id3v1tag.getYear(), null));
    /* no FieldKey.DISC_NO, */
    fieldValueMap.put(FieldKey.GENRE, TagUtils.concatenateTagFields(id3v1tag.getGenre(), null));
    fieldValueMap.put(FieldKey.TITLE, TagUtils.concatenateTagFields(id3v1tag.getTitle(), null));

    String track = (id3v1tag instanceof ID3v11Tag) ? TagUtils.concatenateTagFields(id3v1tag.getTrack(), null) : null;
    if ("0".equals(track)) { //$NON-NLS-1$
      track = null;
    }
    fieldValueMap.put(FieldKey.TRACK, track);
    /* no FieldKey.TRACK_TOTAL */

    ID3v24Tag mp3tag = new ID3v24Tag();
    fieldValueMap.forEach(new Mp3TagUpdaterAction(logger, mp3tag));
    return mp3tag;
  }

  /**
   * Update the tag of the mp3 file with tag information fetched from the corresponding flac file (tagInformation)
   *
   * @param mp3
   *          the mp3 file
   * @param tagInformation
   *          the tag information (fetched from the flac file)
   * @param overRide
   *          true to override the tag information in the mp3 file
   * @param v24
   *          true to set a v24 tag, false to set a v23 tag
   * @param v1
   *          true to set a v1 tag, false to not set a v1 tag
   * @return true upon success, false otherwise
   */
  boolean setMp3Tag(File mp3, TagInformation tagInformation, boolean overRide, boolean v24, boolean v1) {
    boolean result = true;

    try {
      MP3File mp3file = new MP3File(mp3);

      AbstractID3v2Tag mp3tag = new ID3v24Tag();

      if (mp3file.hasID3v2Tag()) {
        mp3tag = mp3file.getID3v2TagAsv24();
      } else if (mp3file.hasID3v1Tag()) {
        mp3tag = convertFromId3V1(mp3file.getID3v1Tag());
      }

      if (!v24) {
        mp3tag = new ID3v23Tag(mp3tag);
        mp3file.setTag(mp3tag);
      }

      result = setMp3TagFromTagInformation(mp3, tagInformation, mp3tag, overRide);

      if (result) {
        if (v1) {
          mp3file.setID3v1Tag(new ID3v11Tag(mp3tag));
        }
        mp3file.setID3v2Tag(mp3tag);
        mp3file.commit();
      }
    }
    catch (Throwable e) {
      logger.log(Level.WARNING, String.format(Messages.getString("FlacToMp3Impl.3"), mp3.getPath()), e); //$NON-NLS-1$
      result = false;
    }

    return result;
  }

  /**
   * Set fields in an mp3 tag based on tag information (do the actual work of
   * {@link #setMp3Tag(File, TagInformation, boolean, boolean, boolean)}).
   *
   * @param mp3
   *          The mp3 file
   * @param tagInformation
   *          The tag information (fetched from the flac file)
   * @param mp3tag
   *          The mp3 tag to set information in
   * @param overRide
   *          True to override an existing value
   * @return True upon success, false otherwise
   */
  boolean setMp3TagFromTagInformation(File mp3, TagInformation tagInformation, AbstractID3v2Tag mp3tag,
      boolean overRide) {
    if (!setMp3TagField(mp3, mp3tag, FieldKey.ALBUM, tagInformation.getAlbum(), overRide)) {
      return false;
    }

    if (!setMp3TagField(mp3, mp3tag, FieldKey.ALBUM_ARTIST, tagInformation.getAlbumArtist(), overRide)) {
      return false;
    }

    if (!setMp3TagField(mp3, mp3tag, FieldKey.ARTIST, tagInformation.getArtist(), overRide)) {
      return false;
    }

    if (!setMp3TagField(mp3, mp3tag, FieldKey.YEAR, tagInformation.getDate(), overRide)) {
      return false;
    }

    String discNumber = tagInformation.getDiscNumber().trim();
    String discTotal  = tagInformation.getDiscTotal().trim();

    if (!discNumber.isEmpty() //
        && !discTotal.isEmpty()) {
      if (!setMp3TagField(mp3, mp3tag, FieldKey.DISC_NO, discNumber, overRide)) {
        return false;
      }

      if (!setMp3TagField(mp3, mp3tag, FieldKey.DISC_TOTAL, discTotal, overRide)) {
        return false;
      }
    } else if (discNumber.matches("^\\d+\\s*/\\s*\\d+$")) { //$NON-NLS-1$
      String[] discNumberSplit = discNumber.split("\\s*/\\s*", 2); //$NON-NLS-1$
      if (!setMp3TagField(mp3, mp3tag, FieldKey.DISC_NO, discNumberSplit[0], overRide)) {
        return false;
      }

      if (!setMp3TagField(mp3, mp3tag, FieldKey.DISC_TOTAL, discNumberSplit[1], overRide)) {
        return false;
      }
    } else {
      if (!setMp3TagField(mp3, mp3tag, FieldKey.DISC_NO, discNumber, overRide)) {
        return false;
      }

      if (!setMp3TagField(mp3, mp3tag, FieldKey.DISC_TOTAL, discTotal, overRide)) {
        return false;
      }
    }

    if (!setMp3TagField(mp3, mp3tag, FieldKey.GENRE, tagInformation.getGenre(), overRide)) {
      return false;
    }

    if (!setMp3TagField(mp3, mp3tag, FieldKey.TITLE, tagInformation.getTitle(), overRide)) {
      return false;
    }

    if (!setMp3TagField(mp3, mp3tag, FieldKey.TRACK, tagInformation.getTrackNumber(), overRide)) {
      return false;
    }

    if (!setMp3TagField(mp3, mp3tag, FieldKey.TRACK_TOTAL, tagInformation.getTrackTotal(), overRide)) {
      return false;
    }

    return true;
  }

  /**
   * Update the timestamp on an mp3 file: copy the timestamp from the flac file. If this operation fails then that's ok:
   * the mp3 file will have a newer timestamp than the flac file anyway.
   *
   * @param flac
   *          the flac file
   * @param mp3
   *          the mp3 file
   * @return true
   */
  boolean copyTimestamp(File flac, File mp3) {
    if (!mp3.setLastModified(flac.lastModified())) {
      logger.log(Level.INFO, String.format(Messages.getString("FlacToMp3Impl.4"), mp3.getPath())); //$NON-NLS-1$
    }

    return true;
  }

  /**
   * Read the tag information from a flac file
   *
   * @param flacFile
   *          the flac file
   * @return the tag information, or null when reading the tag information failed or the tag is not a flac tag.
   */
  TagInformation readTag(File flacFile) {
    AudioFile af;
    try {
      af = AudioFileIO.read(flacFile);
    }
    catch (Throwable e) {
      logger.log(Level.WARNING, String.format(Messages.getString("FlacToMp3Impl.5"), flacFile.getPath()), e); //$NON-NLS-1$
      return null;
    }

    Tag tag = af.getTag();
    if (!(tag instanceof FlacTag)) {
      /* can't be covered by a test */
      return null;
    }

    return new TagInformation((FlacTag) tag);
  }

  /**
   * Remove an incomplete mp3 file.
   *
   * @param mp3
   *          the incomplete mp3 file
   */
  void removeIncompleteMp3File(File mp3) {
    if (mp3.exists()) {
      boolean             removed  = mp3.delete();
      ShellScriptListener listener = shellScriptListener;
      if (listener != null) {
        if (!removed) {
          /* can't be covered by a test */
          listener.addMessage(String.format(Messages.getString("FlacToMp3Impl.6"), //$NON-NLS-1$
              mp3.getPath()));
        } else {
          listener.addMessage(String.format(Messages.getString("FlacToMp3Impl.7"), mp3.getPath())); //$NON-NLS-1$
        }
      }
    }
  }

  /*
   * State
   */

  /** idle state */
  private static final int STATE_IDLE = 0;

  /** running state */
  private static final int STATE_RUNNING = 1;

  /** stopping state */
  private static final int STATE_STOPPING = 2;

  /** the state of the conversion */
  private AtomicInteger state = new AtomicInteger(STATE_IDLE);

  @Override
  public boolean convert(Flac2Mp3Configuration configuration, File flac, File mp3, boolean simulate)
      throws FileNotFoundException {
    if (!state.compareAndSet(STATE_IDLE, STATE_RUNNING)) {
      /* we were not idle, so we can't run */
      return false;
    }

    Flac2Mp3Configuration config = configuration;
    if (config == null) {
      config = new Flac2Mp3Configuration();
    }

    boolean deleteIncomplete     = false;
    boolean successfulConversion = true;

    try {
      /* Check that the flac file exists */
      if ((flac == null) //
          || !flac.isFile()) {
        throw new FileNotFoundException(String.format(Messages.getString("FlacToMp3Impl.9"), flac != null //$NON-NLS-1$
            ? flac.getPath()
            : Messages.getString("FlacToMp3Impl.10"))); //$NON-NLS-1$
      }

      /* Progress message */
      ShellScriptListener listener = shellScriptListener;
      if (listener != null) {
        listener.addMessage(String.format(Messages.getString("FlacToMp3Impl.8"), flac.getPath())); //$NON-NLS-1$
      }

      /* Check that the mp3 file doesn't exist or is a regular file */
      if ((mp3 == null) //
          || (mp3.exists() //
              && !mp3.isFile())) {
        throw new FileNotFoundException(
            String.format(Messages.getString("FlacToMp3Impl.11"), mp3 != null ? mp3.getPath() : "NULL")); //$NON-NLS-1$//$NON-NLS-2$
      }

      /* Read the tag from the flac file */
      TagInformation tagInformation = readTag(flac);
      if (tagInformation == null) {
        /* can't be covered by a test */
        return false;
      }

      /* Create directory for mp3 file */
      File mp3Dir = mp3.getParentFile();
      listener = shellScriptListener;
      if (!mp3Dir.exists() //
          && (listener != null)) {
        listener.addCommand(String.format("mkdir -p \"%s\"", StringUtils.escQuote(mp3Dir.getPath()))); //$NON-NLS-1$
      }
      boolean mp3DirCreated = false;
      try {
        mp3DirCreated = (state.get() == STATE_RUNNING) //
            && (simulate //
                || DirUtils.mkdir(mp3Dir));
      }
      catch (FileAlreadyExistsException e) {
        /* can't be covered by a test */
        logger.log(Level.WARNING, String.format(Messages.getString("FlacToMp3Impl.12"), e.getMessage())); //$NON-NLS-1$
      }
      if (!mp3DirCreated) {
        return false;
      }

      /*
       * Convert flac to mp3
       */

      if (config.isRunFlacLame()) {
        /* flac */
        List<String> flacCommandList = new LinkedList<>();

        flacCommandList.add(config.getFlacExecutable());

        List<String> options = config.getFlacOptions();
        if (options != null) {
          flacCommandList.addAll(options);
        }
        flacCommandList.add(flac.getPath());

        /* lame */
        List<String> lameCommandList = new LinkedList<>();

        lameCommandList.add(config.getLameExecutable());

        options = config.getLameOptions();
        if (options != null) {
          lameCommandList.addAll(options);
        }
        if (config.isCopyTag()) {
          lameCommandList.add("--add-id3v2"); //$NON-NLS-1$
          lameCommandList.add("--pad-id3v2"); //$NON-NLS-1$
          lameCommandList.add("--ta"); //$NON-NLS-1$
          lameCommandList.add(tagInformation.getArtist());
          lameCommandList.add("--tl"); //$NON-NLS-1$
          lameCommandList.add(tagInformation.getAlbum());
          lameCommandList.add("--tt"); //$NON-NLS-1$
          lameCommandList.add(tagInformation.getTitle());
          lameCommandList.add("--tg"); //$NON-NLS-1$
          lameCommandList.add(tagInformation.getGenre());
          lameCommandList.add("--ty"); //$NON-NLS-1$
          lameCommandList.add(tagInformation.getDate());
          lameCommandList.add("--tn"); //$NON-NLS-1$
          lameCommandList.add(tagInformation.getTrackNumber() + "/" + //$NON-NLS-1$
              tagInformation.getTrackTotal());
        }
        lameCommandList.add("-"); //$NON-NLS-1$
        lameCommandList.add(mp3.getPath());

        listener = shellScriptListener;
        if (listener != null) {
          listener.addCommand(String.format("%s | \\%n%s", //$NON-NLS-1$
              listener.commandListToString(flacCommandList, 0), listener.commandListToString(lameCommandList, 4)));
        }

        deleteIncomplete     = false;
        successfulConversion = false;
        if (state.get() == STATE_RUNNING) {
          if (simulate) {
            successfulConversion = true;
          } else {
            deleteIncomplete     = true;
            successfulConversion = runConversionProcesses(flac, mp3, flacCommandList, lameCommandList);
            deleteIncomplete     = !successfulConversion;
          }
        }
        if (!successfulConversion) {
          return false;
        }
      }

      /*
       * Set tag on mp3 (from the tag that we read from the flac file)
       */

      if (config.isCopyTag() //
          && (mp3.exists() //
              || simulate)) {
        listener = shellScriptListener;
        if (listener != null) {
          List<String> commandList = new LinkedList<>();
          commandList.add("id3v2"); //$NON-NLS-1$
          commandList.add("-a"); //$NON-NLS-1$
          commandList.add(tagInformation.getArtist());
          commandList.add("-A"); //$NON-NLS-1$
          commandList.add(tagInformation.getAlbum());
          commandList.add("--TPE2"); //$NON-NLS-1$
          commandList.add(tagInformation.getAlbumArtist());
          commandList.add("-t"); //$NON-NLS-1$
          commandList.add(tagInformation.getTitle());
          commandList.add("-g"); //$NON-NLS-1$
          commandList.add(tagInformation.getGenre());
          commandList.add("-y"); //$NON-NLS-1$
          commandList.add(tagInformation.getDate());
          commandList.add("-T"); //$NON-NLS-1$
          commandList.add(tagInformation.getTrackNumber() + "/" + tagInformation.getTrackTotal()); //$NON-NLS-1$
          commandList.add("--TPOS"); //$NON-NLS-1$

          {
            String discNumber = tagInformation.getDiscNumber().trim();
            String discTotal  = tagInformation.getDiscTotal().trim();

            if (!discNumber.isEmpty() //
                && !discTotal.isEmpty()) {
              commandList.add(discNumber + "/" + discTotal); //$NON-NLS-1$
            } else if (discNumber.matches("^\\d+\\s*/\\s*\\d+$")) { //$NON-NLS-1$
              commandList.add(discNumber);
            } else {
              commandList.add(discNumber + "/" + discTotal); //$NON-NLS-1$
            }
          }

          commandList.add(mp3.getPath());
          listener.addCommand(listener.commandListToString(commandList, 0));
        }

        deleteIncomplete     = false;
        successfulConversion = false;
        if (state.get() == STATE_RUNNING) {
          if (simulate) {
            successfulConversion = true;
          } else {
            deleteIncomplete     = true;
            successfulConversion =
                setMp3Tag(mp3, tagInformation, true, config.isUseId3V24Tags(), config.isUseId3V1Tags());
            deleteIncomplete     = !successfulConversion;
          }
        }
        if (!successfulConversion) {
          return false;
        }
      }

      /*
       * Set the timestamp of the flac file on the mp3 file
       */

      if (config.isCopyTimestamp() //
          && (mp3.exists() //
              || simulate)) {
        listener = shellScriptListener;
        if (listener != null) {
          listener.addCommand(String.format("touch --reference=\"%s\" \\%n                  \"%s\"", //$NON-NLS-1$
              StringUtils.escQuote(flac.getPath()), StringUtils.escQuote(mp3.getPath())));
        }

        successfulConversion = (state.get() == STATE_RUNNING) //
            && (simulate //
                || copyTimestamp(flac, mp3));
        if (!successfulConversion) {
          return false;
        }
      }
    }
    finally {
      if (!successfulConversion) {
        String p = (mp3 == null) ? "null" : mp3.getPath(); //$NON-NLS-1$
        logger.log(Level.WARNING, String.format(Messages.getString("FlacToMp3Impl.13"), flac.getPath(), p)); //$NON-NLS-1$
        if (deleteIncomplete) {
          removeIncompleteMp3File(mp3);
        }
      }

      /*
       * only clear the state here, to make sure that an incomplete mp3 file is removed
       */
      state.set(STATE_IDLE);
    }

    return true;
  }

  /*
   * ShutdownHookParticipant
   */

  @Override
  public void shutdownHook() {
    int currentState = state.getAndSet(STATE_STOPPING);

    if (currentState != STATE_RUNNING) {
      /* we were not running: no need to wait for idle */
      return;
    }

    /* signal the pipeContainer to stop */
    Pipe pipe = pipeContainer.get();
    if (pipe != null) {
      pipe.signalStop();
    }

    /* wait for idle of the conversion */
    while (state.get() != STATE_IDLE) {
      try {
        Thread.sleep(1);
      }
      catch (InterruptedException e) {
        /* swallow */
      }
    }
  }
}