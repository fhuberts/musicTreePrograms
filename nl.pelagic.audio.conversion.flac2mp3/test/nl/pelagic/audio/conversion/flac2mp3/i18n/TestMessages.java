package nl.pelagic.audio.conversion.flac2mp3.i18n;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "static-method",
    "unused"
})
public class TestMessages {
  @Test(timeout = 8000)
  public void testGetString_Null() {
    Messages m = new Messages();
    String   s = Messages.getString(null);
    assertThat(s, equalTo(null));
  }

  @Test(timeout = 8000)
  public void testGetString_Tests() {
    String in = "Tests.0";             //$NON-NLS-1$
    String s  = Messages.getString(in);
    assertThat(s, equalTo("Do not translate or remove; used in tests")); //$NON-NLS-1$
  }

  @Test(timeout = 8000)
  public void testGetString_NotThere() {
    String in = "Tests.Is.Not.There";  //$NON-NLS-1$
    String s  = Messages.getString(in);
    assertThat(s, equalTo('!' + in + '!'));
  }
}
