package nl.pelagic.audio.conversion.flac2mp3.testhelpers;

import org.jaudiotagger.tag.FieldDataInvalidException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.KeyNotFoundException;
import org.jaudiotagger.tag.id3.ID3v11Tag;
import org.junit.Ignore;

@Ignore
@SuppressWarnings("javadoc")
public class ThrowingTag extends ID3v11Tag {

  public KeyNotFoundException      keyNotFoundException      = null;
  public FieldDataInvalidException fieldDataInvalidException = null;

  @Override
  public void setField(FieldKey genericKey, String... value) throws KeyNotFoundException, FieldDataInvalidException {
    if ((keyNotFoundException == null) && (fieldDataInvalidException == null)) {
      super.setField(genericKey, value);
      return;
    }

    if (keyNotFoundException != null) {
      throw keyNotFoundException;
    }

    if (fieldDataInvalidException != null) {
      throw fieldDataInvalidException;
    }
  }
}
