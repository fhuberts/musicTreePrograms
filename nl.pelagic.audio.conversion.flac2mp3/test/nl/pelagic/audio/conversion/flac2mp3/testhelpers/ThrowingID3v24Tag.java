package nl.pelagic.audio.conversion.flac2mp3.testhelpers;

import java.util.List;

import org.jaudiotagger.tag.FieldDataInvalidException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.KeyNotFoundException;
import org.jaudiotagger.tag.TagField;
import org.jaudiotagger.tag.id3.ID3v24Tag;
import org.junit.Ignore;

@Ignore
@SuppressWarnings("javadoc")
public class ThrowingID3v24Tag extends ID3v24Tag {

  public FieldKey getFieldsThrowOn   = null;
  public FieldKey deleteFieldThrowOn = null;
  public FieldKey setFieldThrowOn    = null;

  @Override
  public List<TagField> getFields(FieldKey genericKey) throws KeyNotFoundException {
    if ((getFieldsThrowOn != null) && (getFieldsThrowOn == genericKey)) {
      throw new KeyNotFoundException();
    }

    return super.getFields(genericKey);
  }

  @Override
  public void deleteField(FieldKey fieldKey) throws KeyNotFoundException {
    if ((deleteFieldThrowOn != null) && (deleteFieldThrowOn == fieldKey)) {
      throw new KeyNotFoundException();
    }

    super.deleteField(fieldKey);
  }

  @Override
  public void setField(FieldKey genericKey, String... values) throws KeyNotFoundException, FieldDataInvalidException {
    if ((setFieldThrowOn != null) && (setFieldThrowOn == genericKey)) {
      throw new KeyNotFoundException();
    }

    super.setField(genericKey, values);
  }

}
