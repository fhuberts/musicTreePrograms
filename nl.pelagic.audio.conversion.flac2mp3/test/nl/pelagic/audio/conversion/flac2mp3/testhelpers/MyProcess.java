package nl.pelagic.audio.conversion.flac2mp3.testhelpers;

import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Ignore;

@Ignore
@SuppressWarnings("javadoc")
public class MyProcess extends Process {
  @Override
  public OutputStream getOutputStream() {
    throw new IllegalStateException();
  }

  @Override
  public InputStream getInputStream() {
    throw new IllegalStateException();
  }

  @Override
  public InputStream getErrorStream() {
    throw new IllegalStateException();
  }

  public int exitValue;
  public int waitForReturnAfter = 0;

  @Override
  public int waitFor() throws InterruptedException {
    Thread.sleep(waitForReturnAfter);
    return exitValue;
  }

  @Override
  public int exitValue() {
    return exitValue;
  }

  @Override
  public void destroy() {
    throw new IllegalStateException();
  }
}
