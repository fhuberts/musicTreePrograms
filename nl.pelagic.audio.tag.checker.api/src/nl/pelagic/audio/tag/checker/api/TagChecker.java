package nl.pelagic.audio.tag.checker.api;

import nl.pelagic.audio.tag.checker.types.GenericTag;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Interface of a bundle that checks a generic tag against certain constraints
 */
@ConsumerType
public interface TagChecker {
  /**
   * Checks a generic tag against certain constraints. When a constraint is not met, one or more reports are created (in
   * the generic tag) for each field that fails the constraints.
   *
   * @param genericTag
   *          the generic tag to check (and to add reports to)
   */
  void check(GenericTag genericTag);

  /**
   * @return true when the tag checker is enabled by default. Called when the tag checker is registered.
   */
  boolean isEnabledByDefault();
}