package nl.pelagic.audio.tag.checker.api;

import java.io.File;
import java.util.List;
import java.util.Map;

import nl.pelagic.audio.tag.checker.types.CheckProcessFile;
import nl.pelagic.audio.tag.checker.types.CheckProcessInstruction;
import nl.pelagic.audio.tag.checker.types.CheckProcessReports;

/**
 * Interface of a service that plugs into the check process.
 *
 * The check process is depicted below.
 *
 * <pre>
 * +---------------------+ When there are no non-directory files in the directory
 * |   directoryEntry    |------------------------------------------+
 * +---------------------+                                          V
 *            |                                                     |
 *            V                                                     |
 * +---------------------+                                          |
 * |     filesEntry      |----+ When not returning                  |
 * +---------------------+    V CheckProcessInstruction.CONTINUE    |
 *            |               |                                     |
 *            V               |                                     |
 * +---------------------+    V                                     |
 * |   filesExtensions   |----+ When not returning                  |
 * +---------------------+    V CheckProcessInstruction.CONTINUE    |
 *            |               |                                     |
 *            V               |                                     |
 * +---------------------+    V                                     |
 * |      filesRead      |----+ When not returning                  |
 * +---------------------+    V CheckProcessInstruction.CONTINUE    |
 *            |               |                                     |
 *            V               |                                     |
 * +---------------------+    V                                     |
 * |    filesTagsRead    |----+ When not returning                  |
 * +---------------------+    V CheckProcessInstruction.CONTINUE    |
 *            |               |                                     |
 *            V               |                                     |
 * +---------------------+    |                                     |
 * | filesTagsConverted  |    |                                     |
 * +---------------------+    |                                     |
 *            |               |                                     |
 *            V               |                                     |
 * +---------------------+    |                                     |
 * |  [checks are run]   |    |                                     |
 * +---------------------+    |                                     |
 *            |               |                                     |
 *            V               |                                     |
 * +---------------------+    V                                     |
 * |      filesExit      |----+                                     |
 * +---------------------+                                          |
 *            |                                                     |
 *            V                                                     |
 * +---------------------+                                          V
 * |    directoryExit    |------------------------------------------+
 * +---------------------+
 * </pre>
 */
public interface CheckProcessPlugin {

  /**
   * @return true when the check process plugin is enabled by default.
   */
  boolean isEnabledByDefault();

  /*
   * Directory
   */

  /**
   * See the comment on {@link CheckProcessPlugin} for the calling order.
   *
   * @param directory
   *          the directory that is entered
   * @param directories
   *          the (filtered) files in the directory that are directories, can not be null but can be empty
   * @param regularFiles
   *          the (filtered) files in the directory that are non-directory files, can not be null but can be empty
   */
  void directoryEntry(File directory, List<File> directories, List<File> regularFiles);

  /**
   * See the comment on {@link CheckProcessPlugin} for the calling order.
   *
   * @param directory
   *          the directory that is exited
   * @return {@link CheckProcessInstruction#RESTART_DIRECTORY} when the directory has to be processed again, otherwise
   *         return {@link CheckProcessInstruction#CONTINUE}
   */
  CheckProcessInstruction directoryExit(File directory);

  /*
   * File
   */

  /**
   * See the comment on {@link CheckProcessPlugin} for the calling order.
   *
   * The map values will only have the {@link CheckProcessFile#file} field set (which is the same as the corresponding
   * key), the rest of the fields will be null.
   *
   * @param map
   *          a map of files against their gathered (check process) data
   * @param errors
   *          a list of files that is to be filled by this methods with files that have to be removed from the check
   *          process when this method returns
   * @param reports
   *          a map to fill with files against their notices and reports. The reports map is pre-filled and contains all
   *          files that are in map, with empty notices and errors
   * @return {@link CheckProcessInstruction#RESTART_DIRECTORY} when the directory has to be processed again, otherwise
   *         return {@link CheckProcessInstruction#CONTINUE}
   */
  CheckProcessInstruction filesEntry(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports);

  /**
   * See the comment on {@link CheckProcessPlugin} for the calling order.
   *
   * The map values will only have the {@link CheckProcessFile#file} and {@link CheckProcessFile#extension} fields set,
   * the rest of the fields will be null.
   *
   * @param map
   *          a map of files against their gathered (check process) data
   * @param errors
   *          a list of files that is to be filled by this methods with files that have to be removed from the check
   *          process when this method returns
   * @param reports
   *          a map to fill with files against their notices and reports. The reports map is pre-filled and contains all
   *          files that are in map, with empty notices and errors
   * @return {@link CheckProcessInstruction#RESTART_DIRECTORY} when the directory has to be processed again, otherwise
   *         return {@link CheckProcessInstruction#CONTINUE}
   */
  CheckProcessInstruction filesExtensions(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports);

  /**
   * See the comment on {@link CheckProcessPlugin} for the calling order.
   *
   * The map values will only have the {@link CheckProcessFile#file}, {@link CheckProcessFile#extension} and
   * {@link CheckProcessFile#af} fields set, the rest of the fields will be null.
   *
   * @param map
   *          a map of files against their gathered (check process) data
   * @param errors
   *          a list of files that is to be filled by this methods with files that have to be removed from the check
   *          process when this method returns
   * @param reports
   *          a map to fill with files against their notices and reports. The reports map is pre-filled and contains all
   *          files that are in map, with empty notices and errors
   * @return {@link CheckProcessInstruction#RESTART_DIRECTORY} when the directory has to be processed again, otherwise
   *         return {@link CheckProcessInstruction#CONTINUE}
   */
  CheckProcessInstruction filesRead(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports);

  /**
   * See the comment on {@link CheckProcessPlugin} for the calling order.
   *
   * The map values will only have the {@link CheckProcessFile#file}, {@link CheckProcessFile#extension},
   * {@link CheckProcessFile#af} and {@link CheckProcessFile#tag} fields set, the rest of the fields will be null.
   *
   * @param map
   *          a map of files against their gathered (check process) data
   * @param errors
   *          a list of files that is to be filled by this methods with files that have to be removed from the check
   *          process when this method returns
   * @param reports
   *          a map to fill with files against their notices and reports. The reports map is pre-filled and contains all
   *          files that are in map, with empty notices and errors
   * @return {@link CheckProcessInstruction#RESTART_DIRECTORY} when the directory has to be processed again, otherwise
   *         return {@link CheckProcessInstruction#CONTINUE}
   */
  CheckProcessInstruction filesTagsRead(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports);

  /**
   * See the comment on {@link CheckProcessPlugin} for the calling order.
   *
   * The map values will have all fields set.
   *
   * @param map
   *          a map of files against their gathered (check process) data
   * @param errors
   *          a list of files that is to be filled by this methods with files that have to be removed from the check
   *          process when this method returns
   * @param reports
   *          a map to fill with files against their notices and reports. The reports map is pre-filled and contains all
   *          files that are in map, with empty notices and errors
   * @return {@link CheckProcessInstruction#RESTART_DIRECTORY} when the directory has to be processed again, otherwise
   *         return {@link CheckProcessInstruction#CONTINUE}
   */
  CheckProcessInstruction filesTagsConverted(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports);

  /**
   * See the comment on {@link CheckProcessPlugin} for the calling order.
   *
   * The map values will have all fields set.
   *
   * @param map
   *          a map of files against their gathered (check process) data
   * @param reports
   *          a map to fill with files against their notices and reports. The reports map is pre-filled and contains all
   *          files that are in map, with empty notices and errors
   * @return {@link CheckProcessInstruction#RESTART_DIRECTORY} when the directory has to be processed again, otherwise
   *         return {@link CheckProcessInstruction#CONTINUE}
   */
  CheckProcessInstruction filesExit(Map<File, CheckProcessFile> map, Map<File, CheckProcessReports> reports);
}