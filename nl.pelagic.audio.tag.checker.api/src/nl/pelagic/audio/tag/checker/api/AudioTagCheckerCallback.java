package nl.pelagic.audio.tag.checker.api;

import java.io.File;
import java.util.List;
import java.util.Map;

import nl.pelagic.audio.tag.checker.types.CheckProcessReports;
import nl.pelagic.audio.tag.checker.types.GenericTag;

import org.jaudiotagger.tag.Tag;
import org.osgi.annotation.versioning.ConsumerType;

/**
 * Interface for users of the AudioTagChecker interface.
 *
 * A service that implements the AudioTagChecker interface will report detailed results through this interface to the
 * user of its interface.
 */
@ConsumerType
public interface AudioTagCheckerCallback {

  /*
   * Per-File Section
   */

  /**
   * Called when a file has an unsupported extension.
   *
   * @param file
   *          the file
   */
  void unsupportedExtension(File file);

  /**
   * Called when the file was not readable.
   *
   * @param file
   *          the unreadable file
   * @param e
   *          the exception that occurred
   */
  void notReadable(File file, Exception e);

  /**
   * Called when the file had no tag.
   *
   * @param file
   *          the file without a tag
   */
  void noTag(File file);

  /**
   * Called when the file had a tag that could not be converted into a generic tag.
   *
   * @param file
   *          the file
   * @param tag
   *          the tag that could not be converted into a generic tag
   */
  void tagNotConverted(File file, Tag tag);

  /**
   * Called when the file had a tag didn't pass all checks.
   *
   * @param tag
   *          the tag (the tag contains the file)
   */
  void checksFailed(GenericTag tag);

  /**
   * Called when the file had a tag that passes all checks.
   *
   * @param tag
   *          the tag (the tag contains the file)
   */
  void checksPassed(GenericTag tag);

  /*
   * Batching Section
   */

  /**
   * Called when there are files with unsupported extensions.
   *
   * @param files
   *          the list of files with unsupported extensions
   */
  void unsupportedExtension(List<File> files);

  /**
   * Called when there are unreadable files
   *
   * @param files
   *          the map of unreadable file and their exceptions
   */
  void notReadable(Map<File, Exception> files);

  /**
   * Called when there are files without tags.
   *
   * @param files
   *          the list of files without a tag
   */
  void noTag(List<File> files);

  /**
   * Called when there are files with tags that could not be converted into a generic tags.
   *
   * @param files
   *          the files and their tags
   */
  void tagNotConverted(Map<File, Tag> files);

  /**
   * Called when there are files with tags that didn't pass all checks.
   *
   * @param tags
   *          the tags (each tag contains the file)
   */
  void checksFailed(List<GenericTag> tags);

  /**
   * Called when there are files with tags that passed all checks.
   *
   * @param tags
   *          the tags (each tag contains the file)
   */
  void checksPassed(List<GenericTag> tags);

  /**
   * Report a list of notices and/or a list of errors
   *
   * @param reports
   *          a map of files against their notices and reports. Never null or empty: always contains all files being
   *          processed, but their reports may be empty (use {@link CheckProcessReports#isEmpty()} to check).
   */
  void report(Map<File, CheckProcessReports> reports);
}
