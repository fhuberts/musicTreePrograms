package nl.pelagic.audio.tag.checker.types;

import java.io.File;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.tag.Tag;
import org.osgi.annotation.versioning.ProviderType;

/**
 * The state of a file during the check process.
 */
@ProviderType
public class CheckProcessFile {
  /**
   * The file
   */
  public File file = null;

  /**
   * The extension of the filename
   */
  public String extension = null;

  /**
   * The read audio file
   */
  public AudioFile af = null;

  /**
   * The actual tag
   */
  public Tag tag = null;

  /**
   * The generic tag
   */
  public GenericTag genericTag = null;
}