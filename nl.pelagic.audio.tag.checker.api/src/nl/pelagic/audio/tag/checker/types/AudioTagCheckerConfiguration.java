package nl.pelagic.audio.tag.checker.types;

import java.io.File;
import java.util.Set;
import java.util.regex.Pattern;

import nl.pelagic.audio.tag.checker.api.CheckProcessPlugin;
import nl.pelagic.audio.tag.checker.api.TagChecker;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Configuration for the AudioTagChecker interface
 */
@ProviderType
public class AudioTagCheckerConfiguration {
  /** The default value for the recursiveScan setting */
  public static final boolean DEFAULT_RECURSIVESCAN = false;

  /** The default value for the regexInAllDirs setting */
  public static final boolean DEFAULT_REGEXINALLDIRS = false;

  /**
   * Default Constructor.
   */
  public AudioTagCheckerConfiguration() {
    super();
  }

  /**
   * Minimal Constructor.
   *
   * @param checkPath
   *          the path to check
   */
  public AudioTagCheckerConfiguration(File checkPath) {
    super();
    this.checkPath = checkPath;
  }

  /**
   * Checks whether an object's class name or simple name is in the provided set
   *
   * @param set
   *          the set to check
   * @param obj
   *          the object
   * @return true when the set is not null and when the object is not null and when the object's simple class name or
   *         the object's class name is in the set
   */
  static boolean isInEnabledDisabledSet(Set<String> set, Object obj) {
    return (obj != null) //
        && (set != null) //
        && (set.contains(obj.getClass().getSimpleName()) //
            || set.contains(obj.getClass().getName()));
  }

  /** The path to check */
  private File checkPath = null;

  /**
   * @return the checkPath
   */
  public File getCheckPath() {
    return checkPath;
  }

  /**
   * @param checkPath
   *          the checkPath to set
   */
  public void setCheckPath(File checkPath) {
    this.checkPath = checkPath;
  }

  /** True when a recursive scan must be performed */
  private boolean recursiveScan = DEFAULT_RECURSIVESCAN;

  /**
   * @return the recursiveScan
   */
  public boolean isRecursiveScan() {
    return recursiveScan;
  }

  /**
   * @param recursiveScan
   *          the recursiveScan to set
   */
  public void setRecursiveScan(boolean recursiveScan) {
    this.recursiveScan = recursiveScan;
  }

  /**
   * True when the compiled regular expression must be applied in all directories, false when it must only be applied in
   * the checkPath directory.
   */
  private boolean regexInAllDirs = DEFAULT_REGEXINALLDIRS;

  /**
   * @return the regexInAllDirs
   */
  public boolean isRegexInAllDirs() {
    return regexInAllDirs;
  }

  /**
   * @param regexInAllDirs
   *          the regexInAllDirs to set
   */
  public void setRegexInAllDirs(boolean regexInAllDirs) {
    this.regexInAllDirs = regexInAllDirs;
  }

  /** The compiled regular expression to use for filename matching */
  private Pattern regexPattern = null;

  /**
   * @return the regexPattern
   */
  public Pattern getRegexPattern() {
    return regexPattern;
  }

  /**
   * @param regexPattern
   *          the regexPattern to set
   */
  public void setRegexPattern(Pattern regexPattern) {
    this.regexPattern = regexPattern;
  }

  /**
   * A list of enabled tag checkers
   *
   * @see #isTagCheckerEnabled
   */
  private Set<String> enabledTagCheckers = null;

  /**
   * @return the enabledTagCheckers
   */
  public Set<String> getEnabledTagCheckers() {
    return enabledTagCheckers;
  }

  /**
   * @param enabledTagCheckers
   *          the enabledTagCheckers to set
   */
  public void setEnabledTagCheckers(Set<String> enabledTagCheckers) {
    this.enabledTagCheckers = enabledTagCheckers;
  }

  /**
   * A list of disabled tag checkers
   *
   * @see #isTagCheckerDisabled
   */
  private Set<String> disabledTagCheckers = null;

  /**
   * @return the disabledTagCheckers
   */
  public Set<String> getDisabledTagCheckers() {
    return disabledTagCheckers;
  }

  /**
   * @param disabledTagCheckers
   *          the disabledTagCheckers to set
   */
  public void setDisabledTagCheckers(Set<String> disabledTagCheckers) {
    this.disabledTagCheckers = disabledTagCheckers;
  }

  /**
   * <p>
   * Determine if a tag checker is enabled:
   * </p>
   * <ul>
   * <li>When {@link #enabledTagCheckers} is set (not null and not empty), and</li>
   * <li>When the specified tag checker is listed in {@link #enabledTagCheckers}</li>
   * </ul>
   *
   * <p>
   * A tag checker can be be listed by its:
   * </p>
   * <ul>
   * <li>TagChecker.getClass().getName(), or</li>
   * <li>TagChecker.getClass().getSimpleName()</li>
   * </ul>
   *
   * @param tagChecker
   *          the tag checker
   * @return true when the specified tag checker is enabled
   */
  public boolean isTagCheckerEnabled(TagChecker tagChecker) {
    return isInEnabledDisabledSet(enabledTagCheckers, tagChecker);
  }

  /**
   * <p>
   * Determine if a tag checker is disabled:
   * </p>
   * <ul>
   * <li>When {@link #disabledTagCheckers} is set (not null and not empty), and</li>
   * <li>When the specified tag checker is listed in {@link #disabledTagCheckers}</li>
   * </ul>
   *
   * <p>
   * A tag checker can be be listed by its:
   * </p>
   * <ul>
   * <li>TagChecker.getClass().getName(), or</li>
   * <li>TagChecker.getClass().getSimpleName()</li>
   * </ul>
   *
   * @param tagChecker
   *          the tag checker
   * @return true when the specified tag checker is disabled
   */
  public boolean isTagCheckerDisabled(TagChecker tagChecker) {
    return isInEnabledDisabledSet(disabledTagCheckers, tagChecker);
  }

  /**
   * A list of enabled check process plugins
   *
   * @see #isCheckProcessPluginEnabled
   */
  private Set<String> enabledCheckProcessPlugins = null;

  /**
   * @return the enabledCheckProcessPlugins
   */
  public Set<String> getEnabledCheckProcessPlugins() {
    return enabledCheckProcessPlugins;
  }

  /**
   * @param enabledCheckProcessPlugins
   *          the enabledCheckProcessPlugins to set
   */
  public void setEnabledCheckProcessPlugins(Set<String> enabledCheckProcessPlugins) {
    this.enabledCheckProcessPlugins = enabledCheckProcessPlugins;
  }

  /**
   * A list of disabled check process plugins
   *
   * @see #isCheckProcessPluginDisabled
   */
  private Set<String> disabledCheckProcessPlugins = null;

  /**
   * @return the disabledCheckProcessPlugins
   */
  public Set<String> getDisabledCheckProcessPlugins() {
    return disabledCheckProcessPlugins;
  }

  /**
   * @param disabledCheckProcessPlugins
   *          the disabledCheckProcessPlugins to set
   */
  public void setDisabledCheckProcessPlugins(Set<String> disabledCheckProcessPlugins) {
    this.disabledCheckProcessPlugins = disabledCheckProcessPlugins;
  }

  /**
   * <p>
   * Determine if a check process plugin is enabled:
   * </p>
   * <ul>
   * <li>When {@link #enabledCheckProcessPlugins} is set (not null and not empty), and</li>
   * <li>When the specified check process plugin is listed in {@link #enabledCheckProcessPlugins}</li>
   * </ul>
   *
   * <p>
   * A check process plugin can be be listed by its:
   * </p>
   * <ul>
   * <li>CheckProcessPlugin.getClass().getName(), or</li>
   * <li>CheckProcessPlugin.getClass().getSimpleName()</li>
   * </ul>
   *
   * @param checkProcessPlugin
   *          the check process plugin
   * @return true when the specified check process plugin is enabled
   */
  public boolean isCheckProcessPluginEnabled(CheckProcessPlugin checkProcessPlugin) {
    return isInEnabledDisabledSet(enabledCheckProcessPlugins, checkProcessPlugin);
  }

  /**
   * <p>
   * Determine if a check process plugin is disabled:
   * </p>
   * <ul>
   * <li>When {@link #disabledCheckProcessPlugins} is set (not null and not empty), and</li>
   * <li>When the specified check process plugin is listed in {@link #disabledCheckProcessPlugins}</li>
   * </ul>
   *
   * <p>
   * A check process plugin can be be listed by its:
   * </p>
   * <ul>
   * <li>CheckProcessPlugin.getClass().getName(), or</li>
   * <li>CheckProcessPlugin.getClass().getSimpleName()</li>
   * </ul>
   *
   * @param checkProcessPlugin
   *          the check process plugin
   * @return true when the specified check process plugin is disabled
   */
  public boolean isCheckProcessPluginDisabled(CheckProcessPlugin checkProcessPlugin) {
    return isInEnabledDisabledSet(disabledCheckProcessPlugins, checkProcessPlugin);
  }
}
