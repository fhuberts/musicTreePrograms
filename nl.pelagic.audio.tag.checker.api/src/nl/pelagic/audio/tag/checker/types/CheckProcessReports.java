package nl.pelagic.audio.tag.checker.types;

import java.util.LinkedList;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Notices and/or error to report for a file
 */
@ProviderType
public class CheckProcessReports {
  /**
   * A list of notices
   */
  public List<String> notices = new LinkedList<>();

  /**
   * A list of errors
   */
  public List<String> errors = new LinkedList<>();

  /**
   * Clears the notices and errors
   */
  public void clear() {
    notices.clear();
    errors.clear();
  }

  /**
   * @return true when both notices and errors are empty
   */
  public boolean isEmpty() {
    return notices.isEmpty() //
        && errors.isEmpty();
  }
}