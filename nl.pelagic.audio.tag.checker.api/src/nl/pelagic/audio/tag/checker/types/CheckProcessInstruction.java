package nl.pelagic.audio.tag.checker.types;

/**
 * An instruction that is returned to the check process from a check process plugin.
 */
public enum CheckProcessInstruction {
  /** Continue the check process */
  CONTINUE,

  /** Restart the check process at the current directory being checked */
  RESTART_DIRECTORY;

  /**
   * Aggregate processing instructions.
   *
   * The last non-continue value will win.
   *
   * @param next
   *          the next processing instruction
   * @return the aggregated processing instruction
   */
  public CheckProcessInstruction aggregate(CheckProcessInstruction next) {
    if ((next == null) //
        || (CheckProcessInstruction.CONTINUE == next)) {

      return this;
    }

    return next;
  }
}