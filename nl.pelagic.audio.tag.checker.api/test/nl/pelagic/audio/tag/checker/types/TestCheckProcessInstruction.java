package nl.pelagic.audio.tag.checker.types;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "static-method"
})
public class TestCheckProcessInstruction {

  GenericTag tag;

  @Before
  public void setUp() {
    tag = new GenericTag();
  }

  @After
  public void tearDown() {
    tag = null;
  }

  @Test(timeout = 8000)
  public void testAggregateProcessingInstruction() {
    CheckProcessInstruction instr = CheckProcessInstruction.CONTINUE;

    instr = instr.aggregate(null);
    assertThat(instr, equalTo(CheckProcessInstruction.CONTINUE));
    instr = instr.aggregate(CheckProcessInstruction.CONTINUE);
    assertThat(instr, equalTo(CheckProcessInstruction.CONTINUE));
    instr = instr.aggregate(CheckProcessInstruction.RESTART_DIRECTORY);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
    instr = instr.aggregate(CheckProcessInstruction.RESTART_DIRECTORY);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
    instr = instr.aggregate(CheckProcessInstruction.CONTINUE);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
    instr = instr.aggregate(null);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
    instr = instr.aggregate(CheckProcessInstruction.RESTART_DIRECTORY);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));

    instr = CheckProcessInstruction.RESTART_DIRECTORY;

    instr = instr.aggregate(null);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
    instr = instr.aggregate(CheckProcessInstruction.CONTINUE);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
    instr = instr.aggregate(CheckProcessInstruction.RESTART_DIRECTORY);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
    instr = instr.aggregate(CheckProcessInstruction.RESTART_DIRECTORY);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
    instr = instr.aggregate(CheckProcessInstruction.CONTINUE);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
    instr = instr.aggregate(null);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
    instr = instr.aggregate(CheckProcessInstruction.RESTART_DIRECTORY);
    assertThat(instr, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));
  }
}