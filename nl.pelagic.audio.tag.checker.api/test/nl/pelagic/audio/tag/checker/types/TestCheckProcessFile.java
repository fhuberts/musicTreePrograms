package nl.pelagic.audio.tag.checker.types;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;

import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "static-method"
})
public class TestCheckProcessFile {

  @Test(timeout = 8000)
  public void testInitial() {
    CheckProcessFile file = new CheckProcessFile();

    assertThat(file.file, nullValue());
    assertThat(file.extension, nullValue());
    assertThat(file.af, nullValue());
    assertThat(file.tag, nullValue());
    assertThat(file.genericTag, nullValue());
  }
}