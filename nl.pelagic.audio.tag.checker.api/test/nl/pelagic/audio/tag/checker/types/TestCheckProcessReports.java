package nl.pelagic.audio.tag.checker.types;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "static-method",
    "nls"
})
public class TestCheckProcessReports {

  @Test(timeout = 8000)
  public void testInitial() {
    CheckProcessReports reports = new CheckProcessReports();

    assertThat(reports.notices, notNullValue());
    assertThat(reports.errors, notNullValue());
    assertThat(Integer.valueOf(reports.notices.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(reports.errors.size()), equalTo(Integer.valueOf(0)));

    assertThat(Boolean.valueOf(reports.isEmpty()), equalTo(Boolean.TRUE));
  }

  @Test(timeout = 8000)
  public void testClear() {
    CheckProcessReports reports = new CheckProcessReports();

    String str1n = "string 1 notice";
    String str2n = "string 1 notice";
    String str1e = "string 1 error";

    reports.notices.add(str1n);
    reports.notices.add(str2n);
    reports.errors.add(str1e);

    assertThat(reports.notices, notNullValue());
    assertThat(reports.errors, notNullValue());
    assertThat(Integer.valueOf(reports.notices.size()), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(reports.errors.size()), equalTo(Integer.valueOf(1)));
    assertThat(reports.notices.get(0), equalTo(str1n));
    assertThat(reports.notices.get(1), equalTo(str2n));
    assertThat(reports.errors.get(0), equalTo(str1e));

    assertThat(Boolean.valueOf(reports.isEmpty()), equalTo(Boolean.FALSE));

    reports.clear();

    assertThat(reports.notices, notNullValue());
    assertThat(reports.errors, notNullValue());
    assertThat(Integer.valueOf(reports.notices.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(reports.errors.size()), equalTo(Integer.valueOf(0)));

    assertThat(Boolean.valueOf(reports.isEmpty()), equalTo(Boolean.TRUE));
  }

  @Test(timeout = 8000)
  public void testIsEmpty() {
    CheckProcessReports reports = new CheckProcessReports();

    String str1n = "string 1 notice";
    String str2n = "string 1 notice";
    String str1e = "string 1 error";

    assertThat(Boolean.valueOf(reports.isEmpty()), equalTo(Boolean.TRUE));

    reports.notices.add(str1n);
    reports.notices.add(str2n);
    reports.errors.add(str1e);

    assertThat(Boolean.valueOf(reports.isEmpty()), equalTo(Boolean.FALSE));

    reports.notices.clear();

    assertThat(Boolean.valueOf(reports.isEmpty()), equalTo(Boolean.FALSE));

    reports.errors.clear();

    assertThat(Boolean.valueOf(reports.isEmpty()), equalTo(Boolean.TRUE));

    reports.notices.add(str1n);
    reports.notices.add(str2n);

    assertThat(Boolean.valueOf(reports.isEmpty()), equalTo(Boolean.FALSE));
  }
}
