package nl.pelagic.musicTree.flac2mp3.cli;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;

@Ignore
@SuppressWarnings({
    "nls",
    "javadoc"
})
public class MyBundle implements Bundle {

  @Override
  public int compareTo(Bundle arg0) {
    throw new IllegalStateException();
  }

  @Override
  public int getState() {
    throw new IllegalStateException();
  }

  @Override
  public void start(int options) throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void start() throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void stop(int options) throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void stop() throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void update(InputStream input) throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void update() throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void uninstall() throws BundleException {
    throw new IllegalStateException();
  }

  public Hashtable<String, String> headers = new Hashtable<>();

  @Override
  public Dictionary<String, String> getHeaders() {
    return headers;
  }

  public long id = 42;

  @Override
  public long getBundleId() {
    return id;
  }

  @Override
  public String getLocation() {
    throw new IllegalStateException();
  }

  @Override
  public ServiceReference<?>[] getRegisteredServices() {
    throw new IllegalStateException();
  }

  @Override
  public ServiceReference<?>[] getServicesInUse() {
    throw new IllegalStateException();
  }

  @Override
  public boolean hasPermission(Object permission) {
    throw new IllegalStateException();
  }

  @Override
  public URL getResource(String name) {
    throw new IllegalStateException();
  }

  @Override
  public Dictionary<String, String> getHeaders(String locale) {
    throw new IllegalStateException();
  }

  public String bsn = "";

  @Override
  public String getSymbolicName() {
    return bsn;
  }

  @Override
  public Class<?> loadClass(String name) throws ClassNotFoundException {
    throw new IllegalStateException();
  }

  @Override
  public Enumeration<URL> getResources(String name) throws IOException {
    throw new IllegalStateException();
  }

  @Override
  public Enumeration<String> getEntryPaths(String path) {
    throw new IllegalStateException();
  }

  @Override
  public URL getEntry(String path) {
    throw new IllegalStateException();
  }

  @Override
  public long getLastModified() {
    throw new IllegalStateException();
  }

  @Override
  public Enumeration<URL> findEntries(String path, String filePattern, boolean recurse) {
    throw new IllegalStateException();
  }

  @Override
  public BundleContext getBundleContext() {
    throw new IllegalStateException();
  }

  @Override
  public Map<X509Certificate, List<X509Certificate>> getSignerCertificates(int signersType) {
    throw new IllegalStateException();
  }

  public Version version = Version.emptyVersion;

  @Override
  public Version getVersion() {
    return version;
  }

  @Override
  public <A> A adapt(Class<A> type) {
    throw new IllegalStateException();
  }

  @Override
  public File getDataFile(String filename) {
    throw new IllegalStateException();
  }
}