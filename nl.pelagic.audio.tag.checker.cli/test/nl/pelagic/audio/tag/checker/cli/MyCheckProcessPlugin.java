package nl.pelagic.audio.tag.checker.cli;

import java.io.File;
import java.util.List;
import java.util.Map;

import nl.pelagic.audio.tag.checker.api.CheckProcessPlugin;
import nl.pelagic.audio.tag.checker.types.CheckProcessFile;
import nl.pelagic.audio.tag.checker.types.CheckProcessInstruction;
import nl.pelagic.audio.tag.checker.types.CheckProcessReports;

import org.junit.Ignore;

@Ignore
@SuppressWarnings("javadoc")
public class MyCheckProcessPlugin implements CheckProcessPlugin {

  boolean enabled = true;

  @Override
  public boolean isEnabledByDefault() {
    return enabled;
  }

  @Override
  public void directoryEntry(File directory, List<File> directories, List<File> regularFiles) {
    // nothing to do
  }

  @Override
  public CheckProcessInstruction directoryExit(File directory) {
    // nothing to do
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesEntry(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    // nothing to do
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesExtensions(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    // nothing to do
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesRead(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    // nothing to do
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesTagsRead(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    // nothing to do
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesTagsConverted(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    // nothing to do
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesExit(Map<File, CheckProcessFile> map, Map<File, CheckProcessReports> reports) {
    // nothing to do
    return CheckProcessInstruction.CONTINUE;
  }
}