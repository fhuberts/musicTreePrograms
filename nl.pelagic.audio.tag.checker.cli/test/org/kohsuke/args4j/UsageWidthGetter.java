package org.kohsuke.args4j;

import org.junit.Ignore;

@SuppressWarnings("javadoc")
@Ignore
public class UsageWidthGetter {
  public static int getUsageWidth(CmdLineParser parser) {
    return parser.getProperties().getUsageWidth();
  }
}
