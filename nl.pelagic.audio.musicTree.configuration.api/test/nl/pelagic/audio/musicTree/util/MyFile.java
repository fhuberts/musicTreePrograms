package nl.pelagic.audio.musicTree.util;

import java.io.File;
import java.io.IOException;

import org.junit.Ignore;

@Ignore
@SuppressWarnings({
    "nls",
    "javadoc"
})
public class MyFile extends File {

  public MyFile(File parent, String child) {
    super(parent, child);
  }

  private static final long serialVersionUID = 1L;

  public int getCanonicalPathThrowOnCount = Integer.MAX_VALUE;

  @Override
  public String getCanonicalPath() throws IOException {
    getCanonicalPathThrowOnCount--;
    if (getCanonicalPathThrowOnCount <= 0) {
      getCanonicalPathThrowOnCount = 1;
      throw new IOException("test");
    }

    return super.getCanonicalPath();
  }
}
