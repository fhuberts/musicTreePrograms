package nl.pelagic.audio.tag.checker.flatten.common;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import java.io.File;

import nl.pelagic.audio.tag.checker.types.CheckProcessFile;

import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "static-method",
    "boxing",
    "nls"
})
public class TestFileContainerComparator {
  @Test(timeout = 8000)
  public void testCompare() {
    FileContainerComparator comp = new FileContainerComparator();

    FileContainer c1 = new FileContainer();
    FileContainer c2 = new FileContainer();

    int r;

    r = comp.compare(null, null);
    assertThat(r, equalTo(1));

    r = comp.compare(c1, null);
    assertThat(r, equalTo(-1));

    r = comp.compare(null, c2);
    assertThat(r, equalTo(1));

    c1.discCount = 1;
    c2.discCount = 2;
    r            = comp.compare(c1, c2);
    assertThat(r, equalTo(-1));
    r = comp.compare(c2, c1);
    assertThat(r, equalTo(1));
    c1.discCount = 1;
    c2.discCount = 1;

    c1.discNumber = 1;
    c2.discNumber = 2;
    r             = comp.compare(c1, c2);
    assertThat(r, equalTo(-1));
    r = comp.compare(c2, c1);
    assertThat(r, equalTo(1));
    c1.discNumber = 1;
    c2.discNumber = 1;

    c1.trackCount = 1;
    c2.trackCount = 2;
    r             = comp.compare(c1, c2);
    assertThat(r, equalTo(-1));
    r = comp.compare(c2, c1);
    assertThat(r, equalTo(1));
    c1.trackCount = 1;
    c2.trackCount = 1;

    c1.trackNumber = 1;
    c2.trackNumber = 2;
    r              = comp.compare(c1, c2);
    assertThat(r, equalTo(-1));
    r = comp.compare(c2, c1);
    assertThat(r, equalTo(1));
    c1.trackNumber = 1;
    c2.trackNumber = 1;

    c1.checkProcessFile = null;
    c2.checkProcessFile = null;
    r                   = comp.compare(c1, c2);
    assertThat(r, equalTo(1));

    c1.checkProcessFile = new CheckProcessFile();
    c2.checkProcessFile = null;
    r                   = comp.compare(c1, c2);
    assertThat(r, equalTo(-1));

    c1.checkProcessFile = null;
    c2.checkProcessFile = new CheckProcessFile();
    r                   = comp.compare(c1, c2);
    assertThat(r, equalTo(1));

    c1.checkProcessFile = new CheckProcessFile();
    c2.checkProcessFile = new CheckProcessFile();

    c1.checkProcessFile.file = null;
    c2.checkProcessFile.file = null;
    r                        = comp.compare(c1, c2);
    assertThat(r, equalTo(1));

    c1.checkProcessFile.file = new File("a/b");
    c2.checkProcessFile.file = null;
    r                        = comp.compare(c1, c2);
    assertThat(r, equalTo(-1));

    c1.checkProcessFile.file = null;
    c2.checkProcessFile.file = new File("a/b");
    r                        = comp.compare(c1, c2);
    assertThat(r, equalTo(1));

    c1.checkProcessFile.file = new File("a/b");
    c2.checkProcessFile.file = new File("a/c");
    r                        = comp.compare(c1, c2);
    assertThat(r, equalTo(-1));
    r = comp.compare(c2, c1);
    assertThat(r, equalTo(1));
    r = comp.compare(c1, c1);
    assertThat(r, equalTo(0));
  }
}