package nl.pelagic.audio.tag.checker.flatten.flatten;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import nl.pelagic.audio.tag.checker.flatten.common.FileContainer;
import nl.pelagic.audio.tag.checker.flatten.common.i18n.Messages;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "nls",
    "boxing"
})
public class TestFlattenAlbums {

  FlattenAlbums impl;

  @Before
  public void setup() {
    impl = new FlattenAlbums();
  }

  @After
  public void tearDown() {
    impl = null;
  }

  @Test(timeout = 8000)
  public void testIsFlattenMode() {
    assertThat(impl.isFlattenMode(), equalTo(true));
  }

  @Test(timeout = 8000)
  public void testGetReportPrefix() {
    assertThat(impl.getReportPrefix(), equalTo(Messages.getString("Flatten.0") + ": "));
  }

  @Test(timeout = 8000)
  public void testShouldIgnoreFile() {
    FileContainer c = new FileContainer();

    c.discCount         = 1;
    c.hasFlattenComment = true;
    assertThat(impl.shouldIgnoreFile(c), equalTo(true));

    c.discCount         = 2;
    c.hasFlattenComment = true;
    assertThat(impl.shouldIgnoreFile(c), equalTo(true));

    c.discCount         = 2;
    c.hasFlattenComment = false;
    assertThat(impl.shouldIgnoreFile(c), equalTo(false));
  }

  @Test(timeout = 8000)
  public void testIsEnabledByDefault() {
    assertThat(impl.isEnabledByDefault(), equalTo(false));
  }
}