package nl.pelagic.audio.tag.checker.flatten.common;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "static-method",
    "boxing"
})
public class TestProcessableAlbum {

  @Test(timeout = 8000)
  public void testInitial() {
    ProcessableAlbum c = new ProcessableAlbum();
    assertThat(c, notNullValue());
    assertThat(c.name, nullValue());
    assertThat(c.tracks, notNullValue());
    assertThat(c.tracks.size(), equalTo(0));
    assertThat(c.discCountMax, equalTo(0));
    assertThat(c.fileNameLengthMax, equalTo(0));
    assertThat(c.trackDigits, equalTo(0));
  }
}