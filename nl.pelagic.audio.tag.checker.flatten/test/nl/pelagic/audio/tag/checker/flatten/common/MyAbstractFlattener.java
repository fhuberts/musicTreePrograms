package nl.pelagic.audio.tag.checker.flatten.common;

import org.junit.Ignore;

@SuppressWarnings({
    "javadoc",
    "nls"
})
@Ignore
public class MyAbstractFlattener extends AbstractFlattener {

  public boolean isFlattenMode = true;

  @Override
  protected boolean isFlattenMode() {
    return isFlattenMode;
  }

  public String reportPrefix = "TEST-REPORT-PREFIX: ";

  @Override
  protected String getReportPrefix() {
    return reportPrefix;
  }

  public int       ignoreIndex = 0;
  public boolean[] ignores     = {
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false
  };

  @Override
  protected boolean shouldIgnoreFile(FileContainer container) {
    return ignores[ignoreIndex++];
  }

  public boolean enabled = false;

  @Override
  public boolean isEnabledByDefault() {
    return enabled;
  }
}