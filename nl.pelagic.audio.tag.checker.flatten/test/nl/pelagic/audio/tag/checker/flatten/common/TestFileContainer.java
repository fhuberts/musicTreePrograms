package nl.pelagic.audio.tag.checker.flatten.common;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "static-method",
    "boxing"
})
public class TestFileContainer {
  @Test(timeout = 8000)
  public void testInitial() {
    FileContainer c = new FileContainer();
    assertThat(c, notNullValue());
    assertThat(c.checkProcessFile, nullValue());
    assertThat(c.trackTitle, nullValue());
    assertThat(c.comment, nullValue());
    assertThat(c.hasFlattenComment, equalTo(false));

    assertThat(c.trackNumber, equalTo(0));
    assertThat(c.trackCount, equalTo(0));
    assertThat(c.discNumber, equalTo(0));
    assertThat(c.discCount, equalTo(0));

    assertThat(c.trackNumberOriginal, equalTo(0));
    assertThat(c.trackCountOriginal, equalTo(0));
    assertThat(c.trackDigitsOriginal, equalTo(0));
    assertThat(c.discNumberOriginal, equalTo(0));
    assertThat(c.discCountOriginal, equalTo(0));
    assertThat(c.discDigitsOriginal, equalTo(0));
  }
}