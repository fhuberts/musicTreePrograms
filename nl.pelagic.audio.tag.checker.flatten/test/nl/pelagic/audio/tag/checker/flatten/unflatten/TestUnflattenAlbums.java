package nl.pelagic.audio.tag.checker.flatten.unflatten;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import nl.pelagic.audio.tag.checker.flatten.common.FileContainer;
import nl.pelagic.audio.tag.checker.flatten.common.i18n.Messages;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "nls",
    "boxing"
})
public class TestUnflattenAlbums {

  UnflattenAlbums impl;

  @Before
  public void setup() {
    impl = new UnflattenAlbums();
  }

  @After
  public void tearDown() {
    impl = null;
  }

  @Test(timeout = 8000)
  public void testIsFlattenMode() {
    assertThat(impl.isFlattenMode(), equalTo(false));
  }

  @Test(timeout = 8000)
  public void testGetReportPrefix() {
    assertThat(impl.getReportPrefix(), equalTo(Messages.getString("Flatten.1") + ": "));
  }

  @Test(timeout = 8000)
  public void testShouldIgnoreFile() {
    FileContainer c = new FileContainer();

    c.hasFlattenComment = true;
    assertThat(impl.shouldIgnoreFile(c), equalTo(false));

    c.hasFlattenComment = false;
    assertThat(impl.shouldIgnoreFile(c), equalTo(true));
  }

  @Test(timeout = 8000)
  public void testIsEnabledByDefault() {
    assertThat(impl.isEnabledByDefault(), equalTo(false));
  }
}