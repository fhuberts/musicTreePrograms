package nl.pelagic.audio.tag.checker.flatten.common;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import nl.pelagic.audio.tag.checker.flatten.common.i18n.Messages;
import nl.pelagic.audio.tag.checker.types.CheckProcessFile;
import nl.pelagic.audio.tag.checker.types.CheckProcessInstruction;
import nl.pelagic.audio.tag.checker.types.CheckProcessReports;
import nl.pelagic.util.file.ExtensionUtils;
import nl.pelagic.util.file.FileUtils;

import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "nls",
    "boxing"
})
public class TestAbstractFlattener {
  static final File   testResourcesDir     = new File("testresources");
  static final String laserFlac            = "laser.flac";
  static final String laserMp3v1           = "laser-v1.mp3";
  static final String laserMp3v22          = "laser-v22.mp3";
  static final String laserMp3v23          = "laser-v23.mp3";
  static final String laserMp3v24          = "laser-v24.mp3";
  static final String laserMp3InvalidTrack = "laser-invalid-track.mp3";
  static final String laserNoTagMp3        = "laser_no_tag.mp3";
  static final String TESTALBUM            = "Testdata";

  MyAbstractFlattener impl;

  @Before
  public void setup() {
    impl = new MyAbstractFlattener();
  }

  @After
  public void tearDown() {
    impl = null;
  }

  @Test(timeout = 8000)
  public void testInitial() {
    assertThat(impl.commentPattern, notNullValue());
  }

  static File prepareFile(boolean flac, String filename, FileContainer c, boolean set) throws Throwable {
    c.checkProcessFile = new CheckProcessFile();
    File src;
    File dst;
    if (flac) {
      src                          = new File(testResourcesDir, filename);
      dst                          = new File("temp-file.flac");
      c.checkProcessFile.extension = ".flac";
    } else {
      src                          = new File(testResourcesDir, filename);
      dst                          = new File("temp-file.mp3");
      c.checkProcessFile.extension = ".mp3";
    }

    Path srcPath = src.toPath();
    Path dstPath = srcPath.resolveSibling(dst.getName());
    dst                     = dstPath.toFile();
    c.checkProcessFile.file = dst;

    Files.copy(srcPath, dstPath, StandardCopyOption.REPLACE_EXISTING);

    try {
      c.checkProcessFile.af  = AudioFileIO.read(dst);
      c.checkProcessFile.tag = c.checkProcessFile.af.getTag();

      if (set) {
        Tag tag = c.checkProcessFile.tag;

        tag.setField(FieldKey.ALBUM, TESTALBUM);
        if (c.trackTitle == null) {
          tag.deleteField(FieldKey.TITLE);
        } else {
          tag.setField(FieldKey.TITLE, c.trackTitle);
        }
        tag.setField(FieldKey.TRACK, Integer.toString(c.trackNumber));
        tag.setField(FieldKey.TRACK_TOTAL, Integer.toString(c.trackCount));
        tag.setField(FieldKey.DISC_NO, Integer.toString(c.discNumber));
        tag.setField(FieldKey.DISC_TOTAL, Integer.toString(c.discCount));
        if (c.hasFlattenComment) {
          String s = (c.comment.isEmpty())
              ? String.format(AbstractFlattener.COMMENT_PREFIX_FMT, c.trackNumberOriginal, c.trackCountOriginal,
                  c.discNumberOriginal, c.discCountOriginal)
              : c.comment;
          tag.setField(FieldKey.COMMENT, s);
        } else {
          tag.deleteField(FieldKey.COMMENT);
        }
        c.checkProcessFile.af.commit();
      }

      return dst;
    }
    catch (Throwable e) {
      dst.delete();
      throw e;
    }
  }

  static void readFile(CheckProcessFile c)
      throws CannotReadException, IOException, TagException, ReadOnlyFileException, InvalidAudioFrameException {
    c.extension = ExtensionUtils.split(c.file.getName(), false)[1].toLowerCase(Locale.getDefault());
    c.af        = AudioFileIO.read(c.file);
    c.tag       = c.af.getTag();
  }

  static void checkAlbum(ProcessableAlbum album, FileContainer c) {
    assertThat(album, notNullValue());
    assertThat(c, notNullValue());

    assertThat(album.name, equalTo(TESTALBUM));
    assertThat(album.tracks.size(), equalTo(1));
    assertThat(album.discCountMax, equalTo(c.discCount));
    assertThat(album.fileNameLengthMax, equalTo(c.checkProcessFile.file.getName().length()));
    FileContainer track = album.tracks.get(0);

    assertThat(track.trackTitle, equalTo(c.trackTitle));
    assertThat(track.trackNumber, equalTo(c.trackNumber));
    assertThat(track.trackCount, equalTo(c.trackCount));
    assertThat(track.discNumber, equalTo(c.discNumber));
    assertThat(track.discCount, equalTo(c.discCount));
    assertThat(track.hasFlattenComment, equalTo(c.hasFlattenComment));
    if (!c.hasFlattenComment) {
      assertThat(track.comment, equalTo(c.comment));
      assertThat(track.trackNumberOriginal, equalTo(0));
      assertThat(track.trackCountOriginal, equalTo(0));
      assertThat(track.trackDigitsOriginal, equalTo(0));
      assertThat(track.discNumberOriginal, equalTo(0));
      assertThat(track.discCountOriginal, equalTo(0));
      assertThat(track.discDigitsOriginal, equalTo(0));
    } else {
      String s = String.format(AbstractFlattener.COMMENT_PREFIX_FMT, c.trackNumberOriginal, c.trackCountOriginal,
          c.discNumberOriginal, c.discCountOriginal);
      assertThat(track.comment, equalTo(s));
      assertThat(track.trackNumberOriginal, equalTo(c.trackNumberOriginal));
      assertThat(track.trackCountOriginal, equalTo(c.trackCountOriginal));
      assertThat(track.trackDigitsOriginal, equalTo(Math.max(2, (int) Math.log10(c.trackCountOriginal) + 1)));
      assertThat(track.discNumberOriginal, equalTo(c.discNumberOriginal));
      assertThat(track.discCountOriginal, equalTo(c.discCountOriginal));
      assertThat(track.discDigitsOriginal, equalTo(Math.max(2, (int) Math.log10(c.discCountOriginal) + 1)));
    }
  }

  @Test(timeout = 8000)
  public void testFileToProcessableAlbumInvalidTitleAndTrack() throws Throwable {
    Map<String, ProcessableAlbum> processableAlbums = new TreeMap<>();
    CheckProcessReports           fileReports       = new CheckProcessReports();

    FileContainer c;

    // no track title

    c                   = new FileContainer();
    c.trackTitle        = null;
    c.trackNumber       = 12;
    c.trackCount        = 34;
    c.discNumber        = 2;
    c.discCount         = 3;
    c.comment           = "";
    c.hasFlattenComment = false;

    boolean r;
    File    f = null;

    try {
      f = prepareFile(false, laserMp3v24, c, true);

      r = impl.fileToProcessableAlbum(c.checkProcessFile, processableAlbums, fileReports);

      assertThat(r, equalTo(true));
      assertThat(processableAlbums.size(), equalTo(0));
      assertThat(fileReports.isEmpty(), equalTo(false));
      assertThat(fileReports.notices.size(), equalTo(0));
      assertThat(fileReports.errors.size(), equalTo(1));
      String error = fileReports.errors.get(0);
      assertThat(error, equalTo(impl.getReportPrefix() + Messages.getString("Flatten.2")));
    }
    finally {
      if (f != null) {
        f.delete();
        f = null;
      }
    }

    processableAlbums = new TreeMap<>();
    fileReports       = new CheckProcessReports();

    // invalid number

    c                   = new FileContainer();
    c.trackTitle        = laserMp3InvalidTrack;
    c.trackNumber       = 12;
    c.trackCount        = 34;
    c.discNumber        = 2;
    c.discCount         = 3;
    c.comment           = "";
    c.hasFlattenComment = false;

    try {
      f = prepareFile(false, laserMp3InvalidTrack, c, false);

      r = impl.fileToProcessableAlbum(c.checkProcessFile, processableAlbums, fileReports);

      assertThat(r, equalTo(true));
      assertThat(processableAlbums.size(), equalTo(0));
      assertThat(fileReports.isEmpty(), equalTo(false));
      assertThat(fileReports.notices.size(), equalTo(0));
      assertThat(fileReports.errors.size(), equalTo(1));
      String error = fileReports.errors.get(0);
      assertThat(error.startsWith(impl.getReportPrefix() + Messages.getString("Flatten.3") + ": "), equalTo(true));
    }
    finally {
      if (f != null) {
        f.delete();
        f = null;
      }
    }
  }

  @Test(timeout = 8000)
  public void testFileToProcessableAlbum() throws Throwable {
    Map<String, ProcessableAlbum> processableAlbums = new TreeMap<>();
    CheckProcessReports           fileReports       = new CheckProcessReports();

    FileContainer c;

    // normal

    c                   = new FileContainer();
    c.trackTitle        = laserMp3v24;
    c.trackNumber       = 12;
    c.trackCount        = 34;
    c.discNumber        = 2;
    c.discCount         = 3;
    c.comment           = "";
    c.hasFlattenComment = false;

    boolean r;
    File    f = null;

    try {
      f = prepareFile(false, laserMp3v24, c, true);

      r = impl.fileToProcessableAlbum(c.checkProcessFile, processableAlbums, fileReports);

      assertThat(r, equalTo(false));
      assertThat(processableAlbums.size(), equalTo(1));
      assertThat(fileReports.isEmpty(), equalTo(true));
      ProcessableAlbum album = processableAlbums.get(TESTALBUM);
      assertThat(album, notNullValue());
      checkAlbum(album, c);
    }
    finally {
      if (f != null) {
        f.delete();
        f = null;
      }
    }

    processableAlbums = new TreeMap<>();
    fileReports       = new CheckProcessReports();

    // normal,ignoring file

    c                   = new FileContainer();
    c.trackTitle        = laserMp3v24;
    c.trackNumber       = 12;
    c.trackCount        = 34;
    c.discNumber        = 2;
    c.discCount         = 3;
    c.comment           = "";
    c.hasFlattenComment = false;

    try {
      f = prepareFile(false, laserMp3v24, c, true);

      impl.ignoreIndex = 0;
      impl.ignores[0]  = true;
      r                = impl.fileToProcessableAlbum(c.checkProcessFile, processableAlbums, fileReports);

      assertThat(r, equalTo(false));
      assertThat(processableAlbums.size(), equalTo(0));
      assertThat(fileReports.isEmpty(), equalTo(true));
    }
    finally {
      if (f != null) {
        f.delete();
        f = null;
      }
    }

    impl.ignoreIndex  = 0;
    impl.ignores[0]   = false;
    processableAlbums = new TreeMap<>();
    fileReports       = new CheckProcessReports();

    // normal, already present in processableAlbums

    c                   = new FileContainer();
    c.trackTitle        = laserMp3v24;
    c.trackNumber       = 12;
    c.trackCount        = 34;
    c.discNumber        = 2;
    c.discCount         = 3;
    c.comment           = "";
    c.hasFlattenComment = false;

    try {
      f = prepareFile(false, laserMp3v24, c, true);

      ProcessableAlbum album = new ProcessableAlbum();
      album.name = TESTALBUM;
      processableAlbums.put(TESTALBUM, album);
      r = impl.fileToProcessableAlbum(c.checkProcessFile, processableAlbums, fileReports);

      assertThat(r, equalTo(false));
      assertThat(processableAlbums.size(), equalTo(1));
      assertThat(fileReports.isEmpty(), equalTo(true));
      album = processableAlbums.get(TESTALBUM);
      assertThat(album, notNullValue());
      checkAlbum(album, c);
    }
    finally {
      if (f != null) {
        f.delete();
        f = null;
      }
    }

    processableAlbums = new TreeMap<>();
    fileReports       = new CheckProcessReports();

    // with comment

    c                     = new FileContainer();
    c.trackTitle          = laserMp3v24;
    c.trackNumber         = 12;
    c.trackCount          = 34;
    c.discNumber          = 2;
    c.discCount           = 3;
    c.comment             = "";
    c.hasFlattenComment   = true;
    c.trackNumberOriginal = 5;
    c.trackCountOriginal  = 7;
    c.discNumberOriginal  = 32;
    c.discCountOriginal   = 135;

    try {
      f = prepareFile(false, laserMp3v24, c, true);

      r = impl.fileToProcessableAlbum(c.checkProcessFile, processableAlbums, fileReports);

      assertThat(r, equalTo(false));
      assertThat(processableAlbums.size(), equalTo(1));
      assertThat(fileReports.isEmpty(), equalTo(true));
      ProcessableAlbum album = processableAlbums.get(TESTALBUM);
      assertThat(album, notNullValue());
      checkAlbum(album, c);
    }
    finally {
      if (f != null) {
        f.delete();
        f = null;
      }
    }

    processableAlbums = new TreeMap<>();
    fileReports       = new CheckProcessReports();

    // with comment with invalid number

    c                     = new FileContainer();
    c.trackTitle          = laserMp3v24;
    c.trackNumber         = 12;
    c.trackCount          = 34;
    c.discNumber          = 2;
    c.discCount           = 3;
    c.comment             = AbstractFlattener.COMMENT_PREFIX + "1:2:3:99999999999999999999999999999999999999999999";
    c.hasFlattenComment   = true;
    c.trackNumberOriginal = 5;
    c.trackCountOriginal  = 7;
    c.discNumberOriginal  = 32;
    c.discCountOriginal   = 135;

    try {
      f = prepareFile(false, laserMp3v24, c, true);

      r = impl.fileToProcessableAlbum(c.checkProcessFile, processableAlbums, fileReports);

      assertThat(r, equalTo(true));
      assertThat(processableAlbums.size(), equalTo(0));
      assertThat(fileReports.isEmpty(), equalTo(false));
      assertThat(fileReports.notices.size(), equalTo(0));
      assertThat(fileReports.errors.size(), equalTo(1));
      String error = fileReports.errors.get(0);
      assertThat(error.startsWith(impl.getReportPrefix() + Messages.getString("Flatten.6") + ": "), equalTo(true));
    }
    finally {
      if (f != null) {
        f.delete();
        f = null;
      }
    }
  }

  @Test(timeout = 8000)
  public void testFilesToProcessableAlbumsNormal()
      throws CannotReadException, IOException, TagException, ReadOnlyFileException, InvalidAudioFrameException {
    boolean                        r;
    Map<File, CheckProcessFile>    map               = new LinkedHashMap<>();
    Map<String, ProcessableAlbum>  processableAlbums = new LinkedHashMap<>();
    Map<File, CheckProcessReports> reports           = new LinkedHashMap<>();

    // normal

    CheckProcessFile cpf0 = new CheckProcessFile();
    CheckProcessFile cpf1 = new CheckProcessFile();
    CheckProcessFile cpf2 = new CheckProcessFile();
    CheckProcessFile cpf3 = new CheckProcessFile();
    CheckProcessFile cpf4 = new CheckProcessFile();
    CheckProcessFile cpf5 = new CheckProcessFile();
    cpf0.file = new File(testResourcesDir, laserMp3v1);
    cpf1.file = new File(testResourcesDir, laserMp3v24);
    cpf2.file = new File(testResourcesDir, laserFlac);
    cpf3.file = new File(testResourcesDir, laserMp3v22);
    cpf4.file = new File(testResourcesDir, laserMp3v23);
    cpf5.file = new File(testResourcesDir, laserMp3InvalidTrack);
    map.put(cpf0.file, cpf0);
    map.put(cpf1.file, cpf1);
    map.put(cpf2.file, cpf2);
    map.put(cpf3.file, cpf3);
    map.put(cpf4.file, cpf4);
    map.put(cpf5.file, cpf5);

    readFile(cpf0);
    readFile(cpf1);
    readFile(cpf2);
    readFile(cpf3);
    readFile(cpf4);
    readFile(cpf5);

    r = impl.filesToProcessableAlbums(map, processableAlbums, reports);

    assertThat(r, equalTo(false));
    assertThat(processableAlbums.size(), equalTo(1));
    assertThat(reports.isEmpty(), equalTo(false));
    ProcessableAlbum album = processableAlbums.get(TESTALBUM);
    assertThat(album, notNullValue());

    assertThat(album.name, equalTo(TESTALBUM));
    assertThat(album.tracks.size(), equalTo(4));
    assertThat(album.discCountMax, equalTo(2));
    assertThat(album.fileNameLengthMax, equalTo(13));
    assertThat(album.trackDigits, equalTo(2));

    FileContainer track = album.tracks.get(0);
    assertThat(track.checkProcessFile.file, equalTo(cpf2.file));
    track = album.tracks.get(1);
    assertThat(track.checkProcessFile.file, equalTo(cpf3.file));
    track = album.tracks.get(2);
    assertThat(track.checkProcessFile.file, equalTo(cpf4.file));
    track = album.tracks.get(3);
    assertThat(track.checkProcessFile.file, equalTo(cpf1.file));

    CheckProcessReports reps = reports.get(cpf0.file);
    assertThat(reps, notNullValue());
    assertThat(reps.notices.isEmpty(), equalTo(true));
    assertThat(reps.errors.isEmpty(), equalTo(false));
    assertThat(reps.errors.get(0), equalTo(impl.getReportPrefix() + Messages.getString("Flatten.4")));

    // reset

    map               = new LinkedHashMap<>();
    processableAlbums = new LinkedHashMap<>();
    reports           = new LinkedHashMap<>();

    // normal

    cpf1      = new CheckProcessFile();
    cpf2      = new CheckProcessFile();
    cpf3      = new CheckProcessFile();
    cpf4      = new CheckProcessFile();
    cpf1.file = new File(testResourcesDir, laserMp3v24);
    cpf2.file = new File(testResourcesDir, laserFlac);
    cpf3.file = new File(testResourcesDir, laserMp3v22);
    cpf4.file = new File(testResourcesDir, laserMp3v23);
    map.put(cpf1.file, cpf1);
    map.put(cpf2.file, cpf2);
    map.put(cpf3.file, cpf3);
    map.put(cpf4.file, cpf4);

    readFile(cpf1);
    readFile(cpf2);
    readFile(cpf3);
    readFile(cpf4);

    r = impl.filesToProcessableAlbums(map, processableAlbums, reports);

    assertThat(r, equalTo(true));
    assertThat(processableAlbums.size(), equalTo(1));
    assertThat(reports.isEmpty(), equalTo(true));
    album = processableAlbums.get(TESTALBUM);
    assertThat(album, notNullValue());

    assertThat(album.name, equalTo(TESTALBUM));
    assertThat(album.tracks.size(), equalTo(4));
    assertThat(album.discCountMax, equalTo(2));
    assertThat(album.fileNameLengthMax, equalTo(13));
    assertThat(album.trackDigits, equalTo(2));

    track = album.tracks.get(0);
    assertThat(track.checkProcessFile.file, equalTo(cpf2.file));
    track = album.tracks.get(1);
    assertThat(track.checkProcessFile.file, equalTo(cpf3.file));
    track = album.tracks.get(2);
    assertThat(track.checkProcessFile.file, equalTo(cpf4.file));
    track = album.tracks.get(3);
    assertThat(track.checkProcessFile.file, equalTo(cpf1.file));
  }

  @Test(timeout = 8000)
  public void testFilesToProcessableAlbumsInvalidTag()
      throws CannotReadException, IOException, TagException, ReadOnlyFileException, InvalidAudioFrameException {
    String                         dummyAlbum        = "dummy empty album";
    boolean                        r;
    Map<File, CheckProcessFile>    map               = new LinkedHashMap<>();
    Map<String, ProcessableAlbum>  processableAlbums = new LinkedHashMap<>();
    Map<File, CheckProcessReports> reports           = new LinkedHashMap<>();

    // v1 tag, and an empty dummy album

    ProcessableAlbum emptyAlbum = new ProcessableAlbum();
    processableAlbums.put(dummyAlbum, emptyAlbum);

    CheckProcessFile cpf1 = new CheckProcessFile();
    cpf1.file = new File(testResourcesDir, laserMp3v1);
    reports.put(cpf1.file, new CheckProcessReports());
    map.put(cpf1.file, cpf1);

    readFile(cpf1);

    r = impl.filesToProcessableAlbums(map, processableAlbums, reports);

    processableAlbums.remove(dummyAlbum);

    assertThat(r, equalTo(false));
    assertThat(processableAlbums.size(), equalTo(0));
    assertThat(reports.isEmpty(), equalTo(false));
    CheckProcessReports reps = reports.get(cpf1.file);
    assertThat(reps, notNullValue());
    assertThat(reps.notices.isEmpty(), equalTo(true));
    assertThat(reps.errors.isEmpty(), equalTo(false));
    assertThat(reps.errors.get(0), equalTo(impl.getReportPrefix() + Messages.getString("Flatten.4")));
  }

  @Test(timeout = 8000)
  public void testUpdateTagFlatten() throws Throwable {
    ProcessableAlbum processableAlbum = new ProcessableAlbum();
    FileContainer    track            = new FileContainer();
    int              trackNumber      = 42;

    // flatten

    processableAlbum.tracks.add(track);
    processableAlbum.tracks.add(track);
    processableAlbum.tracks.add(track);
    track.trackTitle        = "track title 1";
    track.trackNumber       = 12;
    track.trackCount        = 34;
    track.discNumber        = 56;
    track.discCount         = 78;
    track.comment           = "comment 1";
    track.hasFlattenComment = false;

    File f = null;

    try {
      f = prepareFile(false, laserMp3v24, track, true);

      processableAlbum.trackDigits = 4;
      track.trackTitle             = "track title 2";
      track.trackNumber            = 13;
      track.trackCount             = 35;
      track.discNumber             = 57;
      track.discCount              = 79;
      track.comment                = "comment 2";
      track.hasFlattenComment      = false;

      impl.updateTag(processableAlbum, track, trackNumber);

      CheckProcessFile cpf = new CheckProcessFile();
      cpf.file = f;
      readFile(cpf);

      assertThat(cpf.tag.getFirst(FieldKey.COMMENT), equalTo(String.format(AbstractFlattener.COMMENT_PREFIX_FMT,
          track.trackNumber, track.trackCount, track.discNumber, track.discCount)));
      assertThat(cpf.tag.getFirst(FieldKey.TRACK), equalTo("0042"));
      assertThat(cpf.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("3"));
      assertThat(cpf.tag.getFirst(FieldKey.DISC_NO), equalTo("1"));
      assertThat(cpf.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("1"));
    }
    finally {
      if (f != null) {
        f.delete();
        f = null;
      }
    }
  }

  @Test(timeout = 8000)
  public void testUpdateTagUnflatten() throws Throwable {
    ProcessableAlbum processableAlbum = new ProcessableAlbum();
    FileContainer    track            = new FileContainer();
    int              trackNumber      = 42;

    impl.isFlattenMode = false;

    // unflatten

    processableAlbum.tracks.add(track);
    processableAlbum.tracks.add(track);
    track.trackTitle          = "track title 1";
    track.trackNumber         = 12;
    track.trackCount          = 34;
    track.discNumber          = 56;
    track.discCount           = 78;
    track.comment             = "comment 1";
    track.hasFlattenComment   = false;
    track.trackNumberOriginal = 21;
    track.trackCountOriginal  = 43;
    track.discNumberOriginal  = 65;
    track.discCountOriginal   = 87;

    File f = null;

    try {
      f = prepareFile(false, laserMp3v24, track, true);

      processableAlbum.trackDigits = 4;
      track.trackDigitsOriginal    = 3;
      track.trackTitle             = "track title 2";
      track.trackNumber            = 13;
      track.trackCount             = 35;
      track.discNumber             = 57;
      track.discCount              = 79;
      track.comment                = "comment 2";
      track.hasFlattenComment      = false;
      track.trackNumberOriginal    = 22;
      track.trackCountOriginal     = 44;
      track.discNumberOriginal     = 66;
      track.discCountOriginal      = 88;

      impl.updateTag(processableAlbum, track, trackNumber);

      CheckProcessFile cpf = new CheckProcessFile();
      cpf.file = f;
      readFile(cpf);

      assertThat(cpf.tag.getFirst(FieldKey.COMMENT), equalTo(""));
      assertThat(cpf.tag.getFirst(FieldKey.TRACK), equalTo("022"));
      assertThat(cpf.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("44"));
      assertThat(cpf.tag.getFirst(FieldKey.DISC_NO), equalTo("66"));
      assertThat(cpf.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("88"));
    }
    finally {
      if (f != null) {
        f.delete();
        f = null;
      }
    }
  }

  @Test(timeout = 8000)
  public void testRenameFile() throws IOException {
    Path src = Files.createTempFile(this.getClass().getSimpleName(), "testRenameFile-src");
    Path dst = Files.createTempFile(this.getClass().getSimpleName(), "testRenameFile-dst");
    try {
      Files.delete(dst);
      assertThat(src.toFile().exists(), equalTo(true));
      assertThat(dst.toFile().exists(), equalTo(false));

      AbstractFlattener.renameFile(src.toFile(), dst.toFile().getName());

      assertThat(src.toFile().exists(), equalTo(false));
      assertThat(dst.toFile().exists(), equalTo(true));
    }
    finally {
      if (src.toFile().exists()) {
        src.toFile().delete();
      }
      if (dst.toFile().exists()) {
        dst.toFile().delete();
      }
    }
  }

  @Test(timeout = 8000)
  public void testReportProcessFile() {
    Map<File, CheckProcessReports> reports           = new HashMap<>();
    FileContainer                  track             = new FileContainer();
    String                         fileNameNew       = "fileNameNew";
    int                            fileNameLengthMax = 42;

    File f = new File(testResourcesDir, laserMp3v23);
    track.checkProcessFile      = new CheckProcessFile();
    track.checkProcessFile.file = f;

    impl.reportProcessFile(reports, track, fileNameNew, fileNameLengthMax);
    impl.reportProcessFile(reports, track, fileNameNew, fileNameLengthMax);

    assertThat(reports.size(), equalTo(1));
    CheckProcessReports reps = reports.get(testResourcesDir);
    assertThat(reps, notNullValue());
    assertThat(reps.errors.size(), equalTo(0));
    assertThat(reps.notices.size(), equalTo(3));
    String s1 = reps.notices.get(0);
    String s2 = reps.notices.get(1);
    String s3 = reps.notices.get(2);
    assertThat(s1, equalTo(impl.getReportPrefix()));
    assertThat(s2, equalTo(String.format("  %-" + Integer.toString(fileNameLengthMax) + "s --> %s",
        track.checkProcessFile.file.getName(), fileNameNew)));
    assertThat(s3, equalTo(s2));
  }

  @Test(timeout = 8000)
  public void testDirectoryEntry() {
    impl.directoryEntry(null, null, null);
  }

  @Test(timeout = 8000)
  public void testDirectoryExit() {
    assertThat(impl.directoryExit(null), equalTo(CheckProcessInstruction.CONTINUE));
  }

  @Test(timeout = 8000)
  public void testFilesEntry() {
    assertThat(impl.filesEntry(null, null, null), equalTo(CheckProcessInstruction.CONTINUE));
  }

  @Test(timeout = 8000)
  public void testFilesExtensions() {
    assertThat(impl.filesExtensions(null, null, null), equalTo(CheckProcessInstruction.CONTINUE));
  }

  @Test(timeout = 8000)
  public void testFilesRead() {
    assertThat(impl.filesRead(null, null, null), equalTo(CheckProcessInstruction.CONTINUE));
  }

  @Test(timeout = 8000)
  public void testFilesTagsRead() throws FileAlreadyExistsException, FileNotFoundException, IOException,
      CannotReadException, TagException, ReadOnlyFileException, InvalidAudioFrameException {
    Map<File, CheckProcessFile>    map     = new HashMap<>();
    List<File>                     errors  = new LinkedList<>();
    Map<File, CheckProcessReports> reports = new HashMap<>();

    File srcDir = new File(testResourcesDir, "subdir-multi");
    File dstDir = new File(testResourcesDir, "subdir-multi-temp");

    CheckProcessInstruction r;

    try {
      FileUtils.delete(dstDir);
      FileUtils.copy(srcDir, dstDir);
      AbstractFlattener.renameFile(new File(dstDir, "0127 - Laser mp3 1.mp3"), "Laser mp3 1.mp3");

      File[] files = dstDir.listFiles();
      for (File f : files) {
        CheckProcessFile value = new CheckProcessFile();
        value.file = f;
        readFile(value);
        map.put(f, value);
      }

      r = impl.filesTagsRead(map, errors, reports);

      assertThat(r, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));

      files = dstDir.listFiles();
      assertThat(files.length, equalTo(3));
      List<File> fls = Arrays.asList(files);
      File       fl1 = new File(dstDir, "01 - Laser mp3 1.mp3");
      File       fl2 = new File(dstDir, "02 - Laser mp3 2.mp3");
      File       fl3 = new File(dstDir, "03 - Laser mp3 3.mp3");
      assertThat(fls.contains(fl1), equalTo(true));
      assertThat(fls.contains(fl2), equalTo(true));
      assertThat(fls.contains(fl3), equalTo(true));

      CheckProcessFile c1 = new CheckProcessFile();
      CheckProcessFile c2 = new CheckProcessFile();
      CheckProcessFile c3 = new CheckProcessFile();
      c1.file = fl1;
      c2.file = fl2;
      c3.file = fl3;
      readFile(c1);
      readFile(c2);
      readFile(c3);

      // check file 1
      assertThat(c1.tag.getFirst(FieldKey.COMMENT),
          equalTo(String.format(AbstractFlattener.COMMENT_PREFIX_FMT, 27, 35, 1, 3)));
      assertThat(c1.tag.getFirst(FieldKey.TRACK), equalTo("01"));
      assertThat(c1.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("3"));
      assertThat(c1.tag.getFirst(FieldKey.DISC_NO), equalTo("1"));
      assertThat(c1.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("1"));

      // check file 2
      assertThat(c2.tag.getFirst(FieldKey.COMMENT),
          equalTo(String.format(AbstractFlattener.COMMENT_PREFIX_FMT, 7, 12, 2, 3)));
      assertThat(c2.tag.getFirst(FieldKey.TRACK), equalTo("02"));
      assertThat(c2.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("3"));
      assertThat(c2.tag.getFirst(FieldKey.DISC_NO), equalTo("1"));
      assertThat(c2.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("1"));

      // check file 3
      assertThat(c3.tag.getFirst(FieldKey.COMMENT),
          equalTo(String.format(AbstractFlattener.COMMENT_PREFIX_FMT, 14, 19, 3, 3)));
      assertThat(c3.tag.getFirst(FieldKey.TRACK), equalTo("03"));
      assertThat(c3.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("3"));
      assertThat(c3.tag.getFirst(FieldKey.DISC_NO), equalTo("1"));
      assertThat(c3.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("1"));

      assertThat(errors.size(), equalTo(0));
      assertThat(reports.size(), equalTo(1));
      CheckProcessReports reps = reports.get(dstDir);
      assertThat(reps, notNullValue());
      assertThat(reps.errors.size(), equalTo(0));
      assertThat(reps.notices.size(), equalTo(4));
      assertThat(reps.notices.get(0), equalTo(impl.getReportPrefix()));
      assertThat(reps.notices.get(1), equalTo("  Laser mp3 1.mp3        --> " + fl1.getName()));
      assertThat(reps.notices.get(2), equalTo("  0207 - Laser mp3 2.mp3 --> " + fl2.getName()));
      assertThat(reps.notices.get(3), equalTo("  0314 - Laser mp3 3.mp3 --> " + fl3.getName()));

      // reset

      map.clear();
      errors.clear();
      reports.clear();

      // flatten again

      impl.ignoreIndex = 0;
      impl.ignores[0]  = true;
      impl.ignores[1]  = true;
      impl.ignores[2]  = true;

      File[] files2 = dstDir.listFiles();
      for (File f : files2) {
        CheckProcessFile value = new CheckProcessFile();
        value.file = f;
        readFile(value);
        map.put(f, value);
      }

      r = impl.filesTagsRead(map, errors, reports);

      assertThat(r, equalTo(CheckProcessInstruction.CONTINUE));
      assertThat(errors.size(), equalTo(0));
      assertThat(reports.size(), equalTo(0));

      // reset

      map.clear();
      errors.clear();
      reports.clear();

      impl.ignoreIndex = 0;
      impl.ignores[0]  = false;
      impl.ignores[1]  = false;
      impl.ignores[2]  = false;

      // unflatten

      impl.isFlattenMode = false;

      files = dstDir.listFiles();
      for (File f : files) {
        CheckProcessFile value = new CheckProcessFile();
        value.file = f;
        readFile(value);
        map.put(f, value);
      }

      r = impl.filesTagsRead(map, errors, reports);

      assertThat(r, equalTo(CheckProcessInstruction.RESTART_DIRECTORY));

      files = dstDir.listFiles();
      assertThat(files.length, equalTo(3));
      fls = Arrays.asList(files);
      fl1 = new File(dstDir, "0127 - Laser mp3 1.mp3");
      fl2 = new File(dstDir, "0207 - Laser mp3 2.mp3");
      fl3 = new File(dstDir, "0314 - Laser mp3 3.mp3");
      assertThat(fls.contains(fl1), equalTo(true));
      assertThat(fls.contains(fl2), equalTo(true));
      assertThat(fls.contains(fl3), equalTo(true));

      c1      = new CheckProcessFile();
      c2      = new CheckProcessFile();
      c3      = new CheckProcessFile();
      c1.file = fl1;
      c2.file = fl2;
      c3.file = fl3;
      readFile(c1);
      readFile(c2);
      readFile(c3);

      // check file 1
      assertThat(c1.tag.getFirst(FieldKey.COMMENT), equalTo(""));
      assertThat(c1.tag.getFirst(FieldKey.TRACK), equalTo("27"));
      assertThat(c1.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("35"));
      assertThat(c1.tag.getFirst(FieldKey.DISC_NO), equalTo("1"));
      assertThat(c1.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("3"));

      // check file 2
      assertThat(c2.tag.getFirst(FieldKey.COMMENT), equalTo(""));
      assertThat(c2.tag.getFirst(FieldKey.TRACK), equalTo("07"));
      assertThat(c2.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("12"));
      assertThat(c2.tag.getFirst(FieldKey.DISC_NO), equalTo("2"));
      assertThat(c2.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("3"));

      // check file 3
      assertThat(c3.tag.getFirst(FieldKey.COMMENT), equalTo(""));
      assertThat(c3.tag.getFirst(FieldKey.TRACK), equalTo("14"));
      assertThat(c3.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("19"));
      assertThat(c3.tag.getFirst(FieldKey.DISC_NO), equalTo("3"));
      assertThat(c3.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("3"));

      assertThat(errors.size(), equalTo(0));
      assertThat(reports.size(), equalTo(1));
      reps = reports.get(dstDir);
      assertThat(reps, notNullValue());
      assertThat(reps.errors.size(), equalTo(0));
      assertThat(reps.notices.size(), equalTo(4));
      assertThat(reps.notices.get(0), equalTo(impl.getReportPrefix()));
      assertThat(reps.notices.get(1), equalTo("  01 - Laser mp3 1.mp3 --> " + fl1.getName()));
      assertThat(reps.notices.get(2), equalTo("  02 - Laser mp3 2.mp3 --> " + fl2.getName()));
      assertThat(reps.notices.get(3), equalTo("  03 - Laser mp3 3.mp3 --> " + fl3.getName()));

    }
    finally {
      FileUtils.delete(dstDir);
    }
  }

  @Test(timeout = 8000)
  public void testFilesTagsRead_Empty() {
    Map<File, CheckProcessFile>    map     = new HashMap<>();
    List<File>                     errors  = new LinkedList<>();
    Map<File, CheckProcessReports> reports = new HashMap<>();

    CheckProcessInstruction r = impl.filesTagsRead(map, errors, reports);

    assertThat(r, equalTo(CheckProcessInstruction.CONTINUE));
    assertThat(errors.size(), equalTo(0));
    assertThat(reports.size(), equalTo(0));
  }

  @Test(timeout = 8000)
  public void testFilesTagsRead_ReadOnlyFile() throws FileAlreadyExistsException, FileNotFoundException, IOException,
      CannotReadException, TagException, ReadOnlyFileException, InvalidAudioFrameException {
    Map<File, CheckProcessFile>    map     = new HashMap<>();
    List<File>                     errors  = new LinkedList<>();
    Map<File, CheckProcessReports> reports = new HashMap<>();

    File src      = new File(testResourcesDir, laserMp3v24);
    File dst1     = new File(testResourcesDir, "temp-file 1.mp3");
    File dst2     = new File(testResourcesDir, "temp-file 2.mp3");
    Path srcPath  = src.toPath();
    Path dst1Path = dst1.toPath();
    Path dst2Path = dst2.toPath();

    Set<PosixFilePermission> dst1Perms = Files.getPosixFilePermissions(srcPath);
    Set<PosixFilePermission> dst2Perms = Files.getPosixFilePermissions(srcPath);
    try {
      if (dst1.exists()) {
        Files.setPosixFilePermissions(dst1Path, dst1Perms);
        FileUtils.delete(dst1);
      }
      if (dst2.exists()) {
        Files.setPosixFilePermissions(dst2Path, dst2Perms);
        FileUtils.delete(dst2);
      }
      FileUtils.copy(src, dst1);
      FileUtils.copy(src, dst2);

      CheckProcessFile value = new CheckProcessFile();
      value.file = dst1;
      readFile(value);
      map.put(dst1, value);

      value      = new CheckProcessFile();
      value.file = dst2;
      readFile(value);
      map.put(dst2, value);

      dst1Perms = Files.getPosixFilePermissions(dst1Path);
      dst2Perms = Files.getPosixFilePermissions(dst2Path);
      Files.setPosixFilePermissions(dst1Path, new HashSet<>());
      Files.setPosixFilePermissions(dst2Path, new HashSet<>());

      CheckProcessReports cpr2 = new CheckProcessReports();
      reports.put(dst2, cpr2);

      CheckProcessInstruction r = impl.filesTagsRead(map, errors, reports);

      assertThat(r, equalTo(CheckProcessInstruction.CONTINUE));
      assertThat(errors.size(), equalTo(0));
      assertThat(reports.size(), equalTo(2));
      CheckProcessReports reps1 = reports.get(dst1);
      CheckProcessReports reps2 = reports.get(dst2);
      assertThat(reps1, notNullValue());
      assertThat(reps2, notNullValue());
      assertThat(reps1.notices.size(), equalTo(0));
      assertThat(reps1.errors.size(), equalTo(1));
      assertThat(reps2.notices.size(), equalTo(0));
      assertThat(reps2.errors.size(), equalTo(1));
      assertThat(reps1.errors.get(0),
          equalTo(String.format(impl.getReportPrefix() + Messages.getString("Flatten.5"), dst1.toString(),
              "org.jaudiotagger.audio.exceptions.UnableToModifyFileException: Cannot modify " + dst1.getAbsolutePath()
                  + " because do not have permissions to modify file")));
      assertThat(reps2.errors.get(0),
          equalTo(String.format(impl.getReportPrefix() + Messages.getString("Flatten.5"), dst2.toString(),
              "org.jaudiotagger.audio.exceptions.UnableToModifyFileException: Cannot modify " + dst2.getAbsolutePath()
                  + " because do not have permissions to modify file")));
    }
    finally {
      if (dst1.exists()) {
        Files.setPosixFilePermissions(dst1Path, dst1Perms);
        FileUtils.delete(dst1);
      }
      if (dst2.exists()) {
        Files.setPosixFilePermissions(dst2Path, dst2Perms);
        FileUtils.delete(dst2);
      }
    }
  }

  @Test(timeout = 8000)
  public void testFilesTagsConverted() {
    assertThat(impl.filesTagsConverted(null, null, null), equalTo(CheckProcessInstruction.CONTINUE));
  }

  @Test(timeout = 8000)
  public void testFilesExit() {
    assertThat(impl.filesExit(null, null), equalTo(CheckProcessInstruction.CONTINUE));
  }

  @Test(timeout = 8000)
  public void testFilesTagsRead_Shutdown() throws FileAlreadyExistsException, FileNotFoundException, IOException,
      CannotReadException, TagException, ReadOnlyFileException, InvalidAudioFrameException {
    Map<File, CheckProcessFile>    map     = new HashMap<>();
    List<File>                     errors  = new LinkedList<>();
    Map<File, CheckProcessReports> reports = new HashMap<>();

    File srcDir = new File(testResourcesDir, "subdir-multi");
    File dstDir = new File(testResourcesDir, "subdir-multi-temp");

    CheckProcessInstruction r;

    impl.shutdownHook();

    try {
      FileUtils.delete(dstDir);
      FileUtils.copy(srcDir, dstDir);
      AbstractFlattener.renameFile(new File(dstDir, "0127 - Laser mp3 1.mp3"), "Laser mp3 1.mp3");

      File[] files = dstDir.listFiles();
      for (File f : files) {
        CheckProcessFile value = new CheckProcessFile();
        value.file = f;
        readFile(value);
        map.put(f, value);
      }

      r = impl.filesTagsRead(map, errors, reports);

      assertThat(r, equalTo(CheckProcessInstruction.CONTINUE));
      assertThat(errors.size(), equalTo(0));
      assertThat(reports.size(), equalTo(0));

      files = dstDir.listFiles();
      assertThat(files.length, equalTo(3));
      List<File> fls = Arrays.asList(files);
      File       fl1 = new File(dstDir, "Laser mp3 1.mp3");
      File       fl2 = new File(dstDir, "0207 - Laser mp3 2.mp3");
      File       fl3 = new File(dstDir, "0314 - Laser mp3 3.mp3");
      assertThat(fls.contains(fl1), equalTo(true));
      assertThat(fls.contains(fl2), equalTo(true));
      assertThat(fls.contains(fl3), equalTo(true));

      CheckProcessFile c1 = new CheckProcessFile();
      CheckProcessFile c2 = new CheckProcessFile();
      CheckProcessFile c3 = new CheckProcessFile();
      c1.file = fl1;
      c2.file = fl2;
      c3.file = fl3;
      readFile(c1);
      readFile(c2);
      readFile(c3);

      // check file 1
      assertThat(c1.tag.getFirst(FieldKey.COMMENT), equalTo(""));
      assertThat(c1.tag.getFirst(FieldKey.TRACK), equalTo("27"));
      assertThat(c1.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("35"));
      assertThat(c1.tag.getFirst(FieldKey.DISC_NO), equalTo("01"));
      assertThat(c1.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("03"));

      // check file 2
      assertThat(c2.tag.getFirst(FieldKey.COMMENT), equalTo(""));
      assertThat(c2.tag.getFirst(FieldKey.TRACK), equalTo("07"));
      assertThat(c2.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("12"));
      assertThat(c2.tag.getFirst(FieldKey.DISC_NO), equalTo("02"));
      assertThat(c2.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("03"));

      // check file 3
      assertThat(c3.tag.getFirst(FieldKey.COMMENT), equalTo(""));
      assertThat(c3.tag.getFirst(FieldKey.TRACK), equalTo("14"));
      assertThat(c3.tag.getFirst(FieldKey.TRACK_TOTAL), equalTo("19"));
      assertThat(c3.tag.getFirst(FieldKey.DISC_NO), equalTo("03"));
      assertThat(c3.tag.getFirst(FieldKey.DISC_TOTAL), equalTo("03"));
    }
    finally {
      FileUtils.delete(dstDir);
    }
  }

  @Test(timeout = 8000)
  public void testProcessTrack_Shutdown() throws Throwable {
    Map<String, ProcessableAlbum> processableAlbums = new TreeMap<>();
    CheckProcessReports           fileReports       = new CheckProcessReports();

    FileContainer c;

    // no track title

    c                   = new FileContainer();
    c.trackTitle        = "title";
    c.trackNumber       = 12;
    c.trackCount        = 34;
    c.discNumber        = 2;
    c.discCount         = 3;
    c.comment           = "";
    c.hasFlattenComment = false;

    boolean r;
    File    f = null;

    try {
      f = prepareFile(false, laserMp3v24, c, true);

      r = impl.fileToProcessableAlbum(c.checkProcessFile, processableAlbums, fileReports);
      assertThat(r, equalTo(false));

      impl.shutdownHook();

      ProcessableAlbum processableAlbum = processableAlbums.get(TESTALBUM);
      processableAlbum.trackDigits = 6;
      Map<File, CheckProcessReports> reports = new HashMap<>();
      r = impl.processTrack(processableAlbum, c, 2, reports);
      assertThat(r, equalTo(false));
      assertThat(reports.size(), equalTo(0));
    }
    finally {
      if (f != null) {
        f.delete();
        f = null;
      }
    }
  }
}