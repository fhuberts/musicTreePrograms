package nl.pelagic.audio.tag.checker.flatten.flatten;

import nl.pelagic.audio.tag.checker.api.CheckProcessPlugin;
import nl.pelagic.audio.tag.checker.flatten.common.AbstractFlattener;
import nl.pelagic.audio.tag.checker.flatten.common.FileContainer;
import nl.pelagic.audio.tag.checker.flatten.common.i18n.Messages;
import nl.pelagic.shutdownhook.api.ShutdownHookParticipant;

import org.osgi.service.component.annotations.Component;

/**
 * This plugin flattens multi-disc flac or mp3 albums, in a reversible fashion (through the comment of the tag).
 */
@Component(service = {
    CheckProcessPlugin.class, //
    ShutdownHookParticipant.class
})
public class FlattenAlbums extends AbstractFlattener {
  /** the prefix for notices and errors */
  final String REPORT_PREFIX;

  /**
   * Constructor
   */
  public FlattenAlbums() {
    REPORT_PREFIX = Messages.getString("Flatten.0") + ": "; //$NON-NLS-1$ //$NON-NLS-2$
  }

  /*
   * AbstractFlattener
   */

  @Override
  protected boolean isFlattenMode() {
    return true;
  }

  @Override
  protected String getReportPrefix() {
    return REPORT_PREFIX;
  }

  @Override
  protected boolean shouldIgnoreFile(FileContainer container) {
    /* ignore tracks on single-disc albums */
    if (container.discCount <= 1) {
      return true;
    }

    /* ignore tracks that already have the COMMENT_PREFIX comment */
    return container.hasFlattenComment;
  }

  /*
   * CheckProcessPlugin
   */

  @Override
  public boolean isEnabledByDefault() {
    return false;
  }
}