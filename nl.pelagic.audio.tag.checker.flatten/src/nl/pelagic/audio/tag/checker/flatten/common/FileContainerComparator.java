package nl.pelagic.audio.tag.checker.flatten.common;

import java.io.File;
import java.util.Comparator;

import nl.pelagic.audio.tag.checker.types.CheckProcessFile;

/**
 * Compares 2 FileContainer objects, used when sorting lists of album tracks
 */
public class FileContainerComparator implements Comparator<FileContainer> {

  @Override
  public int compare(FileContainer o1, FileContainer o2) {
    if (o1 == null) {
      return 1;
    }
    if (o2 == null) {
      return -1;
    }

    if (o1.discCount < o2.discCount) {
      return -1;
    }
    if (o1.discCount > o2.discCount) {
      return 1;
    }

    if (o1.discNumber < o2.discNumber) {
      return -1;
    }
    if (o1.discNumber > o2.discNumber) {
      return 1;
    }

    if (o1.trackCount < o2.trackCount) {
      return -1;
    }
    if (o1.trackCount > o2.trackCount) {
      return 1;
    }

    if (o1.trackNumber < o2.trackNumber) {
      return -1;
    }
    if (o1.trackNumber > o2.trackNumber) {
      return 1;
    }

    CheckProcessFile cpf1 = o1.checkProcessFile;
    CheckProcessFile cpf2 = o2.checkProcessFile;

    if (cpf1 == null) {
      return 1;
    }
    if (cpf2 == null) {
      return -1;
    }

    File f1 = cpf1.file;
    File f2 = cpf2.file;

    if (f1 == null) {
      return 1;
    }
    if (f2 == null) {
      return -1;
    }

    return f1.getAbsolutePath().compareTo(f2.getAbsolutePath());
  }
}