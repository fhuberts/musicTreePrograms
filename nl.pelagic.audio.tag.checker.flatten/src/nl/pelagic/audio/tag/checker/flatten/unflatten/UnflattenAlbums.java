package nl.pelagic.audio.tag.checker.flatten.unflatten;

import nl.pelagic.audio.tag.checker.api.CheckProcessPlugin;
import nl.pelagic.audio.tag.checker.flatten.common.AbstractFlattener;
import nl.pelagic.audio.tag.checker.flatten.common.FileContainer;
import nl.pelagic.audio.tag.checker.flatten.common.i18n.Messages;
import nl.pelagic.shutdownhook.api.ShutdownHookParticipant;

import org.osgi.service.component.annotations.Component;

/**
 * This plugin un-flattens single-disc flac or mp3 albums (through the comment of the tag).
 */
@Component(service = {
    CheckProcessPlugin.class, //
    ShutdownHookParticipant.class
})
public class UnflattenAlbums extends AbstractFlattener {
  /** the prefix for notices and errors */
  final String REPORT_PREFIX;

  /**
   * Constructor
   */
  public UnflattenAlbums() {
    REPORT_PREFIX = Messages.getString("Flatten.1") + ": "; //$NON-NLS-1$ //$NON-NLS-2$
  }

  /*
   * AbstractFlattener
   */

  @Override
  protected boolean isFlattenMode() {
    return false;
  }

  @Override
  protected String getReportPrefix() {
    return REPORT_PREFIX;
  }

  @Override
  protected boolean shouldIgnoreFile(FileContainer container) {
    /* ignore tracks that don't have the COMMENT_PREFIX comment */
    return !container.hasFlattenComment;
  }

  /*
   * CheckProcessPlugin
   */

  @Override
  public boolean isEnabledByDefault() {
    return false;
  }
}