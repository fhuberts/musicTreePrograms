package nl.pelagic.audio.tag.checker.flatten.common;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.pelagic.audio.tag.checker.api.CheckProcessPlugin;
import nl.pelagic.audio.tag.checker.flatten.common.i18n.Messages;
import nl.pelagic.audio.tag.checker.types.CheckProcessFile;
import nl.pelagic.audio.tag.checker.types.CheckProcessInstruction;
import nl.pelagic.audio.tag.checker.types.CheckProcessReports;
import nl.pelagic.shutdownhook.api.ShutdownHookParticipant;
import nl.pelagic.util.file.ExtensionUtils;

import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.tag.FieldDataInvalidException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.KeyNotFoundException;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.flac.FlacTag;
import org.jaudiotagger.tag.id3.ID3v22Tag;
import org.jaudiotagger.tag.id3.ID3v23Tag;
import org.jaudiotagger.tag.id3.ID3v24Tag;

/**
 * Base class for flattening and un-flattening
 */
public abstract class AbstractFlattener implements CheckProcessPlugin, ShutdownHookParticipant {
  /** the tag comment prefix for processed tracks */
  public static final String COMMENT_PREFIX = "AUDIOTAGCHECKER-FLATTENED-ALBUM:"; //$NON-NLS-1$

  /** the tag comment prefix string format for processed tracks */
  public static final String COMMENT_PREFIX_FMT = COMMENT_PREFIX + "%d:%d:%d:%d"; //$NON-NLS-1$

  /** the tag comment prefix for processed tracks */
  public static final String COMMENT_PREFIX_REGEX = COMMENT_PREFIX + "(\\d+):(\\d+):(\\d+):(\\d+)"; //$NON-NLS-1$

  /** the compiled {@link #COMMENT_PREFIX_REGEX} */
  protected final Pattern commentPattern;

  /**
   * Constructor
   */
  public AbstractFlattener() {
    commentPattern = Pattern.compile(COMMENT_PREFIX_REGEX);
  }

  /*
   * Helpers
   */

  /**
   * @return true when flattening, false when un-flattening
   */
  protected abstract boolean isFlattenMode();

  /**
   * @return the prefix to use in notices and errors
   */
  protected abstract String getReportPrefix();

  /**
   * @param container
   *          the file information
   * @return true when the file should be ignored
   */
  protected abstract boolean shouldIgnoreFile(FileContainer container);

  /**
   * Process a file into a map of albums.
   *
   * Add a track in the processing maps when it has the required tag information an when it is part of a multi-disc
   * album.
   *
   * @param checkProcessFile
   *          the check process track data
   * @param processableAlbums
   *          a map of albums against their tracks and album information
   * @param fileReports
   *          the notices and errors for the track to fill
   * @return true when there was an error with the track, false when everything is ok
   */
  boolean fileToProcessableAlbum(CheckProcessFile checkProcessFile, Map<String, ProcessableAlbum> processableAlbums,
      CheckProcessReports fileReports) {
    FileContainer container = new FileContainer();

    container.checkProcessFile = checkProcessFile;

    container.trackTitle = checkProcessFile.tag.getFirst(FieldKey.TITLE).trim();
    if (container.trackTitle.isEmpty()) {
      fileReports.errors.add(getReportPrefix() + Messages.getString("Flatten.2")); //$NON-NLS-1$
      return true;
    }

    try {
      container.trackNumber = Integer.parseInt(checkProcessFile.tag.getFirst(FieldKey.TRACK).trim());
      container.trackCount  = Integer.parseInt(checkProcessFile.tag.getFirst(FieldKey.TRACK_TOTAL).trim());
      container.discNumber  = Integer.parseInt(checkProcessFile.tag.getFirst(FieldKey.DISC_NO).trim());
      container.discCount   = Integer.parseInt(checkProcessFile.tag.getFirst(FieldKey.DISC_TOTAL).trim());
    }
    catch (NumberFormatException e) {
      fileReports.errors.add(getReportPrefix() + Messages.getString("Flatten.3") + ": " + e.getLocalizedMessage()); //$NON-NLS-1$ //$NON-NLS-2$
      return true;
    }

    container.comment = checkProcessFile.tag.getFirst(FieldKey.COMMENT).trim();

    Matcher m = commentPattern.matcher(container.comment);
    container.hasFlattenComment = m.matches();
    if (container.hasFlattenComment) {
      try {
        container.trackNumberOriginal = Integer.parseInt(m.group(1));
        container.trackCountOriginal  = Integer.parseInt(m.group(2));
        container.trackDigitsOriginal = Math.max(2, (int) Math.log10(container.trackCountOriginal) + 1);
        container.discNumberOriginal  = Integer.parseInt(m.group(3));
        container.discCountOriginal   = Integer.parseInt(m.group(4));
        container.discDigitsOriginal  = Math.max(2, (int) Math.log10(container.discCountOriginal) + 1);
      }
      catch (NumberFormatException e) {
        fileReports.errors.add(getReportPrefix() + Messages.getString("Flatten.6") + ": " + e.getLocalizedMessage()); //$NON-NLS-1$ //$NON-NLS-2$
        return true;
      }
    }

    if (!shouldIgnoreFile(container)) {
      String           albumTitle       = checkProcessFile.tag.getFirst(FieldKey.ALBUM).trim();
      ProcessableAlbum processableAlbum = processableAlbums.get(albumTitle);
      if (processableAlbum == null) {
        processableAlbum = new ProcessableAlbum();
        processableAlbums.put(albumTitle, processableAlbum);
        processableAlbum.name = albumTitle;
      }

      processableAlbum.tracks.add(container);
      processableAlbum.discCountMax      = Math.max(processableAlbum.discCountMax, container.discCount);
      processableAlbum.fileNameLengthMax =
          Math.max(processableAlbum.fileNameLengthMax, container.checkProcessFile.file.getName().length());
    }

    return false;
  }

  /**
   * Process files into a map of albums.
   *
   * Only flac files and mp3 files are accepted, others are ignored.
   *
   * All albums have their tracks sorted.
   *
   * @param map
   *          a map of files against their gathered (check process) data
   * @param processableAlbums
   *          a map of albums against their tracks and album information
   * @param reports
   *          a map to fill with files against their notices and reports. The reports map is pre-filled and contains all
   *          files that are in map, with empty notices and errors
   * @return true when there were no errors and when there are albums with tracks
   */
  boolean filesToProcessableAlbums(Map<File, CheckProcessFile> map, Map<String, ProcessableAlbum> processableAlbums,
      Map<File, CheckProcessReports> reports) {
    boolean errors = false;

    for (CheckProcessFile checkProcessFile : map.values()) {
      CheckProcessReports fileReports = reports.get(checkProcessFile.file);
      if (fileReports == null) {
        fileReports = new CheckProcessReports();
      }

      if (!run.get()) {
        return false;
      }

      /* all files need to be flac or mp3 files */
      Class<? extends Tag> tagClass = checkProcessFile.tag.getClass();
      if ((tagClass != ID3v24Tag.class) //
          && (tagClass != ID3v23Tag.class) //
          && (tagClass != ID3v22Tag.class) //
          && (tagClass != FlacTag.class) //
      ) {
        fileReports.errors.add(getReportPrefix() + Messages.getString("Flatten.4")); //$NON-NLS-1$
        errors = true;
      } else {
        errors = fileToProcessableAlbum(checkProcessFile, processableAlbums, fileReports) //
            || errors;
      }

      if (!fileReports.isEmpty()) {
        reports.put(checkProcessFile.file, fileReports);
      }
    }

    boolean allEmpty = true;

    /* sort tracks for all albums */
    for (ProcessableAlbum processableAlbum : processableAlbums.values()) {
      Collections.sort(processableAlbum.tracks, new FileContainerComparator());
      processableAlbum.trackDigits = Math.max(2, (int) Math.log10(processableAlbum.tracks.size()) + 1);

      if (!processableAlbum.tracks.isEmpty()) {
        allEmpty = false;
      }
    }

    return !allEmpty //
        && !errors;
  }

  /**
   * Update the tag of a track
   *
   * @param processableAlbum
   *          the album
   * @param track
   *          the track
   * @param trackNumber
   *          the track number
   * @throws KeyNotFoundException
   *           when a tag field name was invalid
   * @throws FieldDataInvalidException
   *           when a tag field value was invalid
   * @throws CannotWriteException
   *           when a tag could not be written
   */
  protected void updateTag(ProcessableAlbum processableAlbum, FileContainer track, int trackNumber)
      throws KeyNotFoundException, FieldDataInvalidException, CannotWriteException {
    Tag tag = track.checkProcessFile.tag;

    String trackNr;
    int    trackCount;
    int    discNumber;
    int    discCount;

    if (isFlattenMode()) {
      trackNr    = String.format("%0" + processableAlbum.trackDigits + "d",                            //$NON-NLS-1$ //$NON-NLS-2$
          Integer.valueOf(trackNumber));
      trackCount = processableAlbum.tracks.size();
      discNumber = 1;
      discCount  = 1;

      String tagComment = String.format(COMMENT_PREFIX_FMT, //
          Integer.valueOf(track.trackNumber), //
          Integer.valueOf(track.trackCount), //
          Integer.valueOf(track.discNumber), //
          Integer.valueOf(track.discCount));
      tag.setField(FieldKey.COMMENT, tagComment);
    } else {
      trackNr    = String.format("%0" + track.trackDigitsOriginal + "d",                            //$NON-NLS-1$ //$NON-NLS-2$
          Integer.valueOf(track.trackNumberOriginal));
      trackCount = track.trackCountOriginal;
      discNumber = track.discNumberOriginal;
      discCount  = track.discCountOriginal;

      tag.deleteField(FieldKey.COMMENT);
    }

    tag.setField(FieldKey.TRACK, trackNr);
    tag.setField(FieldKey.TRACK_TOTAL, Integer.toString(trackCount));
    tag.setField(FieldKey.DISC_NO, Integer.toString(discNumber));
    tag.setField(FieldKey.DISC_TOTAL, Integer.toString(discCount));

    track.checkProcessFile.af.commit();
  }

  /**
   * Rename a file in the same directory
   *
   * @param file
   *          the file
   * @param fileNameNew
   *          the new filename
   * @throws IOException
   *           when the rename failed
   */
  protected static void renameFile(File file, String fileNameNew) throws IOException {
    Path src = file.toPath();
    Path dst = src.resolveSibling(fileNameNew);

    Files.move(src, dst, StandardCopyOption.ATOMIC_MOVE);
  }

  /**
   * Reports a processed file. The file is reported on the notices of its directory.
   *
   * @param reports
   *          a map to fill with files against their notices and reports. The reports map is pre-filled and contains all
   *          files that are in map, with empty notices and errors
   * @param track
   *          the track
   * @param fileNameNew
   *          the new filename
   * @param fileNameLengthMax
   *          the maximum length of filenames in the album
   */
  protected void reportProcessFile(Map<File, CheckProcessReports> reports, FileContainer track, String fileNameNew,
      int fileNameLengthMax) {
    File                trackDir            = track.checkProcessFile.file.getParentFile();
    CheckProcessReports checkProcessReports = reports.get(trackDir);
    if (checkProcessReports == null) {
      checkProcessReports = new CheckProcessReports();
      reports.put(trackDir, checkProcessReports);
      checkProcessReports.notices.add(getReportPrefix());
    }
    checkProcessReports.notices.add(String.format("  %-" + Integer.toString(fileNameLengthMax) + "s --> %s", //$NON-NLS-1$ //$NON-NLS-2$
        track.checkProcessFile.file.getName(), fileNameNew));
  }

  /**
   * Process a track
   *
   * @param processableAlbum
   *          the album
   * @param track
   *          the track
   * @param trackNumber
   *          the track number
   * @param reports
   *          a map to fill with files against their notices and reports. The reports map is pre-filled and contains all
   *          files that are in map, with empty notices and errors
   * @return true when one or more tracks were changed
   */
  boolean processTrack(ProcessableAlbum processableAlbum, FileContainer track, int trackNumber,
      Map<File, CheckProcessReports> reports) {
    String[] fileNameSplit = ExtensionUtils.split(track.checkProcessFile.file.getName(), true);
    String   fileName      = fileNameSplit[0].trim();
    String   extension     = fileNameSplit[1].trim();

    String fileNameWithoutNumbers = fileName.replaceFirst("^\\d+\\s*-\\s*", ""); //$NON-NLS-1$ //$NON-NLS-2$
    if (fileNameWithoutNumbers.equals(fileName)) {
      /*
       * fallback to the track title if the filename is not in the expected format
       */
      fileNameWithoutNumbers = track.trackTitle;
    }

    String fileNameNew;
    if (isFlattenMode()) {
      fileNameNew = String.format("%0" + processableAlbum.trackDigits + "d - %s%s", Integer.valueOf(trackNumber), //$NON-NLS-1$ //$NON-NLS-2$
          fileNameWithoutNumbers, extension);
    } else {
      fileNameNew = String.format("%0" + track.discDigitsOriginal + "d%0" + track.trackDigitsOriginal + "d - %s%s", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          Integer.valueOf(track.discNumberOriginal), Integer.valueOf(track.trackNumberOriginal), fileNameWithoutNumbers,
          extension);
    }

    if (!run.get()) {
      return false;
    }

    boolean changes = false;
    try {
      updateTag(processableAlbum, track, trackNumber);
      renameFile(track.checkProcessFile.file, fileNameNew);
      changes = true;
      reportProcessFile(reports, track, fileNameNew, processableAlbum.fileNameLengthMax);
    }
    catch (KeyNotFoundException //
        | FieldDataInvalidException //
        | CannotWriteException //
        | IOException //
    e) {
      File                f                   = track.checkProcessFile.file;
      CheckProcessReports checkProcessReports = reports.get(f);
      if (checkProcessReports == null) {
        checkProcessReports = new CheckProcessReports();
        reports.put(f, checkProcessReports);
      }
      checkProcessReports.errors.add(String.format(getReportPrefix() + Messages.getString("Flatten.5"), //$NON-NLS-1$
          track.checkProcessFile.file.toString(), e.getLocalizedMessage()));
    }

    return changes;
  }

  /**
   * Process a number of files, if needed
   *
   * @param map
   *          a map of files against their gathered (check process) data
   * @param reports
   *          a map to fill with files against their notices and reports. The reports map is pre-filled and contains all
   *          files that are in map, with empty notices and errors
   * @return true when one or more files were changed
   */

  boolean processFiles(Map<File, CheckProcessFile> map, Map<File, CheckProcessReports> reports) {
    if (map.isEmpty()) {
      return false;
    }

    Map<String, ProcessableAlbum> processableAlbums = new TreeMap<>();
    if (!filesToProcessableAlbums(map, processableAlbums, reports)) {
      return false;
    }

    /*
     * processableAlbums contains only albums with their tracks that should be processed, and all album tracks are
     * sorted
     */

    /* loop over all albums */
    boolean changes = false;
    for (ProcessableAlbum processableAlbum : processableAlbums.values()) {
      List<FileContainer> tracks = processableAlbum.tracks;

      /* loop over all album tracks */
      int trackNumber = 0;
      for (FileContainer track : tracks) {
        if (processTrack(processableAlbum, track, ++trackNumber, reports)) {
          changes = true;
        }
        if (!run.get()) {
          /* Can't be covered in a test */
          return false;
        }
      } // for (track : tracks)
    } // for (entry : albumTracksMap.entrySet())

    return changes;
  }

  /*
   * CheckProcessPlugin
   */

  /*
   * Directories
   */

  @Override
  public void directoryEntry(File directory, List<File> directories, List<File> regularFiles) {
    // nothing to do
  }

  @Override
  public CheckProcessInstruction directoryExit(File directory) {
    return CheckProcessInstruction.CONTINUE;
  }

  /*
   * Files
   */

  @Override
  public CheckProcessInstruction filesEntry(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesExtensions(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesRead(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesTagsRead(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    return processFiles(map, reports) //
        ? CheckProcessInstruction.RESTART_DIRECTORY //
        : CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesTagsConverted(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public CheckProcessInstruction filesExit(Map<File, CheckProcessFile> map, Map<File, CheckProcessReports> reports) {
    return CheckProcessInstruction.CONTINUE;
  }

  @Override
  public abstract boolean isEnabledByDefault();

  /*
   * ShutdownHookParticipant
   */

  /** true while we must keep running */
  AtomicBoolean run = new AtomicBoolean(true);

  @Override
  public void shutdownHook() {
    run.set(false);
  }
}