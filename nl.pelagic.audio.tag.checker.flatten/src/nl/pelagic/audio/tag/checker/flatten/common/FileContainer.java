package nl.pelagic.audio.tag.checker.flatten.common;

import nl.pelagic.audio.tag.checker.types.CheckProcessFile;

/**
 * Container for file/track information during the flattening process
 */
public class FileContainer {
  /** The state of a file during the check process */
  public CheckProcessFile checkProcessFile = null;

  /** The track title */
  public String trackTitle = null;

  /** The track comment */
  public String comment = null;

  /** true when the track has the proper flatten comment */
  public boolean hasFlattenComment = false;

  /*
   * Used during flattening
   */

  /** The track number */
  public int trackNumber = 0;

  /** The total number of tracks */
  public int trackCount = 0;

  /** The disc number */
  public int discNumber = 0;

  /** The total number of discs */
  public int discCount = 0;

  /*
   * Used during un-flattening
   */

  /** The track number of the original */
  public int trackNumberOriginal = 0;

  /** The total number of tracks of the original */
  public int trackCountOriginal = 0;

  /** the number of track digits of the original */
  public int trackDigitsOriginal = 0;

  /** The disc number of the original */
  public int discNumberOriginal = 0;

  /** The total number of discs of the original */
  public int discCountOriginal = 0;

  /** the number of disc digits of the original */
  public int discDigitsOriginal = 0;
}