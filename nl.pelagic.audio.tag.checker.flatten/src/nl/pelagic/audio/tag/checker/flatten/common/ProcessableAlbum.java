package nl.pelagic.audio.tag.checker.flatten.common;

import java.util.LinkedList;
import java.util.List;

/**
 * An album that is processed
 */
public class ProcessableAlbum {
  /** the album title */
  public String name = null;

  /** the album tracks */
  public List<FileContainer> tracks = new LinkedList<>();

  /** the maximum discs count of all album tracks */
  public int discCountMax = 0;

  /** the maximum length of all album track filenames */
  public int fileNameLengthMax = 0;

  /*
   * Used during flattening
   */

  /** the number of track digits */
  public int trackDigits = 0;
}