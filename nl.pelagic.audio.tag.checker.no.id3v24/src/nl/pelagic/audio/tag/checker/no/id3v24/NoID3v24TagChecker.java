package nl.pelagic.audio.tag.checker.no.id3v24;

import java.util.Set;

import nl.pelagic.audio.tag.checker.api.TagChecker;
import nl.pelagic.audio.tag.checker.no.id3v24.i18n.Messages;
import nl.pelagic.audio.tag.checker.types.GenericTag;
import nl.pelagic.audio.tag.checker.types.GenericTagFieldName;

import org.jaudiotagger.tag.id3.ID3v24Tag;
import org.osgi.service.component.annotations.Component;

/**
 * This bundle performs 'no ID3v24 tags' checks on a generic tag.
 */
@Component
public class NoID3v24TagChecker implements TagChecker {
  @Override
  public void check(GenericTag genericTag) {
    if (genericTag == null) {
      return;
    }

    /* get the classes of the source tags */
    Set<Class<? extends Object>> sourceTagClasses = genericTag.getSourceTagClasses();

    if (!sourceTagClasses.contains(ID3v24Tag.class)) {
      return;
    }

    genericTag.addReport(GenericTagFieldName.OTHER, Messages.getString("NoID3v24TagChecker.0"), null, null, null); //$NON-NLS-1$
  }

  @Override
  public boolean isEnabledByDefault() {
    return true;
  }
}