package nl.pelagic.audio.tag.checker.no.id3v24;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import java.util.List;
import java.util.Map;

import nl.pelagic.audio.tag.checker.types.GenericTag;
import nl.pelagic.audio.tag.checker.types.GenericTagFieldName;
import nl.pelagic.audio.tag.checker.types.ProblemReport;
import nl.pelagic.audio.tag.checker.types.TypeUtilsForTests;

import org.jaudiotagger.tag.flac.FlacTag;
import org.jaudiotagger.tag.id3.ID3v23Tag;
import org.jaudiotagger.tag.id3.ID3v24Tag;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

@SuppressWarnings({
    "nls",
    "static-method",
    "javadoc"
})
public class TestNoID3v24TagChecker {

  private static NoID3v24TagChecker checker;

  @BeforeClass
  public static void setUpBeforeClass() {
    checker = new NoID3v24TagChecker();
  }

  @AfterClass
  public static void tearDownAfterClass() {
    checker = null;
  }

  @Test(timeout = 8000)
  public void testCheck_Null() {
    checker.check(null);
  }

  @Test(timeout = 8000)
  public void testCheck_NoSourceClasses() {
    GenericTag tag = TypeUtilsForTests.setupTag("11", "1/1", "Title", "1", "2013", null, null, null, null);

    checker.check(tag);

    Map<GenericTagFieldName, List<ProblemReport>> reports = tag.getReports();
    assertThat(Integer.valueOf(reports.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testCheck_NoId3SourceClasses() {
    GenericTag tag = TypeUtilsForTests.setupTag("11", "1/1", "Title", "1", "2013", null, null, null, null);
    tag.addSourceTagClass(FlacTag.class);

    checker.check(tag);

    Map<GenericTagFieldName, List<ProblemReport>> reports = tag.getReports();
    assertThat(Integer.valueOf(reports.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testCheck_NoId3v24SourceClasses() {
    GenericTag tag = TypeUtilsForTests.setupTag("11", "1/1", "Title", "1", "2013", null, null, null, null);
    tag.addSourceTagClass(FlacTag.class);
    tag.addSourceTagClass(ID3v23Tag.class);

    checker.check(tag);

    Map<GenericTagFieldName, List<ProblemReport>> reports = tag.getReports();
    assertThat(Integer.valueOf(reports.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testCheck_Id3v24SourceClasses() {
    GenericTag tag = TypeUtilsForTests.setupTag("11", "1/1", "Title", "1", "2013", null, null, null, null);
    tag.addSourceTagClass(FlacTag.class);
    tag.addSourceTagClass(ID3v24Tag.class);

    checker.check(tag);

    Map<GenericTagFieldName, List<ProblemReport>> reports = tag.getReports();
    assertThat(Integer.valueOf(reports.size()), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testIsEnabledByDefault() {
    assertThat(Boolean.valueOf(checker.isEnabledByDefault()), equalTo(Boolean.TRUE));
  }
}