package nl.pelagic.util.string;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "static-method"
})
public class TestStringUtils {

  @Test(timeout = 8000)
  public void testInstance() {
    @SuppressWarnings("unused")
    StringUtils su = new StringUtils();
  }

  @Test(timeout = 8000)
  public void testEscQuote_Null() {
    String result = StringUtils.escQuote(null);
    assertThat(result, equalTo(null));
  }

  @Test(timeout = 8000)
  public void testEscQuote_Empty() {
    String result = StringUtils.escQuote(""); //$NON-NLS-1$
    assertThat(result, equalTo("")); //$NON-NLS-1$
  }

  @Test(timeout = 8000)
  public void testEscQuote_NoQuotes() {
    String result = StringUtils.escQuote("no quotes in this string"); //$NON-NLS-1$
    assertThat(result, equalTo("no quotes in this string")); //$NON-NLS-1$
  }

  @Test(timeout = 8000)
  public void testEscQuote_Quotes() {
    String result = StringUtils.escQuote("some \"quotes\" in this string"); //$NON-NLS-1$
    assertThat(result, equalTo("some \\\"quotes\\\" in this string")); //$NON-NLS-1$
  }
}
