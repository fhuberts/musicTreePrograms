package nl.pelagic.util.osgi;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.Version;

@SuppressWarnings({
    "javadoc",
    "nls",
    "static-method"
})
public class TestOsgiUtils {

  @Test(timeout = 8000)
  public void testInstance() {
    @SuppressWarnings("unused")
    OsgiUtils ou = new OsgiUtils();
  }

  @Test(timeout = 8000)
  public void testGetRunningBundleVersions_None() {
    MyBundleContext ctx = new MyBundleContext();

    RunningBundleVersionsResult r = OsgiUtils.getRunningBundleVersions(ctx);

    assertThat(Integer.valueOf(r.symbolicNameLengthMax), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(r.nameLengthMax), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(r.versionLengthMax), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(r.gitSHALengthMax), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(r.gitDescriptorLengthMax), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(r.versions.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testGetRunningBundleVersions_Null() {
    MyBundleContext ctx     = new MyBundleContext();
    Bundle[]        bundles = new Bundle[1];
    MyBundle        bundle1 = new MyBundle();
    bundles[0]  = bundle1;
    bundle1.bsn = null;
    ctx.bundles = bundles;

    RunningBundleVersionsResult r = OsgiUtils.getRunningBundleVersions(ctx);

    assertThat(Integer.valueOf(r.symbolicNameLengthMax),
        equalTo(Integer.valueOf(Constants.BUNDLE_SYMBOLICNAME.length())));
    assertThat(Integer.valueOf(r.nameLengthMax), equalTo(Integer.valueOf(Constants.BUNDLE_NAME.length())));
    assertThat(Integer.valueOf(r.versionLengthMax), equalTo(Integer.valueOf(Constants.BUNDLE_VERSION.length())));
    assertThat(Integer.valueOf(r.gitSHALengthMax), equalTo(Integer.valueOf(OsgiUtils.HEADER_GIT_SHA.length())));
    assertThat(Integer.valueOf(r.gitDescriptorLengthMax),
        equalTo(Integer.valueOf(OsgiUtils.HEADER_GIT_DESCRIPTOR.length())));
    assertThat(Integer.valueOf(r.versions.size()), equalTo(Integer.valueOf(1)));
    BundleVersionInfo entry = r.versions.get(Long.toString(bundle1.id));
    assertThat(entry, notNullValue());
    assertThat(entry.bundle, equalTo(bundle1));
    assertThat(entry.symbolicName, equalTo(Long.toString(bundle1.id)));
    assertThat(entry.name, equalTo(""));
    assertThat(entry.version, equalTo(""));
    assertThat(entry.gitSHA, equalTo(""));
    assertThat(entry.gitDescriptor, equalTo(""));
  }

  @Test(timeout = 8000)
  public void testGetRunningBundleVersions_One() {
    MyBundleContext ctx            = new MyBundleContext();
    String          name1          = "name 1";
    String          version1       = "42.43.44";
    String          gitsha1        = "git sha 1";
    String          gitdescriptor1 = "git descriptor 1";

    Bundle[] bundles = new Bundle[1];
    MyBundle bundle1 = new MyBundle();
    bundles[0]  = bundle1;
    bundle1.bsn = "bsn 1";
    bundle1.headers.put(Constants.BUNDLE_NAME, name1);
    bundle1.version = new Version(version1);
    bundle1.headers.put(OsgiUtils.HEADER_GIT_SHA, gitsha1);
    bundle1.headers.put(OsgiUtils.HEADER_GIT_DESCRIPTOR, gitdescriptor1);
    ctx.bundles = bundles;

    RunningBundleVersionsResult r = OsgiUtils.getRunningBundleVersions(ctx);

    assertThat(Integer.valueOf(r.symbolicNameLengthMax),
        equalTo(Integer.valueOf(Constants.BUNDLE_SYMBOLICNAME.length())));
    assertThat(Integer.valueOf(r.nameLengthMax), equalTo(Integer.valueOf(Constants.BUNDLE_NAME.length())));
    assertThat(Integer.valueOf(r.versionLengthMax), equalTo(Integer.valueOf(Constants.BUNDLE_VERSION.length())));
    assertThat(Integer.valueOf(r.gitSHALengthMax), equalTo(Integer.valueOf(gitsha1.length())));
    assertThat(Integer.valueOf(r.gitDescriptorLengthMax), equalTo(Integer.valueOf(gitdescriptor1.length())));
    assertThat(Integer.valueOf(r.versions.size()), equalTo(Integer.valueOf(1)));
    BundleVersionInfo entry = r.versions.get(bundle1.bsn);
    assertThat(entry, notNullValue());
    assertThat(entry.bundle, equalTo(bundle1));
    assertThat(entry.symbolicName, equalTo(bundle1.bsn));
    assertThat(entry.name, equalTo(name1));
    assertThat(entry.version, equalTo(version1));
    assertThat(entry.gitSHA, equalTo(gitsha1));
    assertThat(entry.gitDescriptor, equalTo(gitdescriptor1));
  }

  @Test(timeout = 8000)
  public void testGetRunningBundleVersions_Two() {
    MyBundleContext ctx            = new MyBundleContext();
    String          name1          = "name 1 name 1 name 1 name 1";
    String          version1       = "42.43.44";
    String          gitsha1        = "git sha 1 git sha 1 git sha 1 git sha 1";
    String          gitdescriptor1 = "git descriptor 1 git descriptor 1 git descriptor 1";

    String name2          = "name2";
    String version2       = "42.43.444.2435432";
    String gitsha2        = "git sha2";
    String gitdescriptor2 = "git descriptor 22 git descriptor 22 git descriptor 22";

    Bundle[] bundles = new Bundle[2];
    MyBundle bundle1 = new MyBundle();
    MyBundle bundle2 = new MyBundle();
    bundles[0] = bundle1;
    bundles[1] = bundle2;

    bundle1.bsn = "bsn 1";
    bundle1.headers.put(Constants.BUNDLE_NAME, name1);
    bundle1.version = new Version(version1);
    bundle1.headers.put(OsgiUtils.HEADER_GIT_SHA, gitsha1);
    bundle1.headers.put(OsgiUtils.HEADER_GIT_DESCRIPTOR, gitdescriptor1);

    bundle2.bsn = "bsn 22 bsn 22 bsn 22 bsn 22 bsn 22 bsn 22";
    bundle2.headers.put(Constants.BUNDLE_NAME, name2);
    bundle2.version = new Version(version2);
    bundle2.headers.put(OsgiUtils.HEADER_GIT_SHA, gitsha2);
    bundle2.headers.put(OsgiUtils.HEADER_GIT_DESCRIPTOR, gitdescriptor2);
    ctx.bundles = bundles;

    RunningBundleVersionsResult r = OsgiUtils.getRunningBundleVersions(ctx);
    assertThat(Integer.valueOf(r.symbolicNameLengthMax), equalTo(Integer.valueOf(bundle2.bsn.length())));
    assertThat(Integer.valueOf(r.nameLengthMax), equalTo(Integer.valueOf(name1.length())));
    assertThat(Integer.valueOf(r.versionLengthMax), equalTo(Integer.valueOf(version2.length())));
    assertThat(Integer.valueOf(r.gitSHALengthMax), equalTo(Integer.valueOf(gitsha1.length())));
    assertThat(Integer.valueOf(r.gitDescriptorLengthMax), equalTo(Integer.valueOf(gitdescriptor2.length())));

    assertThat(Integer.valueOf(r.versions.size()), equalTo(Integer.valueOf(2)));

    BundleVersionInfo entry = r.versions.get(bundle1.bsn);
    assertThat(entry, notNullValue());
    assertThat(entry.bundle, equalTo(bundle1));
    assertThat(entry.symbolicName, equalTo(bundle1.bsn));
    assertThat(entry.name, equalTo(name1));
    assertThat(entry.version, equalTo(version1));
    assertThat(entry.gitSHA, equalTo(gitsha1));
    assertThat(entry.gitDescriptor, equalTo(gitdescriptor1));

    entry = r.versions.get(bundle2.bsn);
    assertThat(entry, notNullValue());
    assertThat(entry.bundle, equalTo(bundle2));
    assertThat(entry.symbolicName, equalTo(bundle2.bsn));
    assertThat(entry.name, equalTo(name2));
    assertThat(entry.version, equalTo(version2));
    assertThat(entry.gitSHA, equalTo(gitsha2));
    assertThat(entry.gitDescriptor, equalTo(gitdescriptor2));

    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    PrintStream           out = new PrintStream(bos);
    OsgiUtils.showVersions(out, ctx);
    String s = bos.toString();
    assertThat(s, notNullValue());
    String[] ss = s.split("\n");
    assertThat(Integer.valueOf(ss.length), equalTo(Integer.valueOf(3)));
  }
}