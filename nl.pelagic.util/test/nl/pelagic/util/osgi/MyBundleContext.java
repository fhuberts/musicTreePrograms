package nl.pelagic.util.osgi;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.Dictionary;

import org.junit.Ignore;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceObjects;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

@Ignore
@SuppressWarnings("javadoc")
public class MyBundleContext implements BundleContext {
  @Override
  public String getProperty(String key) {
    throw new IllegalStateException();
  }

  @Override
  public Bundle getBundle() {
    throw new IllegalStateException();
  }

  @Override
  public Bundle installBundle(String location, InputStream input) throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public Bundle installBundle(String location) throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public Bundle getBundle(long id) {
    throw new IllegalStateException();
  }

  Bundle[] bundles = {};

  @Override
  public Bundle[] getBundles() {
    return bundles;
  }

  @Override
  public void addServiceListener(ServiceListener listener, String filter) throws InvalidSyntaxException {
    throw new IllegalStateException();
  }

  @Override
  public void addServiceListener(ServiceListener listener) {
    throw new IllegalStateException();
  }

  @Override
  public void removeServiceListener(ServiceListener listener) {
    throw new IllegalStateException();
  }

  @Override
  public void addBundleListener(BundleListener listener) {
    throw new IllegalStateException();
  }

  @Override
  public void removeBundleListener(BundleListener listener) {
    throw new IllegalStateException();
  }

  @Override
  public void addFrameworkListener(FrameworkListener listener) {
    throw new IllegalStateException();
  }

  @Override
  public void removeFrameworkListener(FrameworkListener listener) {
    throw new IllegalStateException();
  }

  @Override
  public ServiceRegistration<?> registerService(String[] clazzes, Object service, Dictionary<String, ?> properties) {
    throw new IllegalStateException();
  }

  @Override
  public ServiceRegistration<?> registerService(String clazz, Object service, Dictionary<String, ?> properties) {
    throw new IllegalStateException();
  }

  @Override
  public <S> ServiceRegistration<S> registerService(Class<S> clazz, S service, Dictionary<String, ?> properties) {
    throw new IllegalStateException();
  }

  @Override
  public <S> ServiceRegistration<S> registerService(Class<S> clazz, ServiceFactory<S> factory,
      Dictionary<String, ?> properties) {
    throw new IllegalStateException();
  }

  @Override
  public ServiceReference<?>[] getServiceReferences(String clazz, String filter) throws InvalidSyntaxException {
    throw new IllegalStateException();
  }

  @Override
  public ServiceReference<?>[] getAllServiceReferences(String clazz, String filter) throws InvalidSyntaxException {
    throw new IllegalStateException();
  }

  @Override
  public ServiceReference<?> getServiceReference(String clazz) {
    throw new IllegalStateException();
  }

  @Override
  public <S> ServiceReference<S> getServiceReference(Class<S> clazz) {
    throw new IllegalStateException();
  }

  @Override
  public <S> Collection<ServiceReference<S>> getServiceReferences(Class<S> clazz, String filter)
      throws InvalidSyntaxException {
    throw new IllegalStateException();
  }

  @Override
  public <S> S getService(ServiceReference<S> reference) {
    throw new IllegalStateException();
  }

  @Override
  public boolean ungetService(ServiceReference<?> reference) {
    throw new IllegalStateException();
  }

  @Override
  public <S> ServiceObjects<S> getServiceObjects(ServiceReference<S> reference) {
    throw new IllegalStateException();
  }

  @Override
  public File getDataFile(String filename) {
    throw new IllegalStateException();
  }

  @Override
  public Filter createFilter(String filter) throws InvalidSyntaxException {
    throw new IllegalStateException();
  }

  @Override
  public Bundle getBundle(String location) {
    throw new IllegalStateException();
  }
}