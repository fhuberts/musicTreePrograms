package nl.pelagic.util.file;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.nullValue;

import org.junit.Test;

@SuppressWarnings({
    "javadoc",
    "static-method",
    "nls"
})
public class TestFileName {

  @Test(timeout = 8000)
  public void testInstance() {
    @SuppressWarnings("unused")
    FileName fn = new FileName();
  }

  @Test(timeout = 8000)
  public void testContainsInvalidFATJolietCharacters_Null() {
    boolean r = FileName.containsInvalidFATJolietCharacters(null);
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));

    r = FileName.containsInvalidFATJolietCharacters("");
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));
  }

  @Test(timeout = 8000)
  public void testContainsInvalidFATJolietCharacters_No() {
    boolean r;

    r = FileName.containsInvalidFATJolietCharacters("sub1/sub2/some valid file name");
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));

    r = FileName.containsInvalidFATJolietCharacters("/sub1/sub2/some valid file name");
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));
  }

  @Test(timeout = 8000)
  public void testContainsInvalidFATJolietCharacters_Yes() {
    boolean r;

    r = FileName.containsInvalidFATJolietCharacters("sub1?/sub2/some invalid file name");
    assertThat(Boolean.valueOf(r), equalTo(Boolean.TRUE));

    r = FileName.containsInvalidFATJolietCharacters("sub1/sub2?/some invalid file name");
    assertThat(Boolean.valueOf(r), equalTo(Boolean.TRUE));

    r = FileName.containsInvalidFATJolietCharacters("sub1/sub2/some invalid file name?");
    assertThat(Boolean.valueOf(r), equalTo(Boolean.TRUE));

    r = FileName.containsInvalidFATJolietCharacters("/sub1?/sub2/some invalid file name");
    assertThat(Boolean.valueOf(r), equalTo(Boolean.TRUE));

    r = FileName.containsInvalidFATJolietCharacters("/sub1/sub2?/some invalid file name");
    assertThat(Boolean.valueOf(r), equalTo(Boolean.TRUE));

    r = FileName.containsInvalidFATJolietCharacters("/sub1/sub2/some invalid file name?");
    assertThat(Boolean.valueOf(r), equalTo(Boolean.TRUE));
  }

  @Test(timeout = 8000)
  public void testReplaceInvalidFATJolietCharacters_Null() {
    String r = FileName.replaceInvalidFATJolietCharacters(null);
    assertThat(r, nullValue());

    r = FileName.replaceInvalidFATJolietCharacters("");
    assertThat(r, equalTo(""));
  }

  @Test(timeout = 8000)
  public void testReplaceInvalidFATJolietCharacters_No() {
    String str = "sub1/sub2/some valid file name";
    String r   = FileName.replaceInvalidFATJolietCharacters(str);
    assertThat(r, equalTo(str));

    str = "/sub1/sub2/some valid file name";
    r   = FileName.replaceInvalidFATJolietCharacters(str);
    assertThat(r, equalTo(str));
  }

  @Test(timeout = 8000)
  public void testReplaceInvalidFATJolietCharacters_Yes() {
    String str = "sub1/sub2/some valid file name?";
    String exp = "sub1/sub2/some valid file name";
    String r   = FileName.replaceInvalidFATJolietCharacters(str);
    assertThat(r, equalTo(exp));

    str = "sub1/sub2?/some valid file name";
    exp = "sub1/sub2/some valid file name";
    r   = FileName.replaceInvalidFATJolietCharacters(str);
    assertThat(r, equalTo(exp));

    str = "sub1?/sub2/some valid file name";
    exp = "sub1/sub2/some valid file name";
    r   = FileName.replaceInvalidFATJolietCharacters(str);
    assertThat(r, equalTo(exp));

    str = "/sub1/sub2/some valid file name?";
    exp = "/sub1/sub2/some valid file name";
    r   = FileName.replaceInvalidFATJolietCharacters(str);
    assertThat(r, equalTo(exp));

    str = "/sub1/sub2?/some valid file name";
    exp = "/sub1/sub2/some valid file name";
    r   = FileName.replaceInvalidFATJolietCharacters(str);
    assertThat(r, equalTo(exp));

    str = "/sub1?/sub2/some valid file name";
    exp = "/sub1/sub2/some valid file name";
    r   = FileName.replaceInvalidFATJolietCharacters(str);
    assertThat(r, equalTo(exp));
  }

  @Test(timeout = 8000)
  public void testReplaceInvalidFATJolietCharacters_StartingDots() {
    String str = "sub1/sub2/...some valid file name";
    String exp = "sub1/sub2/some valid file name";
    String r   = FileName.replaceInvalidFATJolietCharacters(str);
    assertThat(r, equalTo(exp));
  }

  @Test(timeout = 8000)
  public void testReplaceInvalidFATJolietCharacters_EndingDots() {
    String str = "sub1/sub2/some valid file name...";
    String exp = "sub1/sub2/some valid file name";
    String r   = FileName.replaceInvalidFATJolietCharacters(str);
    assertThat(r, equalTo(exp));
  }
}