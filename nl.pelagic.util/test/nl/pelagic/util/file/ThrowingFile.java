package nl.pelagic.util.file;

import java.io.File;
import java.io.IOException;

import org.junit.Ignore;

@Ignore
@SuppressWarnings({
    "javadoc",
    "nls"
})
public class ThrowingFile extends File {

  private static final long serialVersionUID = 1L;

  public ThrowingFile(String path) {
    super(path);
  }

  @Override
  public String getCanonicalPath() throws IOException {
    throw new IOException("test");
  }

  boolean normalParentFile = false;
  File    parentFile       = null;

  @Override
  public File getParentFile() {
    if (normalParentFile) {
      return super.getParentFile();
    }

    return parentFile;
  }

  @Override
  public File getAbsoluteFile() {
    return this;
  }

  @Override
  public boolean delete() {
    return false;
  }

  File[] lstFiles = null;

  @Override
  public File[] listFiles() {
    return lstFiles;
  }

  @Override
  public boolean mkdirs() {
    return false;
  }
}
