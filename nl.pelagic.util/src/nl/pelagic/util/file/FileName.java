package nl.pelagic.util.file;

import java.io.File;

/**
 * Filename utilities
 */
public class FileName {
  /** Characters not allowed on FAT/FAT32 and Joliet file systems */
  private static final char[] invalidFATJolietCharacters = {
      '/', //
      ':', //
      '*', //
      '?', //
      '"', //
      '<', //
      '>', //
      '|'
  };

  /**
   * Replacement characters for characters not allowed on FAT/FAT32 and Joliet file systems
   */
  private static final char[] replacementFATJolietCharacters = {
      '-', //
      '-', //
      '+', //
      ' ', //
      '\'', //
      '(', //
      ')', //
      '-'
  };

  /**
   * Determine whether or not a file name contains characters that are not allowed on FAT/FAT32 or Joliet file systems.
   *
   * @param fileName
   *          the file name
   * @return true when the file name contains such characters
   */
  public static boolean containsInvalidFATJolietCharacters(String fileName) {
    if ((fileName == null) //
        || fileName.isEmpty()) {
      return false;
    }

    File   f   = new File(fileName);
    String dir = f.getParent();
    if (dir != null) {
      if (containsInvalidFATJolietCharacters(dir)) {
        return true;
      }
    }

    String name = f.getName();

    for (char invalidFATJolietCharacter : invalidFATJolietCharacters) {
      if (name.indexOf(invalidFATJolietCharacter) >= 0) {
        return true;
      }
    }

    return false;
  }

  /**
   * In a file name replace all characters that are not allowed on FAT/FAT32 or Joliet file systems by a replacement
   * character.
   *
   * @param fileName
   *          the file name
   * @return the new file name
   */
  public static String replaceInvalidFATJolietCharacters(String fileName) {
    if ((fileName == null) //
        || fileName.isEmpty()) {
      return fileName;
    }

    File   f   = new File(fileName);
    String dir = f.getParent();
    if (dir != null) {
      dir = replaceInvalidFATJolietCharacters(dir);
    }

    String fileNameInt = f.getName();
    for (int i = 0; i < invalidFATJolietCharacters.length; i++) {
      char invalidFATJolietCharacter     = invalidFATJolietCharacters[i];
      char replacementFATJolietCharacter = replacementFATJolietCharacters[i];
      fileNameInt = fileNameInt.replace(invalidFATJolietCharacter, replacementFATJolietCharacter);
    }

    while (fileNameInt.startsWith(".")) { //$NON-NLS-1$
      fileNameInt = fileNameInt.substring(1);
    }

    while (fileNameInt.endsWith(".")) { //$NON-NLS-1$
      fileNameInt = fileNameInt.substring(0, fileNameInt.length() - 1);
    }

    return new File(dir, fileNameInt.trim()).getPath();
  }
}