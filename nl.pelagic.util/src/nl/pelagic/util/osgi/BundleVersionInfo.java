package nl.pelagic.util.osgi;

import org.osgi.framework.Bundle;

/**
 * Bundle version information
 */
public class BundleVersionInfo {
  /** the bundle, can't be null */
  Bundle bundle;

  /** the bundle's symbolic name, can't be null */
  String symbolicName;

  /** the bundle's name, can't be null */
  String name;

  /** the bundle's version, can't be null */
  String version;

  /** the bundle's git SHA, can't be null */
  String gitSHA;

  /** the bundle's git descriptor, can't be null */
  String gitDescriptor;
}