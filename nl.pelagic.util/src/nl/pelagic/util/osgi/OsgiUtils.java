package nl.pelagic.util.osgi;

import java.io.PrintStream;
import java.util.Dictionary;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Version;

/**
 * OSGi Utilities
 */
public class OsgiUtils {

  /** the manifest header for a Git SHA */
  public static final String HEADER_GIT_SHA = "Git-SHA"; //$NON-NLS-1$

  /** the manifest header for a Git descriptor */
  public static final String HEADER_GIT_DESCRIPTOR = "Git-Descriptor"; //$NON-NLS-1$

  /**
   * Get version information from all bundles in the framework
   *
   * @param ctx
   *          a bundle context from which to get all bundles
   * @return a container with running bundles version information
   */
  public static RunningBundleVersionsResult getRunningBundleVersions(BundleContext ctx) {
    RunningBundleVersionsResult result = new RunningBundleVersionsResult();

    for (Bundle bundle : ctx.getBundles()) {
      Dictionary<String, String> headers = bundle.getHeaders();

      BundleVersionInfo bvi = new BundleVersionInfo();

      bvi.bundle = bundle;

      bvi.symbolicName = bundle.getSymbolicName();
      if (bvi.symbolicName == null) {
        bvi.symbolicName = Long.toString(bundle.getBundleId());
      }

      bvi.name = headers.get(Constants.BUNDLE_NAME);
      if (bvi.name == null) {
        bvi.name = ""; //$NON-NLS-1$
      }

      Version v = bundle.getVersion();
      bvi.version = (v == Version.emptyVersion) ? "" : v.toString(); //$NON-NLS-1$

      bvi.gitSHA = headers.get(HEADER_GIT_SHA); // $NON-NLS-1$
      if (bvi.gitSHA == null) {
        bvi.gitSHA = ""; //$NON-NLS-1$
      }

      bvi.gitDescriptor = headers.get(HEADER_GIT_DESCRIPTOR); // $NON-NLS-1$
      if (bvi.gitDescriptor == null) {
        bvi.gitDescriptor = ""; //$NON-NLS-1$
      }

      result.versions.put(bvi.symbolicName, bvi);

      result.symbolicNameLengthMax  = Math.max(result.symbolicNameLengthMax, bvi.symbolicName.length());
      result.nameLengthMax          = Math.max(result.nameLengthMax, bvi.name.length());
      result.versionLengthMax       = Math.max(result.versionLengthMax, bvi.version.length());
      result.gitSHALengthMax        = Math.max(result.gitSHALengthMax, bvi.gitSHA.length());
      result.gitDescriptorLengthMax = Math.max(result.gitDescriptorLengthMax, bvi.gitDescriptor.length());
    }

    if (!result.versions.isEmpty()) {
      result.symbolicNameLengthMax  = Math.max(result.symbolicNameLengthMax, Constants.BUNDLE_SYMBOLICNAME.length());
      result.nameLengthMax          = Math.max(result.nameLengthMax, Constants.BUNDLE_NAME.length());
      result.versionLengthMax       = Math.max(result.versionLengthMax, Constants.BUNDLE_VERSION.length());
      result.gitSHALengthMax        = Math.max(result.gitSHALengthMax, HEADER_GIT_SHA.length());
      result.gitDescriptorLengthMax = Math.max(result.gitDescriptorLengthMax, HEADER_GIT_DESCRIPTOR.length());
    }

    return result;
  }

  /**
   * Output a table of version information of all bundles in the framework
   *
   * @param out
   *          the print stream to output the information to
   * @param ctx
   *          a bundle context from which to get all bundles
   */
  public static void showVersions(PrintStream out, BundleContext ctx) {
    RunningBundleVersionsResult result = getRunningBundleVersions(ctx);

    String fmt = //
        "%-" + Integer.toString(result.symbolicNameLengthMax) + "s" // //$NON-NLS-1$ //$NON-NLS-2$
            + "  %-" + Integer.toString(result.nameLengthMax) + "s" // //$NON-NLS-1$ //$NON-NLS-2$
            + "  %-" + Integer.toString(result.versionLengthMax) + "s" // //$NON-NLS-1$ //$NON-NLS-2$
            + "  %-" + Integer.toString(result.gitDescriptorLengthMax) + "s" // //$NON-NLS-1$ //$NON-NLS-2$
            + "  %-" + Integer.toString(result.gitSHALengthMax) + "s" // //$NON-NLS-1$ //$NON-NLS-2$
            + "%n"; //$NON-NLS-1$
    out.printf(fmt, //
        Constants.BUNDLE_SYMBOLICNAME, //
        Constants.BUNDLE_NAME, //
        Constants.BUNDLE_VERSION, //
        HEADER_GIT_DESCRIPTOR, //
        HEADER_GIT_SHA);
    for (BundleVersionInfo vi : result.versions.values()) {
      out.printf(fmt, //
          vi.symbolicName, //
          vi.name, //
          vi.version, //
          vi.gitDescriptor, //
          vi.gitSHA);
    }
  }
}