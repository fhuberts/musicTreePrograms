package nl.pelagic.util.osgi;

import java.util.Map;
import java.util.TreeMap;

/**
 * Container class for running bundles version information
 */
public class RunningBundleVersionsResult {
  /**
   * the maximum length of all symbolicName strings in {@link BundleVersionInfo#symbolicName}
   */
  int symbolicNameLengthMax = 0;

  /**
   * the maximum length of all symbolicName strings in {@link BundleVersionInfo#name}
   */
  int nameLengthMax = 0;

  /**
   * the maximum length of all symbolicName strings in {@link BundleVersionInfo#version}
   */
  int versionLengthMax = 0;

  /**
   * the maximum length of all symbolicName strings in {@link BundleVersionInfo#gitSHA}
   */
  int gitSHALengthMax = 0;

  /**
   * the maximum length of all symbolicName strings in {@link BundleVersionInfo#gitDescriptor}
   */
  int gitDescriptorLengthMax = 0;

  /** a map of bundle symbolic names against their version information */
  Map<String, BundleVersionInfo> versions = new TreeMap<>();
}