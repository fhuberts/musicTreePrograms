package nl.pelagic.audio.tag.checker;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import nl.pelagic.audio.tag.checker.types.AudioTagCheckerConfiguration;
import nl.pelagic.audio.tag.checker.types.CheckProcessFile;
import nl.pelagic.audio.tag.checker.types.CheckProcessInstruction;
import nl.pelagic.audio.tag.checker.types.GenericTag;
import nl.pelagic.audio.tag.checker.types.GenericTagFieldName;
import nl.pelagic.audio.tag.checker.types.ProblemReport;
import nl.pelagic.util.file.FilenameFilterWithRegex;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings({
    "nls", //
    "javadoc", //
    "static-method"
})
public class TestAudioTagCheckerImpl {

  private MyTagConverter               mytc           = null;
  private AudioTagCheckerImpl          atc            = null;
  private MyTagChecker                 mytch          = null;
  private MyTagChecker                 mytchd         = null;
  private MyCheckProcessPlugin         mycpp          = null;
  private MyCheckProcessPlugin         mycppd         = null;
  private MyAudioTagCheckerCallback    callback       = null;
  private AudioTagCheckerConfiguration config         = null;
  private FilenameFilter               filenameFilter = null;

  private String testresourcesFiletest     = "testresources/filetest";
  private String testresourcesDirtest      = "testresources/dirtest";
  private String testresourcesDirtestAllOk = "testresources/dirtest-all-ok";
  private String testresourcesDirtestEmpty = "testresources/dirtest.empty";

  @Before
  public void setUp() throws IOException {
    atc            = new AudioTagCheckerImpl();
    mytc           = new MyTagConverter();
    mytch          = new MyTagChecker();
    mytchd         = new MyTagChecker();
    mytchd.enabled = false;
    mycpp          = new MyCheckProcessPlugin();
    mycppd         = new MyCheckProcessPlugin();
    mycppd.enabled = false;

    atc.tagConverters.add(mytc);
    atc.tagCheckers.add(mytch);
    atc.tagCheckers.add(mytchd);
    atc.checkProcessPlugins.add(mycppd);
    atc.checkProcessPlugins.add(mycpp);

    callback       = new MyAudioTagCheckerCallback();
    config         = new AudioTagCheckerConfiguration();
    filenameFilter = new FilenameFilterWithRegex(null, config.getRegexPattern());

    config.setRecursiveScan(true);

    atc.activate();
  }

  @After
  public void tearDown() {
    atc.deactivate();

    filenameFilter = null;
    config         = null;
    callback       = null;

    atc.checkProcessPlugins.remove(mycpp);
    atc.checkProcessPlugins.remove(mycppd);
    atc.tagCheckers.remove(mytchd);
    atc.tagCheckers.remove(mytch);
    atc.tagConverters.remove(mytc);

    mycpp  = null;
    mycppd = null;
    mytchd = null;
    mytch  = null;
    mytc   = null;
    atc    = null;
  }

  @Test(timeout = 8000)
  public void testProcess_NotRun() {
    atc.shutdownHook();
    atc.process(config, new File(testresourcesDirtest), filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_NonExistingFile() {
    File ed = new File(testresourcesDirtest, "dummynonexistingfile");

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  /*
   * Helpers
   */

  @Test(timeout = 8000)
  public void testProcessRegularFilesExtensions() {
    Map<File, CheckProcessFile> map           = new TreeMap<>();
    List<File>                  filesToRemove = new LinkedList<>();

    atc.processRegularFilesExtensions(map, callback, filesToRemove);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    File             ed  = new File(testresourcesFiletest, "laser.flac");
    CheckProcessFile rfc = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(false);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);

    assertThat(rfc.extension, nullValue());
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);

    assertThat(rfc.extension, equalTo("flac"));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);

    assertThat(rfc.extension, equalTo(""));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.unsupportedExtensions.get(0), equalTo(ed));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.unsupportedExtensionsBatch.get(0), equalTo(ed));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(1)));
    assertThat(filesToRemove.get(0), equalTo(ed));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.mp3333");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);

    assertThat(rfc.extension, equalTo("mp3333"));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.unsupportedExtensions.get(0), equalTo(ed));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.unsupportedExtensionsBatch.get(0), equalTo(ed));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(1)));
    assertThat(filesToRemove.get(0), equalTo(ed));
  }

  @Test(timeout = 8000)
  public void testProcessRegularFilesReadFile() {
    Map<File, CheckProcessFile> map           = new TreeMap<>();
    List<File>                  filesToRemove = new LinkedList<>();

    atc.processRegularFilesReadFile(map, callback, filesToRemove);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    File             ed  = new File(testresourcesFiletest, "laser.flac");
    CheckProcessFile rfc = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(false);

    atc.processRegularFilesReadFile(map, callback, filesToRemove);

    assertThat(rfc.af, nullValue());
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(true);

    atc.processRegularFilesReadFile(map, callback, filesToRemove);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser-not-readable-file.mp33333");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(true);

    atc.processRegularFilesReadFile(map, callback, filesToRemove);

    assertThat(rfc.af, nullValue());
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.notReadables.get(0), equalTo(ed));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.notReadablesBatch.get(ed), notNullValue());
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(1)));
    assertThat(filesToRemove.get(0), equalTo(ed));
  }

  @Test(timeout = 8000)
  public void testProcessRegularFilesReadTag() {
    Map<File, CheckProcessFile> map           = new TreeMap<>();
    List<File>                  filesToRemove = new LinkedList<>();

    atc.processRegularFilesReadTag(map, callback, filesToRemove);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    File             ed  = new File(testresourcesFiletest, "laser.flac");
    CheckProcessFile rfc = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(false);

    atc.processRegularFilesReadTag(map, callback, filesToRemove);

    assertThat(rfc.af, nullValue());
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    atc.processRegularFilesReadTag(map, callback, filesToRemove);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    rfc.af.setTag(null);

    atc.processRegularFilesReadTag(map, callback, filesToRemove);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.noTags.get(0), equalTo(ed));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.noTagsBatch.get(0), equalTo(ed));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(1)));
    assertThat(filesToRemove.get(0), equalTo(ed));
  }

  @Test(timeout = 8000)
  public void testProcessRegularFilesConvertTag() {
    Map<File, CheckProcessFile> map           = new TreeMap<>();
    List<File>                  filesToRemove = new LinkedList<>();

    atc.processRegularFilesConvertTag(map, callback, filesToRemove);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    File             ed  = new File(testresourcesFiletest, "laser.flac");
    CheckProcessFile rfc = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(false);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    atc.processRegularFilesConvertTag(map, callback, filesToRemove);

    assertThat(rfc.af, nullValue());
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    atc.processRegularFilesConvertTag(map, callback, filesToRemove);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.tagNotConverteds.get(0), equalTo(ed));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.tagNotConvertedsBatch.get(ed), notNullValue());
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(1)));
    assertThat(filesToRemove.get(0), equalTo(ed));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    mytc.retval = true;
    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    atc.processRegularFilesConvertTag(map, callback, filesToRemove);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();
    mytc.stcs.clear();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    mytc.retval = true;
    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    atc.processRegularFilesConvertTag(map, callback, filesToRemove);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.tagNotConverteds.get(0), equalTo(ed));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.tagNotConvertedsBatch.get(ed), notNullValue());
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(1)));
    assertThat(filesToRemove.get(0), equalTo(ed));
  }

  @Test(timeout = 8000)
  public void testProcessRegularFilesCheckTag() {
    AudioTagCheckerConfiguration configuration = new AudioTagCheckerConfiguration();
    Map<File, CheckProcessFile>  map           = new TreeMap<>();
    List<File>                   filesToRemove = new LinkedList<>();

    atc.processRegularFilesCheckTag(configuration, map, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    File             ed  = new File(testresourcesFiletest, "laser.flac");
    CheckProcessFile rfc = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    mytc.retval = true;
    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesConvertTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    atc.run.set(false);
    atc.processRegularFilesCheckTag(configuration, map, callback);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    mytc.retval = true;
    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesConvertTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    atc.processRegularFilesCheckTag(configuration, map, callback);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPasseds.get(0), equalTo(rfc.genericTag));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPassedsBatch.get(0), equalTo(rfc.genericTag));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    Set<String> disabledCheckers = new HashSet<>();
    disabledCheckers.add(MyTagChecker.class.getName());
    configuration.setDisabledTagCheckers(disabledCheckers);
    mytc.retval    = true;
    mytchd.enabled = true;
    mytchd.key     = GenericTagFieldName.ALBUMTITLE;
    List<ProblemReport> reports         = new LinkedList<>();
    List<Integer>       positionMarkers = new LinkedList<>();
    positionMarkers.add(Integer.valueOf(1));
    ProblemReport report = new ProblemReport("message", "expectedValue", "actualValue", positionMarkers);
    reports.add(report);
    mytchd.value = reports;
    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesConvertTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    atc.processRegularFilesCheckTag(configuration, map, callback);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPasseds.get(0), equalTo(rfc.genericTag));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPassedsBatch.get(0), equalTo(rfc.genericTag));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    disabledCheckers = new HashSet<>();
    configuration.setDisabledTagCheckers(disabledCheckers);
    mytc.retval     = true;
    mytch.enabled   = false;
    mytchd.enabled  = true;
    mytchd.key      = GenericTagFieldName.ALBUMTITLE;
    reports         = new LinkedList<>();
    positionMarkers = new LinkedList<>();
    positionMarkers.add(Integer.valueOf(1));
    report = new ProblemReport("message", "expectedValue", "actualValue", positionMarkers);
    reports.add(report);
    mytchd.value = reports;
    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesConvertTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    atc.processRegularFilesCheckTag(configuration, map, callback);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksFaileds.get(0), equalTo(rfc.genericTag));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksFailedsBatch.get(0), equalTo(rfc.genericTag));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));

    map.clear();
    filesToRemove.clear();
    callback.reset();

    ed       = new File(testresourcesFiletest, "laser.flac");
    rfc      = new CheckProcessFile();
    rfc.file = ed;
    map.put(ed, rfc);

    disabledCheckers = new HashSet<>();
    configuration.setDisabledTagCheckers(disabledCheckers);
    Set<String> enabledCheckers = new HashSet<>();
    enabledCheckers.add("non.existing.tag.checker");
    configuration.setEnabledTagCheckers(enabledCheckers);
    mytc.retval    = true;
    mytch.enabled  = true;
    mytchd.enabled = true;
    mytchd.key     = null;
    mytchd.value   = null;
    atc.run.set(true);

    atc.processRegularFilesExtensions(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadFile(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesReadTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));
    atc.processRegularFilesConvertTag(map, callback, filesToRemove);
    assertThat(Integer.valueOf(filesToRemove.size()), equalTo(Integer.valueOf(0)));

    atc.processRegularFilesCheckTag(configuration, map, callback);

    assertThat(rfc.af, notNullValue());
    assertThat(rfc.af.getFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPasseds.get(0), equalTo(rfc.genericTag));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPassedsBatch.get(0), equalTo(rfc.genericTag));
  }

  /*
   * Files
   */

  @Test(timeout = 8000)
  public void testProcess_FilesEntryErrors() {
    File ed = new File(testresourcesFiletest, "laser");

    mycpp.filesEntryErrorsExit.add(ed);

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_UnsupportedExtension1() {
    File ed = new File(testresourcesFiletest, "laser");

    mycpp.filesExtensionsErrors.add(ed);

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(callback.unsupportedExtensions.contains(ed)), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(callback.unsupportedExtensionsBatch.contains(ed)), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_UnsupportedExtension2() {
    File ed = new File(testresourcesFiletest, "laser.mp333");

    mycpp.filesExtensionsErrors.add(ed);

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(callback.unsupportedExtensions.contains(ed)), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(callback.unsupportedExtensionsBatch.contains(ed)), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_Unreadable() {
    File ed = new File(testresourcesFiletest, "laser_zero_size.flac");

    mycpp.filesReadErrors.add(ed);

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(callback.notReadables.contains(ed)), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.notReadablesBatch.get(ed), notNullValue());
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_NotConvertedMp3() {
    File ed = new File(testresourcesFiletest, "laser.mp3");

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(callback.tagNotConverteds.contains(ed)), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.tagNotConvertedsBatch.get(ed), notNullValue());
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_NotConvertedFlac() {
    File ed = new File(testresourcesFiletest, "laser.flac");

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(callback.tagNotConverteds.contains(ed)), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.tagNotConvertedsBatch.get(ed), notNullValue());
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_CheckerDisabled() {
    Set<String> tcs = new HashSet<>();
    tcs.add(mytch.getClass().getSimpleName());
    config.setDisabledTagCheckers(tcs);
    mytc.retval = true;
    File ed = new File(testresourcesFiletest, "laser.flac");

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPasseds.get(0).getBackingFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPassedsBatch.get(0).getBackingFile(), equalTo(ed));
  }

  @Test(timeout = 8000)
  public void testProcess_CheckerNotEnabled() {
    Set<String> tcs = new HashSet<>();
    tcs.add("somedummycheckerclass");
    config.setEnabledTagCheckers(tcs);
    mytc.retval = true;
    File ed = new File(testresourcesFiletest, "laser.flac");

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPasseds.get(0).getBackingFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPassedsBatch.get(0).getBackingFile(), equalTo(ed));
  }

  @Test(timeout = 8000)
  public void testProcess_CheckFailed() {
    mytc.retval = true;
    mytch.key   = GenericTagFieldName.FILE;
    mytch.value = new LinkedList<>();
    List<Integer> positionMarkers = new LinkedList<>();
    ProblemReport pr              = new ProblemReport("message", "expectedValue", "actualValue", positionMarkers);
    mytch.value.add(pr);

    File ed = new File(testresourcesFiletest, "laser.flac");

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(1)));
    GenericTag gt = callback.checksFaileds.get(0);
    assertThat(gt.getBackingFile(), equalTo(ed));
    Map<GenericTagFieldName, List<ProblemReport>> prs = gt.getReports();
    assertThat(prs, notNullValue());
    assertThat(Integer.valueOf(prs.size()), equalTo(Integer.valueOf(1)));
    List<ProblemReport> pr1 = prs.get(GenericTagFieldName.FILE);
    assertThat(pr1, notNullValue());
    assertThat(Integer.valueOf(pr1.size()), equalTo(Integer.valueOf(1)));
    ProblemReport pr11 = pr1.get(0);
    assertThat(pr11, notNullValue());
    assertThat(pr11.getActualValue(), equalTo(pr.getActualValue()));
    assertThat(pr11.getExpectedValue(), equalTo(pr.getExpectedValue()));
    assertThat(pr11.getMessage(), equalTo(pr.getMessage()));
    assertThat(pr11.getPositionMarkers(), equalTo(pr.getPositionMarkers()));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(1)));
    gt = callback.checksFailedsBatch.get(0);
    assertThat(gt.getBackingFile(), equalTo(ed));
    prs = gt.getReports();
    assertThat(prs, notNullValue());
    assertThat(Integer.valueOf(prs.size()), equalTo(Integer.valueOf(1)));
    pr1 = prs.get(GenericTagFieldName.FILE);
    assertThat(pr1, notNullValue());
    assertThat(Integer.valueOf(pr1.size()), equalTo(Integer.valueOf(1)));
    pr11 = pr1.get(0);
    assertThat(pr11, notNullValue());
    assertThat(pr11.getActualValue(), equalTo(pr.getActualValue()));
    assertThat(pr11.getExpectedValue(), equalTo(pr.getExpectedValue()));
    assertThat(pr11.getMessage(), equalTo(pr.getMessage()));
    assertThat(pr11.getPositionMarkers(), equalTo(pr.getPositionMarkers()));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_CheckPassed() {
    mytc.retval = true;

    File ed = new File(testresourcesFiletest, "laser.flac");

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPasseds.get(0).getBackingFile(), equalTo(ed));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(1)));
    assertThat(callback.checksPassedsBatch.get(0).getBackingFile(), equalTo(ed));
  }

  /*
   * Dirs
   */

  @Test(timeout = 8000)
  public void testProcess_NotRecursive1() {
    config.setRecursiveScan(false);
    atc.process(config, new File(testresourcesDirtest), filenameFilter, false, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_NotRecursive2() {
    config.setRecursiveScan(true);
    atc.process(config, new File(testresourcesDirtest), filenameFilter, false, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_NotRecursive3() {
    config.setRecursiveScan(false);
    atc.process(config, new File(testresourcesDirtest), filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_Recursive() {
    config.setRecursiveScan(true);
    atc.process(config, new File(testresourcesDirtest), filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(3)));
    assertThat(Boolean.valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "subdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(
        Boolean
            .valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "subdir/subsubdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(3)));
    assertThat(
        Boolean.valueOf(callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(
        Boolean
            .valueOf(callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "subdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(
        Boolean.valueOf(
            callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "subdir/subsubdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_Recursive_Empty_Dir() throws IOException {
    config.setRecursiveScan(true);
    File           base               = null;
    Pattern        pattern            = Pattern.compile("only-accept-this-file-so-none-really");
    FilenameFilter filenameFilterNone = new FilenameFilterWithRegex(base, pattern);
    atc.process(config, new File(testresourcesDirtestEmpty), filenameFilterNone, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_Recursive_Aborted() {
    callback.atc                        = atc;
    callback.shutdownAfterNumberOfCalls = 2;
    config.setRecursiveScan(true);
    atc.process(config, new File(testresourcesDirtest), filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(callback.tagNotConverteds.toString(), Integer.valueOf(callback.tagNotConverteds.size()),
        equalTo(Integer.valueOf(2)));
    assertThat(Boolean.valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "subdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(2)));
    assertThat(
        Boolean.valueOf(callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(
        Boolean
            .valueOf(callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "subdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testProcess_EmptyDirectory() {
    File ed = new File(testresourcesDirtest, "dummyemptydirectory");
    ed.mkdirs();
    ed.deleteOnExit();

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testCheck_Nulls() throws IOException {
    boolean r = atc.check(null, null);
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));

    r = atc.check(config, null);
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));
  }

  @Test(timeout = 8000)
  public void testCheck_ScanPath_Problems() throws IOException {
    boolean r = atc.check(config, callback);
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));

    config.setCheckPath(new File(testresourcesDirtest, "somepaththatdoesntexist"));
    r = atc.check(config, callback);
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));
  }

  @Test(timeout = 8000)
  public void testCheck_Recursive1() throws IOException {
    config.setRecursiveScan(true);
    config.setCheckPath(new File(testresourcesDirtest));

    boolean r = atc.check(config, callback);
    assertThat(Boolean.valueOf(r), equalTo(Boolean.TRUE));

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(3)));
    assertThat(Boolean.valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "subdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(
        Boolean
            .valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "subdir/subsubdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(3)));
    assertThat(
        Boolean.valueOf(callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(
        Boolean
            .valueOf(callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "subdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(
        Boolean.valueOf(
            callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "subdir/subsubdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testCheck_Recursive2() throws IOException {
    config.setRecursiveScan(true);
    config.setRegexInAllDirs(true);
    config.setCheckPath(new File(testresourcesDirtest));

    boolean r = atc.check(config, callback);
    assertThat(Boolean.valueOf(r), equalTo(Boolean.TRUE));

    assertThat(Integer.valueOf(callback.unsupportedExtensions.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadables.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTags.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConverteds.size()), equalTo(Integer.valueOf(3)));
    assertThat(Boolean.valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "subdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(
        Boolean
            .valueOf(callback.tagNotConverteds.contains(new File(testresourcesDirtest, "subdir/subsubdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.checksFaileds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPasseds.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.unsupportedExtensionsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.notReadablesBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.noTagsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.tagNotConvertedsBatch.size()), equalTo(Integer.valueOf(3)));
    assertThat(
        Boolean.valueOf(callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(
        Boolean
            .valueOf(callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "subdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(
        Boolean.valueOf(
            callback.tagNotConvertedsBatch.containsKey(new File(testresourcesDirtest, "subdir/subsubdir/laser.flac"))),
        equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(callback.checksFailedsBatch.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(callback.checksPassedsBatch.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testIsTagCheckerEnabled() {
    AudioTagCheckerConfiguration configuration = new AudioTagCheckerConfiguration();
    MyTagChecker                 tagChecker    = new MyTagChecker();

    HashSet<String> disabledTagCheckers = new HashSet<>();
    HashSet<String> enabledTagCheckers  = new HashSet<>();
    configuration.setDisabledTagCheckers(disabledTagCheckers);
    configuration.setEnabledTagCheckers(enabledTagCheckers);

    /**
     * <pre>
     * i  disabled  enabled  en  isTagCheckerEnabled
     * 0        0        0   0  false
     * 1        0        0   1  true
     * 2        0        1   0  true
     * 3        0        1   1  true
     * 4        1        0   0  false
     * 5        1        0   1  false
     * 6        1        1   0  false
     * 7        1        1   1  false
     * </pre>
     */

    boolean results[] = { //
        false, //
        true, //
        true, //
        true, //
        false, //
        false, //
        false, //
        false //
    };

    for (int i = 0; i < results.length; i++) {
      disabledTagCheckers.clear();
      enabledTagCheckers.clear();

      if ((i & 0x04) != 0) {
        disabledTagCheckers.add(tagChecker.getClass().getName());
      }
      if ((i & 0x02) != 0) {
        enabledTagCheckers.add(tagChecker.getClass().getName());
      }
      tagChecker.enabled = ((i & 0x01) != 0);

      assertThat(Integer.toString(i),
          Boolean.valueOf(AudioTagCheckerImpl.isTagCheckerEnabled(configuration, tagChecker)),
          equalTo(Boolean.valueOf(results[i])));
    }
  }

  @Test(timeout = 8000)
  public void testIsCheckProcessPluginEnabled() {
    AudioTagCheckerConfiguration configuration      = new AudioTagCheckerConfiguration();
    MyCheckProcessPlugin         checkProcessPlugin = new MyCheckProcessPlugin();

    HashSet<String> disabledCheckProcessPlugins = new HashSet<>();
    HashSet<String> enabledCheckProcessPlugins  = new HashSet<>();
    configuration.setDisabledCheckProcessPlugins(disabledCheckProcessPlugins);
    configuration.setEnabledCheckProcessPlugins(enabledCheckProcessPlugins);

    /**
     * <pre>
     * i  disabled  enabled  en  isCheckProcessPluginEnabled
     * 0        0        0   0  false
     * 1        0        0   1  true
     * 2        0        1   0  true
     * 3        0        1   1  true
     * 4        1        0   0  false
     * 5        1        0   1  false
     * 6        1        1   0  false
     * 7        1        1   1  false
     * </pre>
     */

    boolean results[] = { //
        false, //
        true, //
        true, //
        true, //
        false, //
        false, //
        false, //
        false //
    };

    for (int i = 0; i < results.length; i++) {
      disabledCheckProcessPlugins.clear();
      enabledCheckProcessPlugins.clear();

      if ((i & 0x04) != 0) {
        disabledCheckProcessPlugins.add(checkProcessPlugin.getClass().getName());
      }
      if ((i & 0x02) != 0) {
        enabledCheckProcessPlugins.add(checkProcessPlugin.getClass().getName());
      }
      checkProcessPlugin.enabled = ((i & 0x01) != 0);

      assertThat(Integer.toString(i),
          Boolean.valueOf(AudioTagCheckerImpl.isCheckProcessPluginEnabled(configuration, checkProcessPlugin)),
          equalTo(Boolean.valueOf(results[i])));
    }
  }

  @Test(timeout = 8000)
  public void testProcess_Redo_None() {
    File ed = new File(testresourcesDirtestAllOk, "subdir/subsubdir");

    mytc.retval = true;

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(mycpp.directoryEntryReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesEntryReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesExtensionsReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesReadReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesTagsReadReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesTagsConvertedReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesExitReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.directoryExitReturnsIndex), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testProcess_Redo_FilesEntry() {
    File ed = new File(testresourcesDirtestAllOk, "subdir/subsubdir");

    mycpp.filesEntryReturns[0] = CheckProcessInstruction.RESTART_DIRECTORY;
    mytc.retval                = true;

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(mycpp.directoryEntryReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesEntryReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesExtensionsReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesReadReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesTagsReadReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesTagsConvertedReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesExitReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.directoryExitReturnsIndex), equalTo(Integer.valueOf(2)));
  }

  @Test(timeout = 8000)
  public void testProcess_Redo_FilesExtensions() {
    File ed = new File(testresourcesDirtestAllOk, "subdir/subsubdir");

    mycpp.filesExtensionsReturns[0] = CheckProcessInstruction.RESTART_DIRECTORY;
    mytc.retval                     = true;

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(mycpp.directoryEntryReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesEntryReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesExtensionsReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesReadReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesTagsReadReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesTagsConvertedReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesExitReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.directoryExitReturnsIndex), equalTo(Integer.valueOf(2)));
  }

  @Test(timeout = 8000)
  public void testProcess_Redo_FilesRead() {
    File ed = new File(testresourcesDirtestAllOk, "subdir/subsubdir");

    mycpp.filesReadReturns[0] = CheckProcessInstruction.RESTART_DIRECTORY;
    mytc.retval               = true;

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(mycpp.directoryEntryReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesEntryReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesExtensionsReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesReadReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesTagsReadReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesTagsConvertedReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesExitReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.directoryExitReturnsIndex), equalTo(Integer.valueOf(2)));
  }

  @Test(timeout = 8000)
  public void testProcess_Redo_FilesTagsRead() {
    File ed = new File(testresourcesDirtestAllOk, "subdir/subsubdir");

    mycpp.filesTagsReadReturns[0] = CheckProcessInstruction.RESTART_DIRECTORY;
    mytc.retval                   = true;

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(mycpp.directoryEntryReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesEntryReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesExtensionsReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesReadReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesTagsReadReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesTagsConvertedReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesExitReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.directoryExitReturnsIndex), equalTo(Integer.valueOf(2)));
  }

  @Test(timeout = 8000)
  public void testProcess_Redo_FilesTagsConverted() {
    File ed = new File(testresourcesDirtestAllOk, "subdir/subsubdir");

    mycpp.filesTagsConvertedReturns[0] = CheckProcessInstruction.RESTART_DIRECTORY;
    mytc.retval                        = true;

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(mycpp.directoryEntryReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesEntryReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesExtensionsReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesReadReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesTagsReadReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesTagsConvertedReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.filesExitReturnsIndex), equalTo(Integer.valueOf(2)));
    assertThat(Integer.valueOf(mycpp.directoryExitReturnsIndex), equalTo(Integer.valueOf(2)));
  }

  @Test(timeout = 8000)
  public void testProcess_ReadTagFailed() {
    File ed = new File(testresourcesDirtestAllOk, "subdir/subsubdir");

    mytc.retval                   = true;
    mycpp.filesTagsReadFillErrors = true;

    atc.process(config, ed, filenameFilter, true, callback);

    assertThat(Integer.valueOf(mycpp.directoryEntryReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesEntryReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesExtensionsReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesReadReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesTagsReadReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.filesTagsConvertedReturnsIndex), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(mycpp.filesExitReturnsIndex), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(mycpp.directoryExitReturnsIndex), equalTo(Integer.valueOf(1)));
  }
}
