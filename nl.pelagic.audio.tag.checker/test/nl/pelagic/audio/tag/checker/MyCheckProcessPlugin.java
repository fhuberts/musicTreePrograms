package nl.pelagic.audio.tag.checker;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.pelagic.audio.tag.checker.api.CheckProcessPlugin;
import nl.pelagic.audio.tag.checker.types.CheckProcessFile;
import nl.pelagic.audio.tag.checker.types.CheckProcessInstruction;
import nl.pelagic.audio.tag.checker.types.CheckProcessReports;

import org.junit.Ignore;

@Ignore
@SuppressWarnings("javadoc")
public class MyCheckProcessPlugin implements CheckProcessPlugin {

  public enum State { //
    INIT, //
    DIRECTORYENTRY, //
    FILESENTRY, //
    FILESEXTENSIONS, //
    FILESREAD, //
    FILESTAGSREAD, //
    FILESTAGSCONVERTED, //
    FILESEXIT, //
    DIRECTORYEXIT //
  }

  public MyCheckProcessPlugin() {
    reset();
  }

  public State state = State.INIT;

  public boolean doChecks = false;

  public boolean enabled = true;

  public File directoryEntered = null;

  public static final int RETURN_COUNT = 6;

  public int directoryEntryReturnsIndex = 0;

  public int                       directoryExitReturnsIndex = 0;
  public CheckProcessInstruction[] directoryExitReturns      = new CheckProcessInstruction[RETURN_COUNT];

  public List<File>                filesEntryErrorsEntry  = new LinkedList<>();
  public List<File>                filesEntryErrorsExit   = new LinkedList<>();
  public int                       filesEntryReturnsIndex = 0;
  public CheckProcessInstruction[] filesEntryReturns      = new CheckProcessInstruction[RETURN_COUNT];

  public List<File>                filesExtensionsErrors       = new LinkedList<>();
  public int                       filesExtensionsReturnsIndex = 0;
  public CheckProcessInstruction[] filesExtensionsReturns      = new CheckProcessInstruction[RETURN_COUNT];

  public List<File>                filesReadErrors       = new LinkedList<>();
  public int                       filesReadReturnsIndex = 0;
  public CheckProcessInstruction[] filesReadReturns      = new CheckProcessInstruction[RETURN_COUNT];

  public List<File>                filesTagsReadErrors       = new LinkedList<>();
  public boolean                   filesTagsReadFillErrors   = false;
  public int                       filesTagsReadReturnsIndex = 0;
  public CheckProcessInstruction[] filesTagsReadReturns      = new CheckProcessInstruction[RETURN_COUNT];

  public List<File>                filesTagsConvertedErrors       = new LinkedList<>();
  public int                       filesTagsConvertedReturnsIndex = 0;
  public CheckProcessInstruction[] filesTagsConvertedReturns      = new CheckProcessInstruction[RETURN_COUNT];

  public int                       filesExitReturnsIndex = 0;
  public CheckProcessInstruction[] filesExitReturns      = new CheckProcessInstruction[RETURN_COUNT];

  public void reset() {
    state                      = State.INIT;
    enabled                    = true;
    directoryEntered           = null;
    directoryEntryReturnsIndex = 0;
    directoryExitReturnsIndex  = 0;
    filesEntryReturnsIndex     = 0;
    filesExtensionsErrors.clear();
    filesExtensionsReturnsIndex = 0;
    filesReadErrors.clear();
    filesReadReturnsIndex = 0;
    filesTagsReadErrors.clear();
    filesTagsReadFillErrors   = false;
    filesTagsReadReturnsIndex = 0;
    filesTagsConvertedErrors.clear();
    filesTagsConvertedReturnsIndex = 0;
    filesExitReturnsIndex          = 0;
    for (int i = 0; i < RETURN_COUNT; i++) {
      directoryExitReturns[i]      = CheckProcessInstruction.CONTINUE;
      filesEntryReturns[i]         = CheckProcessInstruction.CONTINUE;
      filesExtensionsReturns[i]    = CheckProcessInstruction.CONTINUE;
      filesReadReturns[i]          = CheckProcessInstruction.CONTINUE;
      filesTagsReadReturns[i]      = CheckProcessInstruction.CONTINUE;
      filesTagsConvertedReturns[i] = CheckProcessInstruction.CONTINUE;
      filesExitReturns[i]          = CheckProcessInstruction.CONTINUE;
    }
  }

  @Override
  public boolean isEnabledByDefault() {
    return enabled;
  }

  void checkState(Map<File, CheckProcessFile> map) {
    if (!doChecks || (map == null)) {
      return;
    }

    switch (state) {
      case FILESENTRY:
      case FILESEXTENSIONS:
      case FILESREAD:
      case FILESTAGSREAD:
      case FILESTAGSCONVERTED:
      case FILESEXIT:
        for (Entry<File, CheckProcessFile> entry : map.entrySet()) {
          File             f  = entry.getKey();
          CheckProcessFile fl = entry.getValue();

          assertThat(f, notNullValue());
          assertThat(fl, notNullValue());

          // file
          assertThat(fl.file.getName(), f, equalTo(fl.file));

          // extension
          switch (state) {
            case FILESENTRY:
              assertThat(fl.file.getName(), fl.extension, nullValue());
              break;

            case FILESEXTENSIONS:
            case FILESREAD:
            case FILESTAGSREAD:
            case FILESTAGSCONVERTED:
            case FILESEXIT:
              assertThat(fl.file.getName(), fl.extension, notNullValue());
              break;

            default:
              assert (false);
              break;
          }

          // af
          switch (state) {
            case FILESENTRY:
            case FILESEXTENSIONS:
              assertThat(fl.file.getName(), fl.af, nullValue());
              break;

            case FILESREAD:
            case FILESTAGSREAD:
            case FILESTAGSCONVERTED:
            case FILESEXIT:
              if (fl.file.length() == 0) {
                assertThat(fl.file.getName(), fl.af, nullValue());
              } else {
                assertThat(fl.file.getName(), fl.af, notNullValue());
              }
              break;

            default:
              assert (false);
              break;
          }

          // tag
          switch (state) {
            case FILESENTRY:
            case FILESEXTENSIONS:
            case FILESREAD:
              assertThat(fl.file.getName(), fl.tag, nullValue());
              break;

            case FILESTAGSREAD:
            case FILESTAGSCONVERTED:
            case FILESEXIT:
              assertThat(fl.file.getName(), fl.tag, notNullValue());
              break;

            default:
              assert (false);
              break;
          }

          // genericTag
          switch (state) {
            case FILESENTRY:
            case FILESEXTENSIONS:
            case FILESREAD:
            case FILESTAGSREAD:
              assertThat(fl.file.getName(), fl.genericTag, nullValue());
              break;

            case FILESTAGSCONVERTED:
            case FILESEXIT:
              assertThat(fl.file.getName(), fl.genericTag, notNullValue());
              break;

            default:
              assert (false);
              break;
          }
        }
        break;

      default:
        break;
    }
  }

  @Override
  public void directoryEntry(File directory, List<File> directories, List<File> regularFiles) {
    assertThat(state, equalTo(State.INIT));
    state = State.DIRECTORYENTRY;

    assertThat(directoryEntered, nullValue());
    directoryEntered = directory;

    assertThat(directory, notNullValue());
    assertThat(Boolean.valueOf(directory.exists()), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(directory.isDirectory()), equalTo(Boolean.TRUE));
    assertThat(directories, notNullValue());
    assertThat(regularFiles, notNullValue());

    if (doChecks) {
      for (File dir : directories) {
        assertThat(Boolean.valueOf(dir.exists()), equalTo(Boolean.TRUE));
        assertThat(Boolean.valueOf(dir.isDirectory()), equalTo(Boolean.TRUE));
      }

      for (File regularFile : regularFiles) {
        assertThat(Boolean.valueOf(regularFile.exists()), equalTo(Boolean.TRUE));
        assertThat(Boolean.valueOf(regularFile.isDirectory()), equalTo(Boolean.FALSE));
      }
    }

    checkState(null);

    directoryEntryReturnsIndex++;
  }

  @Override
  public CheckProcessInstruction directoryExit(File directory) {
    assertThat(state, anyOf(equalTo(State.DIRECTORYENTRY), equalTo(State.FILESEXIT)));
    state = State.DIRECTORYENTRY;

    assertThat(directory, notNullValue());
    assertThat(Boolean.valueOf(directory.exists()), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(directory.isDirectory()), equalTo(Boolean.TRUE));

    assertThat(directoryEntered, notNullValue());
    assertThat(directory, equalTo(directoryEntered));
    directoryEntered = null;

    checkState(null);

    state = State.INIT;

    return directoryExitReturns[directoryExitReturnsIndex++];
  }

  @Override
  public CheckProcessInstruction filesEntry(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    assertThat(state, equalTo(State.DIRECTORYENTRY));
    state = State.FILESENTRY;

    assertThat(map, notNullValue());
    assertThat(Boolean.valueOf(map.isEmpty()), equalTo(Boolean.FALSE));

    assertThat(errors, notNullValue());
    assertThat(errors, equalTo(filesEntryErrorsEntry));

    checkState(map);

    errors.addAll(filesEntryErrorsExit);

    return filesEntryReturns[filesEntryReturnsIndex++];
  }

  @Override
  public CheckProcessInstruction filesExtensions(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    assertThat(state, equalTo(State.FILESENTRY));
    state = State.FILESEXTENSIONS;

    assertThat(map, notNullValue());
    assertThat(Boolean.valueOf(map.isEmpty()), equalTo(Boolean.FALSE));

    assertThat(errors, notNullValue());
    assertThat(errors, equalTo(filesExtensionsErrors));

    checkState(map);

    return filesExtensionsReturns[filesExtensionsReturnsIndex++];
  }

  @Override
  public CheckProcessInstruction filesRead(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    assertThat(state, equalTo(State.FILESEXTENSIONS));
    state = State.FILESREAD;

    assertThat(map, notNullValue());
    assertThat(Boolean.valueOf(map.isEmpty()), equalTo(Boolean.FALSE));

    assertThat(errors, notNullValue());
    assertThat(errors, equalTo(filesReadErrors));

    checkState(map);

    return filesReadReturns[filesReadReturnsIndex++];
  }

  @Override
  public CheckProcessInstruction filesTagsRead(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    assertThat(state, equalTo(State.FILESREAD));
    state = State.FILESTAGSREAD;

    assertThat(map, notNullValue());
    assertThat(Boolean.valueOf(map.isEmpty()), equalTo(Boolean.FALSE));

    assertThat(errors, notNullValue());
    assertThat(errors, equalTo(filesTagsReadErrors));

    checkState(map);

    CheckProcessInstruction r = filesTagsReadReturns[filesTagsReadReturnsIndex++];
    if (filesTagsReadFillErrors) {
      if (map.size() != 0) {
        for (File f : map.keySet()) {
          errors.add(f);
        }
      }
    }

    return r;
  }

  @Override
  public CheckProcessInstruction filesTagsConverted(Map<File, CheckProcessFile> map, List<File> errors,
      Map<File, CheckProcessReports> reports) {
    assertThat(state, equalTo(State.FILESTAGSREAD));
    state = State.FILESTAGSCONVERTED;

    assertThat(map, notNullValue());
    assertThat(Boolean.valueOf(map.isEmpty()), equalTo(Boolean.FALSE));

    assertThat(errors, notNullValue());
    if (doChecks) {
      assertThat(errors, equalTo(filesTagsConvertedErrors));
    }

    checkState(map);

    return filesTagsConvertedReturns[filesTagsConvertedReturnsIndex++];
  }

  @Override
  public CheckProcessInstruction filesExit(Map<File, CheckProcessFile> map, Map<File, CheckProcessReports> reports) {
    assertThat(state, anyOf(equalTo(State.FILESENTRY), equalTo(State.FILESEXTENSIONS), equalTo(State.FILESREAD),
        equalTo(State.FILESTAGSREAD), equalTo(State.FILESTAGSCONVERTED)));
    state = State.FILESEXIT;

    assertThat(map, notNullValue());

    checkState(map);

    return filesExitReturns[filesExitReturnsIndex++];
  }
}