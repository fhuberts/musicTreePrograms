package nl.pelagic.audio.tag.checker;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import nl.pelagic.audio.tag.checker.api.AudioTagCheckerCallback;
import nl.pelagic.audio.tag.checker.types.CheckProcessReports;
import nl.pelagic.audio.tag.checker.types.GenericTag;
import nl.pelagic.shutdownhook.api.ShutdownHookParticipant;

import org.jaudiotagger.tag.Tag;
import org.junit.Ignore;

@Ignore
@SuppressWarnings("javadoc")
public class MyAudioTagCheckerCallback implements AudioTagCheckerCallback {
  public List<File>       unsupportedExtensions = new LinkedList<>();
  public List<File>       notReadables          = new LinkedList<>();
  public List<File>       noTags                = new LinkedList<>();
  public List<File>       tagNotConverteds      = new LinkedList<>();
  public List<GenericTag> checksFaileds         = new LinkedList<>();
  public List<GenericTag> checksPasseds         = new LinkedList<>();

  public List<File>           unsupportedExtensionsBatch = new LinkedList<>();
  public Map<File, Exception> notReadablesBatch          = new HashMap<>();
  public List<File>           noTagsBatch                = new LinkedList<>();
  public Map<File, Tag>       tagNotConvertedsBatch      = new HashMap<>();
  public List<GenericTag>     checksFailedsBatch         = new LinkedList<>();
  public List<GenericTag>     checksPassedsBatch         = new LinkedList<>();

  public Map<File, CheckProcessReports> reports = new TreeMap<>();

  public ShutdownHookParticipant atc                             = null;
  public int                     shutdownAfterNumberOfCalls      = 0;
  public int                     shutdownAfterNumberOfCallsBatch = 0;

  public void reset() {
    unsupportedExtensions.clear();
    notReadables.clear();
    noTags.clear();
    tagNotConverteds.clear();
    checksFaileds.clear();
    checksPasseds.clear();

    unsupportedExtensionsBatch.clear();
    notReadablesBatch.clear();
    noTagsBatch.clear();
    tagNotConvertedsBatch.clear();
    checksFailedsBatch.clear();
    checksPassedsBatch.clear();

    reports.clear();
  }

  @Override
  public void unsupportedExtension(File file) {
    unsupportedExtensions.add(file);
  }

  @Override
  public void notReadable(File file, Exception e) {
    notReadables.add(file);
  }

  @Override
  public void noTag(File file) {
    noTags.add(file);
  }

  @Override
  public void tagNotConverted(File file, Tag tag) {
    tagNotConverteds.add(file);
    if ((atc != null) && (shutdownAfterNumberOfCalls > 0)) {
      shutdownAfterNumberOfCalls--;
      if (shutdownAfterNumberOfCalls <= 0) {
        atc.shutdownHook();
      }
    }
  }

  @Override
  public void checksFailed(GenericTag tag) {
    checksFaileds.add(tag);
  }

  @Override
  public void checksPassed(GenericTag tag) {
    checksPasseds.add(tag);
  }

  /*
   * Batch Interface
   */

  @Override
  public void unsupportedExtension(List<File> files) {
    unsupportedExtensionsBatch.addAll(files);
  }

  @Override
  public void notReadable(Map<File, Exception> files) {
    notReadablesBatch.putAll(files);
  }

  @Override
  public void noTag(List<File> files) {
    noTagsBatch.addAll(files);
  }

  @Override
  public void tagNotConverted(Map<File, Tag> files) {
    tagNotConvertedsBatch.putAll(files);
    if ((atc != null) && (shutdownAfterNumberOfCallsBatch > 0)) {
      shutdownAfterNumberOfCallsBatch--;
      if (shutdownAfterNumberOfCallsBatch <= 0) {
        atc.shutdownHook();
      }
    }
  }

  @Override
  public void checksFailed(List<GenericTag> tags) {
    checksFailedsBatch.addAll(tags);
  }

  @Override
  public void checksPassed(List<GenericTag> tags) {
    checksPassedsBatch.addAll(tags);
  }

  @Override
  public void report(Map<File, CheckProcessReports> reports) {
    this.reports.putAll(reports);
  }
}
