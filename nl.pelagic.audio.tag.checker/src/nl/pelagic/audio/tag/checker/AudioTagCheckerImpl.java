package nl.pelagic.audio.tag.checker;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;

import nl.pelagic.audio.tag.checker.api.AudioTagChecker;
import nl.pelagic.audio.tag.checker.api.AudioTagCheckerCallback;
import nl.pelagic.audio.tag.checker.api.CheckProcessPlugin;
import nl.pelagic.audio.tag.checker.api.TagChecker;
import nl.pelagic.audio.tag.checker.api.TagConverter;
import nl.pelagic.audio.tag.checker.types.AudioTagCheckerConfiguration;
import nl.pelagic.audio.tag.checker.types.CheckProcessFile;
import nl.pelagic.audio.tag.checker.types.CheckProcessInstruction;
import nl.pelagic.audio.tag.checker.types.CheckProcessReports;
import nl.pelagic.audio.tag.checker.types.GenericTag;
import nl.pelagic.shutdownhook.api.ShutdownHookParticipant;
import nl.pelagic.util.file.ExtensionUtils;
import nl.pelagic.util.file.FilenameFilterWithRegex;

import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.SupportedFileFormat;
import org.jaudiotagger.tag.Tag;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.FieldOption;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;

/**
 * This bundle performs checks on the tags in music files.
 *
 * The actual music files that are visited depend on the tag conversion bundles that are deployed along this bundle.
 *
 * The actual checks that are performed depend on the tag checker bundles that are deployed along this bundle.
 */
@Component
public class AudioTagCheckerImpl implements AudioTagChecker, ShutdownHookParticipant {
  /**
   * The set of (lowercase) filename extensions (without the dot) supported by the jaudiotagger library
   */
  private static final Set<String> supportedExtensions = new TreeSet<>();

  static {
    Locale locale = Locale.getDefault();
    for (SupportedFileFormat format : SupportedFileFormat.values()) {
      supportedExtensions.add(format.getFilesuffix().toLowerCase(locale));
    }
  }

  /*
   * Consumed Services
   */

  /** A set of all tag converters */
  @Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.AT_LEAST_ONE,
      policyOption = ReferencePolicyOption.GREEDY, fieldOption = FieldOption.REPLACE)
  volatile Collection<TagConverter> tagConverters = new HashSet<>();

  /** A set of all tag checkers */
  @Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.AT_LEAST_ONE,
      policyOption = ReferencePolicyOption.GREEDY, fieldOption = FieldOption.REPLACE)
  volatile Collection<TagChecker> tagCheckers = new HashSet<>();

  /** A set of all check process plugins */
  @Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.MULTIPLE,
      policyOption = ReferencePolicyOption.GREEDY, fieldOption = FieldOption.REPLACE)
  volatile Collection<CheckProcessPlugin> checkProcessPlugins = new HashSet<>();

  /*
   * Lifecycle
   */

  /**
   * Activator
   */
  @Activate
  void activate() {
    run.set(true);
  }

  /**
   * Deactivator
   */
  @Deactivate
  void deactivate() {
    shutdownHook();
  }

  /*
   * Internal Methods
   */

  /**
   * Determine whether a tag checker is enabled
   *
   * @param configuration
   *          the audio tag checker configuration
   * @param tagChecker
   *          the tag checker
   * @return true when the tag checker is enabled
   */
  static boolean isTagCheckerEnabled(AudioTagCheckerConfiguration configuration, TagChecker tagChecker) {
    return !configuration.isTagCheckerDisabled(tagChecker) //
        && (configuration.isTagCheckerEnabled(tagChecker) //
            || tagChecker.isEnabledByDefault());
  }

  /**
   * Determine whether a check process plugin is enabled
   *
   * @param configuration
   *          the audio tag checker configuration
   * @param checkProcessPlugin
   *          the check process plugin
   * @return true when the check process plugin is enabled
   */
  static boolean isCheckProcessPluginEnabled(AudioTagCheckerConfiguration configuration,
      CheckProcessPlugin checkProcessPlugin) {
    return !configuration.isCheckProcessPluginDisabled(checkProcessPlugin) //
        && (configuration.isCheckProcessPluginEnabled(checkProcessPlugin) //
            || checkProcessPlugin.isEnabledByDefault());
  }

  /**
   * Remove entries from a map
   *
   * @param map
   *          the map with check process data to remove entries from
   * @param reports
   *          the map with notices and reports to remove entries from
   * @param filesToRemove
   *          the entries to remove from the map
   * @return true when the map is empty after entries removal
   */
  static boolean processRegularFilesToRemove(Map<File, CheckProcessFile> map, Map<File, CheckProcessReports> reports,
      List<File> filesToRemove) {
    for (File fileToRemove : filesToRemove) {
      map.remove(fileToRemove);
      reports.remove(fileToRemove);
    }
    filesToRemove.clear();

    return map.isEmpty();
  }

  /**
   * Clear notices and reports for all entries
   *
   * @param reports
   *          the map in which to clear notices and errors
   */
  static void clearReports(Map<File, CheckProcessReports> reports) {
    for (CheckProcessReports report : reports.values()) {
      report.clear();
    }
  }

  /**
   * Determine the file extensions for all files in the map
   *
   * @param map
   *          the map with files
   * @param callback
   *          the callback to invoke when an extension is not supported
   * @param filesToRemove
   *          files with unsupported extensions are put into this list of files
   */
  void processRegularFilesExtensions(Map<File, CheckProcessFile> map, AudioTagCheckerCallback callback,
      List<File> filesToRemove) {
    List<File> errors = new LinkedList<>();

    for (CheckProcessFile container : map.values()) {
      if (!run.get()) {
        return;
      }

      container.extension = ExtensionUtils.split(container.file.getName(), false)[1].toLowerCase(Locale.getDefault());
      if (container.extension.isEmpty() //
          || !supportedExtensions.contains(container.extension)) {
        callback.unsupportedExtension(container.file);
        errors.add(container.file);
        filesToRemove.add(container.file);
      }
    }

    if (errors.size() > 0) {
      callback.unsupportedExtension(errors);
    }
  }

  /**
   * Read all files in the map
   *
   * @param map
   *          the map with files
   * @param callback
   *          the callback to invoke when a file is unreadable
   * @param filesToRemove
   *          unreadable files are put into this list of files
   */
  void processRegularFilesReadFile(Map<File, CheckProcessFile> map, AudioTagCheckerCallback callback,
      List<File> filesToRemove) {
    Map<File, Exception> errors = new HashMap<>();

    for (CheckProcessFile container : map.values()) {
      if (!run.get()) {
        return;
      }

      try {
        container.af = AudioFileIO.read(container.file);
      }
      catch (Exception e) {
        callback.notReadable(container.file, e);
        errors.put(container.file, e);
        filesToRemove.add(container.file);
      }
    }

    if (errors.size() > 0) {
      callback.notReadable(errors);
    }
  }

  /**
   * Read the tags of all files in the map
   *
   * @param map
   *          the map with files
   * @param callback
   *          the callback to invoke when a file has no tag
   * @param filesToRemove
   *          files without a tag are put into this list of files
   */
  void processRegularFilesReadTag(Map<File, CheckProcessFile> map, AudioTagCheckerCallback callback,
      List<File> filesToRemove) {
    List<File> errors = new LinkedList<>();

    for (CheckProcessFile container : map.values()) {
      if (!run.get()) {
        return;
      }

      container.tag = container.af.getTag();
      if (container.tag == null) {
        callback.noTag(container.file);
        errors.add(container.file);
        filesToRemove.add(container.file);
      }
    }

    if (errors.size() != 0) {
      callback.noTag(errors);
    }
  }

  /**
   * Convert the tags of all files in the map
   *
   * @param map
   *          the map with files
   * @param callback
   *          the callback to invoke when a tag could not be converted
   * @param filesToRemove
   *          files with tags that couldn't be converted are put into this list of files
   */
  void processRegularFilesConvertTag(Map<File, CheckProcessFile> map, AudioTagCheckerCallback callback,
      List<File> filesToRemove) {
    Map<File, Tag> errors = new HashMap<>();

    for (CheckProcessFile container : map.values()) {
      if (!run.get()) {
        return;
      }

      /* always set the backing file of the tag */
      container.genericTag = new GenericTag();
      container.genericTag.setBackingFile(container.file);
      container.genericTag.setArtwork(container.tag.getArtworkList());

      Class<? extends Tag> tagClass = container.tag.getClass();
      container.genericTag.addSourceTagClass(tagClass);

      boolean                  converted  = false;
      Collection<TagConverter> converters = tagConverters;
      for (TagConverter tagConverter : converters) {
        converted = tagConverter.getSupportedTagClasses().contains(tagClass) //
            && tagConverter.convert(container.genericTag, container.tag);
        if (converted) {
          /* exit the loop early when conversion succeeded */
          break;
        }
      }

      if (!converted) {
        callback.tagNotConverted(container.file, container.tag);
        errors.put(container.file, container.tag);
        filesToRemove.add(container.file);
      }
    }

    if (errors.size() != 0) {
      callback.tagNotConverted(errors);
    }
  }

  /**
   * Check the tags of all files in the map
   *
   * @param configuration
   *          the configuration of the program, used to determine which tag checkers to run
   * @param map
   *          the map with files
   * @param callback
   *          the callback to invoke after the checks on the tag
   */
  void processRegularFilesCheckTag(AudioTagCheckerConfiguration configuration, Map<File, CheckProcessFile> map,
      AudioTagCheckerCallback callback) {
    List<GenericTag> successes = new LinkedList<>();
    List<GenericTag> errors    = new LinkedList<>();

    for (CheckProcessFile container : map.values()) {
      if (!run.get()) {
        return;
      }

      GenericTag tag = container.genericTag;

      Collection<TagChecker> checkers = tagCheckers;
      for (TagChecker tagChecker : checkers) {
        if (!isTagCheckerEnabled(configuration, tagChecker)) {
          continue;
        }

        tagChecker.check(tag);
      }

      /*
       * Report results
       */

      if (tag.getReports().size() != 0) {
        callback.checksFailed(tag);
        errors.add(tag);
        continue;
      }

      callback.checksPassed(tag);
      successes.add(tag);
    }

    if (errors.size() > 0) {
      callback.checksFailed(errors);
    }

    if (successes.size() > 0) {
      callback.checksPassed(successes);
    }
  }

  /**
   * <p>
   * Process a list of regular files
   * </p>
   *
   * @param configuration
   *          the configuration
   * @param regularFiles
   *          the (non-directory) files to process
   * @param callback
   *          the callback to use
   * @return the instruction on how to proceed
   */
  CheckProcessInstruction processRegularFiles(AudioTagCheckerConfiguration configuration, List<File> regularFiles,
      AudioTagCheckerCallback callback) {
    Map<File, CheckProcessFile>    map           = new TreeMap<>();
    Map<File, CheckProcessReports> reports       = new TreeMap<>();
    List<File>                     filesToRemove = new LinkedList<>();

    /*
     * Populate map
     */

    for (File regularFile : regularFiles) {
      CheckProcessFile rfc = new CheckProcessFile();
      rfc.file = regularFile;
      map.put(regularFile, rfc);
      reports.put(regularFile, new CheckProcessReports());
    }

    Collection<CheckProcessPlugin> plugins = checkProcessPlugins;

    CheckProcessInstruction r = CheckProcessInstruction.CONTINUE;
    try {
      /*
       * File Entry
       */

      for (CheckProcessPlugin plugin : plugins) {
        if (isCheckProcessPluginEnabled(configuration, plugin)) {
          r = r.aggregate(plugin.filesEntry(map, filesToRemove, reports));
        }
      }
      callback.report(reports);
      clearReports(reports);
      if (r != CheckProcessInstruction.CONTINUE) {
        return r;
      }
      if (processRegularFilesToRemove(map, reports, filesToRemove)) {
        return CheckProcessInstruction.CONTINUE;
      }

      /*
       * Determine extension
       */

      processRegularFilesExtensions(map, callback, filesToRemove);
      for (CheckProcessPlugin plugin : plugins) {
        if (isCheckProcessPluginEnabled(configuration, plugin)) {
          r = r.aggregate(plugin.filesExtensions(map, filesToRemove, reports));
        }
      }
      callback.report(reports);
      clearReports(reports);
      if (r != CheckProcessInstruction.CONTINUE) {
        return r;
      }
      if (processRegularFilesToRemove(map, reports, filesToRemove)) {
        return CheckProcessInstruction.CONTINUE;
      }

      /*
       * Read the file
       */

      processRegularFilesReadFile(map, callback, filesToRemove);
      for (CheckProcessPlugin plugin : plugins) {
        if (isCheckProcessPluginEnabled(configuration, plugin)) {
          r = r.aggregate(plugin.filesRead(map, filesToRemove, reports));
        }
      }
      callback.report(reports);
      clearReports(reports);
      if (r != CheckProcessInstruction.CONTINUE) {
        return r;
      }
      if (processRegularFilesToRemove(map, reports, filesToRemove)) {
        return CheckProcessInstruction.CONTINUE;
      }

      /*
       * Read the tag
       */

      processRegularFilesReadTag(map, callback, filesToRemove);
      for (CheckProcessPlugin plugin : plugins) {
        if (isCheckProcessPluginEnabled(configuration, plugin)) {
          r = r.aggregate(plugin.filesTagsRead(map, filesToRemove, reports));
        }
      }
      callback.report(reports);
      clearReports(reports);
      if (r != CheckProcessInstruction.CONTINUE) {
        return r;
      }
      if (processRegularFilesToRemove(map, reports, filesToRemove)) {
        return CheckProcessInstruction.CONTINUE;
      }

      /*
       * Convert the tag
       */

      processRegularFilesConvertTag(map, callback, filesToRemove);
      for (CheckProcessPlugin plugin : plugins) {
        if (isCheckProcessPluginEnabled(configuration, plugin)) {
          r = r.aggregate(plugin.filesTagsConverted(map, filesToRemove, reports));
        }
      }
      callback.report(reports);
      clearReports(reports);
      if (r != CheckProcessInstruction.CONTINUE) {
        return r;
      }
      if (processRegularFilesToRemove(map, reports, filesToRemove)) {
        return CheckProcessInstruction.CONTINUE;
      }

      /*
       * Run all tag checkers
       */

      processRegularFilesCheckTag(configuration, map, callback);
    }
    finally {

      /*
       * Done
       */

      for (CheckProcessPlugin plugin : plugins) {
        if (isCheckProcessPluginEnabled(configuration, plugin)) {
          r = r.aggregate(plugin.filesExit(map, reports));
        }
      }
      callback.report(reports);
      clearReports(reports);

      filesToRemove.clear();
      map.clear();

      if (r != CheckProcessInstruction.CONTINUE) {
        return r;
      }
    }

    return CheckProcessInstruction.CONTINUE;
  }

  /**
   * <p>
   * Process a certain directory/file, and below (if so indicated).
   * </p>
   * <p>
   * This is a recursive method.
   * </p>
   *
   * @param configuration
   *          the configuration
   * @param file
   *          the directory/file to process
   * @param filenameFilter
   *          the filename filter to use
   * @param scanDeeper
   *          if true then also process files and directories below the current directory (unless inhibited by the
   *          global recursiveScan setting in the configuration)
   * @param callback
   *          the callback to use
   */
  void process(AudioTagCheckerConfiguration configuration, File file, FilenameFilter filenameFilter, boolean scanDeeper,
      AudioTagCheckerCallback callback) {
    if (!run.get() //
        || !file.exists()) {
      return;
    }

    List<File> directories  = new LinkedList<>();
    List<File> regularFiles = new LinkedList<>();

    if (file.isDirectory()) {
      if (!scanDeeper //
          || !configuration.isRecursiveScan()) {
        /* we're not allowed to dive into the directory */
        return;
      }

      CheckProcessInstruction r = CheckProcessInstruction.CONTINUE;
      do {
        /* list all (filtered) files in the directory */
        File[] directoryFiles = file.listFiles(filenameFilter);
        if ((directoryFiles == null) // Can't be covered in a test since file is
                                     // guaranteed to be a directory and we
                                     // can't force IO errors
            || (directoryFiles.length == 0)) {
          /* no files: return */
          return;
        }

        /* sort the files */
        Arrays.sort(directoryFiles);

        directories.clear();
        regularFiles.clear();

        /* split in directories and regular files */
        for (File f : directoryFiles) {
          if (f.isDirectory()) {
            directories.add(f);
          } else {
            regularFiles.add(f);
          }
        }

        Collection<CheckProcessPlugin> plugins = checkProcessPlugins;
        r = CheckProcessInstruction.CONTINUE;
        try {
          for (CheckProcessPlugin plugin : plugins) {
            if (isCheckProcessPluginEnabled(configuration, plugin)) {
              plugin.directoryEntry(file, directories, regularFiles);
            }
          }

          /* process all the regular files in this directory */
          if (regularFiles.size() > 0) {
            r = r.aggregate(processRegularFiles(configuration, regularFiles, callback));
          }
        }
        finally {
          for (CheckProcessPlugin plugin : plugins) {
            if (isCheckProcessPluginEnabled(configuration, plugin)) {
              r = r.aggregate(plugin.directoryExit(file));
            }
          }
        }
      }
      while (r == CheckProcessInstruction.RESTART_DIRECTORY);

      /* process all the directories in this directory */
      for (File directoryFile : directories) {
        if (!run.get()) {
          return;
        }
        process(configuration, directoryFile, filenameFilter, configuration.isRecursiveScan(), callback);
      }

      return;
    }

    /*
     * We're not dealing with a directory, it must be a regular file
     */

    regularFiles.add(file);

    File       directory      = file.getParentFile();
    List<File> directoryFiles = new LinkedList<>();
    directoryFiles.add(new File(file.getName()));

    Collection<CheckProcessPlugin> plugins = checkProcessPlugins;
    try {
      for (CheckProcessPlugin plugin : plugins) {
        if (isCheckProcessPluginEnabled(configuration, plugin)) {
          plugin.directoryEntry(directory, directories, directoryFiles);
        }
      }

      /* process all the regular files in this directory */
      processRegularFiles(configuration, regularFiles, callback);
    }
    finally {
      for (CheckProcessPlugin plugin : plugins) {
        if (isCheckProcessPluginEnabled(configuration, plugin)) {
          plugin.directoryExit(directory);
        }
      }
    }
  }

  /*
   * Interface Methods
   */

  @Override
  public boolean check(AudioTagCheckerConfiguration config, AudioTagCheckerCallback callback) throws IOException {
    if ((config == null) //
        || (callback == null)) {
      return false;
    }

    File scanPath = config.getCheckPath();
    if ((scanPath == null) //
        || !scanPath.exists()) {
      return false;
    }

    FilenameFilter filenameFilter =
        new FilenameFilterWithRegex(config.isRegexInAllDirs() ? null : scanPath, config.getRegexPattern());

    process(config, scanPath, filenameFilter, true, callback);

    return true;
  }

  /*
   * ShutdownHookParticipant
   */

  /** true while we must keep running */
  AtomicBoolean run = new AtomicBoolean(true);

  @Override
  public void shutdownHook() {
    run.set(false);
  }
}
