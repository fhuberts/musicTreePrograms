package nl.pelagic.audio.conversion.flac2mp3.api;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import nl.pelagic.audio.conversion.flac2mp3.api.i18n.Messages;

/**
 * Configuration for the flac to mp3 conversion. Holds options for the flac and lame processes that actually perform the
 * conversion.
 */
public class Flac2Mp3Configuration {
  /** default flac executable */
  public static final String DEFAULT_FLAC_EXECUTABLE = "flac"; //$NON-NLS-1$

  /** default flac options */
  public static final String DEFAULT_FLAC_OPTIONS = "-s -d -c"; //$NON-NLS-1$

  /** default lame executable */
  public static final String DEFAULT_LAME_EXECUTABLE = "lame"; //$NON-NLS-1$

  /** default lame options */
  public static final String DEFAULT_LAME_OPTIONS = "-S -h -b 320"; //$NON-NLS-1$

  /** default setting for using ID3v1 tags */
  public static final boolean DEFAULT_USE_ID3V1_TAGS = false;

  /** default setting for using ID3v24 tags */
  public static final boolean DEFAULT_USE_ID3V24_TAGS = false;

  /** default setting for forcing conversion */
  public static final boolean DEFAULT_FORCE_CONVERSION = false;

  /** default setting for running flac and lame in the conversion */
  private static final boolean DEFAULT_RUN_FLAC_LAME = true;

  /** default setting for copying tags in the conversion */
  private static final boolean DEFAULT_COPY_TAGS = true;

  /** default setting for copying the timestamp in the conversion */
  private static final boolean DEFAULT_COPY_TIMESTAMP = true;

  /** flac executable */
  private String flacExecutable = DEFAULT_FLAC_EXECUTABLE;

  /** flac options */
  private List<String> flacOptions = new LinkedList<>();

  /** flac executable */
  private String lameExecutable = DEFAULT_LAME_EXECUTABLE;

  /** lame options */
  private List<String> lameOptions = new LinkedList<>();

  /** true to put ID3V1 tags in the created mp3 files, false to not do it */
  private boolean useId3V1Tags = DEFAULT_USE_ID3V1_TAGS;

  /** true to put ID3V24 tags in the created mp3 files, false for ID3V23 tags */
  private boolean useId3V24Tags = DEFAULT_USE_ID3V24_TAGS;

  /** true to force conversion */
  private boolean forceConversion = DEFAULT_FORCE_CONVERSION;

  /** true to run flac and lame in the conversion */
  private boolean runFlacLame = DEFAULT_RUN_FLAC_LAME;

  /** true to copy tags in the conversion */
  private boolean copyTag = DEFAULT_COPY_TAGS;

  /** true to copy the timestamp in the conversion */
  private boolean copyTimestamp = DEFAULT_COPY_TIMESTAMP;

  /**
   * Constructor. Sets default options.
   */
  public Flac2Mp3Configuration() {
    setFlacOptions(DEFAULT_FLAC_OPTIONS);
    setLameOptions(DEFAULT_LAME_OPTIONS);
  }

  /**
   * Try to run the program
   * 
   * @param program
   *          the program to run
   * @return true when running the program was successful
   */
  static boolean tryProgramRun(String[] program) {
    if ((program == null) || (program[0] == null) || (program[0].isEmpty())) {
      return false;
    }

    Process programProcess = null;
    try {
      ProcessBuilder programProcessBuilder = new ProcessBuilder(program);
      programProcess = programProcessBuilder.start();
    }
    catch (Exception e) {
      /* swallow */
      return false;
    }
    finally {
      boolean complete = false;
      while (!complete && (programProcess != null)) {
        try {
          programProcess.waitFor();
          complete = true;
        }
        catch (InterruptedException e) {
          /* swallow & can't be covered by a test */
        }
      }
    }

    return true;
  }

  /**
   * Validate the configuration. Currently only checks that the flac and lame executables are actually executable by
   * trying to execute them with a 'help' argument ("-h" and "--help" for flac and lame respectively)
   * 
   * @return A list with errors, or null when validated
   */
  public List<String> validate() {
    List<String> result = new LinkedList<>();

    String[] flacProgram = {
        flacExecutable,
        "-h" //$NON-NLS-1$
    };
    if (!tryProgramRun(flacProgram)) {
      result.add(String.format(Messages.getString("Flac2Mp3Configuration.0"), flacExecutable)); //$NON-NLS-1$
    }

    String[] lameProgram = {
        lameExecutable,
        "--help" //$NON-NLS-1$
    };
    if (!tryProgramRun(lameProgram)) {
      result.add(String.format(Messages.getString("Flac2Mp3Configuration.1"), lameExecutable)); //$NON-NLS-1$
    }

    if (result.size() == 0) {
      return null;
    }

    return result;
  }

  /**
   * @return the flacExecutable
   */
  public String getFlacExecutable() {
    return flacExecutable;
  }

  /**
   * @param flacExecutable
   *          the flacExecutable to set
   */
  public void setFlacExecutable(String flacExecutable) {
    this.flacExecutable = flacExecutable;
  }

  /**
   * @return the flacOptions
   */
  public List<String> getFlacOptions() {
    return flacOptions;
  }

  /**
   * @param flacOptions
   *          the flacOptions to set. Options are whitespace separated.
   */
  public void setFlacOptions(String flacOptions) {
    this.flacOptions = //
        ((flacOptions == null) //
            || flacOptions.trim().isEmpty()) //
                ? null //
                : Arrays.asList(flacOptions.trim().split("\\s+")); //$NON-NLS-1$
  }

  /**
   * @return the lameExecutable
   */
  public String getLameExecutable() {
    return lameExecutable;
  }

  /**
   * @param lameExecutable
   *          the lameExecutable to set
   */
  public void setLameExecutable(String lameExecutable) {
    this.lameExecutable = lameExecutable;
  }

  /**
   * @return the lameOptions
   */
  public List<String> getLameOptions() {
    return lameOptions;
  }

  /**
   * @param lameOptions
   *          the lameOptions to set. Options are whitespace separated.
   */
  public void setLameOptions(String lameOptions) {
    this.lameOptions = //
        ((lameOptions == null) //
            || lameOptions.trim().isEmpty()) //
                ? null //
                : Arrays.asList(lameOptions.trim().split("\\s+")); //$NON-NLS-1$
  }

  /**
   * @return the useId3V1Tags
   */
  public boolean isUseId3V1Tags() {
    return useId3V1Tags;
  }

  /**
   * @param useId3V1Tags
   *          the useId3V1Tags to set
   */
  public void setUseId3V1Tags(boolean useId3V1Tags) {
    this.useId3V1Tags = useId3V1Tags;
  }

  /**
   * @return the useId3V24Tags
   */
  public boolean isUseId3V24Tags() {
    return useId3V24Tags;
  }

  /**
   * @param useId3V24Tags
   *          the useId3V24Tags to set
   */
  public void setUseId3V24Tags(boolean useId3V24Tags) {
    this.useId3V24Tags = useId3V24Tags;
  }

  /**
   * @return the forceConversion
   */
  public boolean isForceConversion() {
    return forceConversion;
  }

  /**
   * @param forceConversion
   *          the forceConversion to set
   */
  public void setForceConversion(boolean forceConversion) {
    this.forceConversion = forceConversion;
  }

  /**
   * @return the runFlacLame
   */
  public boolean isRunFlacLame() {
    return runFlacLame;
  }

  /**
   * @param runFlacLame
   *          the runFlacLame to set
   */
  public void setRunFlacLame(boolean runFlacLame) {
    this.runFlacLame = runFlacLame;
  }

  /**
   * @return the copyTag
   */
  public boolean isCopyTag() {
    return copyTag;
  }

  /**
   * @param copyTag
   *          the copyTag to set
   */
  public void setCopyTag(boolean copyTag) {
    this.copyTag = copyTag;
  }

  /**
   * @return the copyTimestamp
   */
  public boolean isCopyTimestamp() {
    return copyTimestamp;
  }

  /**
   * @param copyTimestamp
   *          the copyTimestamp to set
   */
  public void setCopyTimestamp(boolean copyTimestamp) {
    this.copyTimestamp = copyTimestamp;
  }
}