package nl.pelagic.audio.conversion.flac2mp3.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import java.util.Arrays;
import java.util.List;

import nl.pelagic.audio.conversion.flac2mp3.api.i18n.Messages;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings({
    "nls",
    "javadoc",
    "static-method"
})
public class TestFlac2Mp3Configuration {

  private Flac2Mp3Configuration flac2Mp3Configuration = null;

  @Before
  public void setUp() {
    flac2Mp3Configuration = new Flac2Mp3Configuration();
  }

  @After
  public void tearDown() {
    flac2Mp3Configuration = null;
  }

  private static List<String> toList(String str) {
    return Arrays.asList(str.trim().split("\\s+"));
  }

  @Test(timeout = 8000)
  public void testTryProgramRun_Null() {
    boolean r = Flac2Mp3Configuration.tryProgramRun(null);

    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));
  }

  @Test(timeout = 8000)
  public void testTryProgramRun_Null_In_First() {
    String[] program = {
        null,
        "some.program.that.doesnt.exist"
    };
    boolean  r       = Flac2Mp3Configuration.tryProgramRun(program);

    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));
  }

  @Test(timeout = 8000)
  public void testTryProgramRun_Empty_In_First() {
    String[] program = {
        "",
        "some.program.that.doesnt.exist"
    };
    boolean  r       = Flac2Mp3Configuration.tryProgramRun(program);

    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));
  }

  @Test(timeout = 8000)
  public void testTryProgramRun_NotFound() {
    String[] program = {
        "some.program.that.doesnt.exist"
    };
    boolean  r       = Flac2Mp3Configuration.tryProgramRun(program);

    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));
  }

  @Test(timeout = 8000)
  public void testTryProgramRun_Found() {
    String[] program = {
        "echo"
    };
    boolean  r       = Flac2Mp3Configuration.tryProgramRun(program);

    assertThat(Boolean.valueOf(r), equalTo(Boolean.TRUE));
  }

  @Test(timeout = 8000)
  public void testValidate_NotFound_Flac() {
    Flac2Mp3Configuration config = new Flac2Mp3Configuration();
    config.setFlacExecutable(null);
    config.setLameExecutable("echo");
    List<String> r = config.validate();

    assertThat(r, notNullValue());
    assertThat(Integer.valueOf(r.size()), equalTo(Integer.valueOf(1)));
    String s = r.get(0);
    assertThat(s, notNullValue());
    assertThat(s, equalTo(String.format(Messages.getString("Flac2Mp3Configuration.0"), "null")));
  }

  @Test(timeout = 8000)
  public void testValidate_NotFound_Lame() {
    Flac2Mp3Configuration config = new Flac2Mp3Configuration();
    config.setFlacExecutable("echo");
    config.setLameExecutable(null);

    List<String> r = config.validate();

    assertThat(r, notNullValue());
    assertThat(Integer.valueOf(r.size()), equalTo(Integer.valueOf(1)));
    String s = r.get(0);
    assertThat(s, notNullValue());
    assertThat(s, equalTo(String.format(Messages.getString("Flac2Mp3Configuration.1"), "null")));
  }

  @Test(timeout = 8000)
  public void testValidate_Normal() {
    Flac2Mp3Configuration config = new Flac2Mp3Configuration();
    config.setFlacExecutable("echo");
    config.setLameExecutable("echo");
    List<String> r = config.validate();
    assertThat(r, nullValue());
  }

  @Test(timeout = 8000)
  public void testGetFlacOptions_Defaults() {
    List<String> dfo = toList(Flac2Mp3Configuration.DEFAULT_FLAC_OPTIONS);
    List<String> dlo = toList(Flac2Mp3Configuration.DEFAULT_LAME_OPTIONS);

    String       fe  = flac2Mp3Configuration.getFlacExecutable();
    List<String> fo  = flac2Mp3Configuration.getFlacOptions();
    String       le  = flac2Mp3Configuration.getLameExecutable();
    List<String> lo  = flac2Mp3Configuration.getLameOptions();
    boolean      v24 = flac2Mp3Configuration.isUseId3V24Tags();

    assertThat(fe, equalTo(Flac2Mp3Configuration.DEFAULT_FLAC_EXECUTABLE));
    assertThat(fo, equalTo(dfo));
    assertThat(le, equalTo(Flac2Mp3Configuration.DEFAULT_LAME_EXECUTABLE));
    assertThat(lo, equalTo(dlo));
    assertThat(Boolean.valueOf(v24), equalTo(Boolean.valueOf(Flac2Mp3Configuration.DEFAULT_USE_ID3V24_TAGS)));
  }

  @Test(timeout = 8000)
  public void testSetFlacOptions() {
    String       in       = "  a -b 123 --d  ";
    List<String> expected = toList(in);
    flac2Mp3Configuration.setFlacOptions("  a -b 123 --d  ");
    List<String> result = flac2Mp3Configuration.getFlacOptions();

    assertThat(result, equalTo(expected));
  }

  @Test(timeout = 8000)
  public void testSetLameOptions() {
    String       in       = "  a -b 123 --d  ";
    List<String> expected = toList(in);
    flac2Mp3Configuration.setLameOptions("  a -b 123 --d  ");
    List<String> result = flac2Mp3Configuration.getLameOptions();

    assertThat(result, equalTo(expected));
  }

  @Test(timeout = 8000)
  public void testSetUseId3V24Tags() {
    boolean s = true;
    flac2Mp3Configuration.setUseId3V24Tags(s);
    boolean v24 = flac2Mp3Configuration.isUseId3V24Tags();
    assertThat(Boolean.valueOf(v24), equalTo(Boolean.valueOf(s)));

    s = false;
    flac2Mp3Configuration.setUseId3V24Tags(s);
    v24 = flac2Mp3Configuration.isUseId3V24Tags();
    assertThat(Boolean.valueOf(v24), equalTo(Boolean.valueOf(s)));
  }

  @Test(timeout = 8000)
  public void testSetForceConversion() {
    assertThat(Boolean.valueOf(flac2Mp3Configuration.isForceConversion()),
        equalTo(Boolean.valueOf(Flac2Mp3Configuration.DEFAULT_FORCE_CONVERSION)));
    flac2Mp3Configuration.setForceConversion(!Flac2Mp3Configuration.DEFAULT_FORCE_CONVERSION);
    assertThat(Boolean.valueOf(flac2Mp3Configuration.isForceConversion()),
        equalTo(Boolean.valueOf(!Flac2Mp3Configuration.DEFAULT_FORCE_CONVERSION)));
  }
}
